# GRAFANA - DATA VISUALISATION

Grafana allows you to query, visualize, alert on and understand your metrics no matter where they are stored. Create, explore, and share dashboards with your team and foster a data driven culture.  
More information : [https://grafana.com/](https://grafana.com/)

## Grafana configuration for the deployment of docker containers

The Grafana configurations used for development and production profiles are the same except for the ports on which the application is served.  
You can find the development and production configurations in their respective folders.  
  - [development](./development/conf)
  - [production](./production/conf)

## Grafana data storage for the deployment of docker containers

You can find the development and production data in their respective folders.  
  - [development](./development/storage)
  - [production](./production/storage)
