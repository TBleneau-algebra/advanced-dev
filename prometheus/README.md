# PROMETHEUS - MONITORING SYSTEM  

Prometheus is an open-source systems monitoring and alerting toolkit originally built at SoundCloud.  
More information : [https://prometheus.io](https://prometheus.io)

## Prometheus configuration for the deployment of docker containers

The Prometheus configurations used for development and production profiles are the same except the target application and its port.  
You can find the development and production configurations in their respective folders.  
  - [development](./development/conf)
  - [production](./production/conf)

## Prometheus data storage for the deployment of docker containers

You can find the development and production data in their respective folders.  
  - [development](./development/storage)
  - [production](./production/storage)
