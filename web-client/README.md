# Application WEB (Client - User Interface)

## Install NodeJS 

Requirement : The first step to build or run this project is to install [NodeJS](https://nodejs.org/en/download/). 

## Install Angular CLI

Install the [Angular CLI](https://github.com/angular/angular-cli) globally.
To install the CLI using `npm`, open a terminal/console window and run the following command :

```sh
      npm install -g @angular/cli
``` 

## Install Angular and project dependencies

To install the Angular and project dependencies using `npm`, run the following command at the root of the project :

```sh
      npm install
``` 

## Run the development server

For a development server run :
```sh
      ng serve
```
or 
```sh
      ng serve --configuration=development
```
or 
```sh
      npm run start
```

Navigate to `http://localhost:8001/`. The app will automatically reload if you change any of the source files.

## Run the unit tests

To execute the unit tests via [Karma](https://karma-runner.github.io), run :

```sh
      ng test
```
or 
```sh
      npm run test
``` 

## Build a static Angular app with the required configuration  

For the development configuration run : 
```sh
      ng build / ng build --configuration=development (these two commands are similar)
```
or
```sh
      npm run build / npm run build:dev (these two commands are similar)
```

For the production configuration run : 
```sh
      ng build --configuration=production
```
or
```sh
      npm run build:prod
```

## Code scaffolding

When you use the Angular CLI's `generate` command, the root folder for creating elements is `src/app`.

To generate a new component, run 
```sh
      ng generate component /src/app/components/component-name
```

To generate a new directive, run 
```sh
      ng generate directive /src/app/shared/directives/directive-name
```

To generate a new pipe, run 
```sh
      ng generate pipe /path/pipe-name
```

To generate a new service, run 
```sh
      ng generate service /src/app/shared/services/service-name
```

To generate a new class, run 
```sh
      ng generate class /path/class-name --type={{choose type}}`

For example :

      ng generate class /src/app/shared/entities/user-detailled --type=entity`
```

To generate a new guard, run
```sh 
      ng generate guard /src/app/shared/guard/guard-name
```

To generate a new interface, run
```sh 
      ng generate interface /src/app/shared/interfaces/interface-name
```

To generate a new enum, run 
```sh
      ng generate enum /src/app/shared/enums/enum-name
```

To generate a new module, run 
```sh
      ng generate module /src/app/shared/modules/module-name 
```

## Git branch format

All branches must be created from the parent branch `dev`.

The branch to be created for a layout : 
```sh
      git branch web-client/layout/layout-name
```
To create multiple versions of a layout, specify the layout version : 
```sh
      git branch web-client/layout/layout-name-version
```

The branch to be created for a section :
```sh
      git branch web-client/section/section-name
```
To create multiple versions of a section, specify the section version : 
```sh
      git branch web-client/section/section-name-version
```

The branch to be created for a component : 
```sh
      git branch web-client/component/component-name
```
To create multiple versions of a component, specify the component version : 
```sh
      git branch web-client/component/component-name-version
``` 

The branch to be created for a service : 
```sh
      git branch web-client/service/service-name
```
To create multiple versions of a service, specify the service version : 
```sh
      git branch web-client/service/service-name-version
```
 
The branch to be created for an entity : 
```sh
      git branch web-client/entity/entity-name
```
To create multiple versions of an entity, specify the entity version :
```sh 
      git branch web-client/entity/entity-name-version`
``` 

The branch to be created for an interface : 
```sh
      git branch web-client/interface/interface-name
```
To create multiple versions of an interface, specify the interface version : 
```sh
      git branch web-client/interface/interface-name-version
```

The branch to be created for a module : 
```sh
      git branch web-client/module/module-name
```
To create multiple versions of a module, specify the module version : 
```sh
      git branch web-client/module/module-name-version
```  

## Git commit format

The first line of the commit message must contains the tags depending on the commit's feature.
The tags hierarchy must be ascendant, that means from the most general to the most precise.

Example 1 :

```sh 
      git commit -m "[WEB_CLIENT][COMPONENTS][NAVBAR] Navigation bar creation allowing the user to navigate on the application"
```

Example 2 : 

```sh
      git commit -m "[WEB_CLIENT][COMPONENTS][NAVBAR][BUTTON][LOGIN][STYLE] Modfication of the button's color from red to green"
```

## Code comment format

To comment the code in Angular, we use the official [JSDoc Support](https://github.com/Microsoft/TypeScript/wiki/JSDoc-support-in-JavaScript) in Java Script.
JSDoc use a tag system that you can find at these addresses : 

- [usejsdoc.org](http://usejsdoc.org/)
- [JSDoc Support](https://github.com/Microsoft/TypeScript/wiki/JSDoc-support-in-JavaScript)

For example :

```sh 
      /**
       * @description Construct NetworkService
       * @param _http A reference to the internal angular HTTP Service
       */
      constructor(private _http: HttpClient) {
          ...
      }
```
```sh 
      /**
       * @description A get method to retrieve the connection path of the WebsocketService
       * @return Requested WebSocket's connection path
       */
      get connection(): string {
          ...
      }
```
```sh 
      /**
       * @description This method is used to build the path of the request
       * @param path The part to append
       * @return this
       * @example
       * Request<T>.path('/user').path(somevariable).path('/fullinfo') // -> /user/me/fullinfo
       */
      path(path: string): ApiRequestInterface<T>;
```

All of your code must be commented (interface, constant, service, component, directive, etc...).
If the code is not commented correctly, the merge request and your task will not be validated.

## Merge request on the dev branch

If you want to merge a branch into dev branch, first make sure you push all your commits on the branch itself. 
Next, run the following command : 

```sh
      git push origin dev
```

This command will prohibit you to push and display a link to make a merge request on your GitLab account.
Follow this link and complete the merge request. This will notify the maintainers of the repository to evaluate your merge request.
He will then accept or refuse your merge request.

## FontAwesome 5

The project use the version 5 of FontAwesome. To retrieve the entire list, click on the following link :

[https://fontawesome.com/icons?d=gallery](https://fontawesome.com/icons?d=gallery)

## Nebular

The project use the last version of Nebular. To retrieve the documentation, click on the following link :

[https://akveo.github.io/nebular/docs/getting-started/what-is-nebular#what-is-nebular](https://akveo.github.io/nebular/docs/getting-started/what-is-nebular#what-is-nebular)

## Docker deployment 

Requirement : In the [nginx](./config/nginx) folder, create files <b>nginx.conf</b>, <b>default.dev.conf</b> and <b>default.prod.conf</b> to specify the configuration of each NGINX server 
running in docker containers.  

Note, if you enable the use of SSL certificates in your configuration, you can include your certificates in the [certs](./config/nginx/certs) folder.
