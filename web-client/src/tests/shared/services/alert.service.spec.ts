import {TestBed} from '@angular/core/testing';
import {AppModule} from '../../../app/app.module';
import {AlertService} from '../../../app/shared/services/alert/alert.service';

describe('AlertService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [AppModule],
    }));

    it('Should create the AlertService service', () => {
        const service: AlertService = TestBed.get(AlertService);
        expect(service).toBeTruthy();
    });

    it('Should run confirm()', () => {
        const service: AlertService = TestBed.get(AlertService);
        expect(service.confirm('Is it a test?', 'Yes', 'No')).toBeTruthy();
    });

    it('Should run error()', () => {
        const service: AlertService = TestBed.get(AlertService);
        expect(service.error('This is a test error')).toBeTruthy();
    });

    it('Should run html()', () => {
        const service: AlertService = TestBed.get(AlertService);
        expect(service.html('<a>Click ont the test link</a>')).toBeTruthy();
    });

    it('Should run image()', () => {
        const service: AlertService = TestBed.get(AlertService);
        expect(service.image('/assets/img/test.png', 600, 500)).toBeTruthy();
    });

    it('Should run info()', () => {
        const service: AlertService = TestBed.get(AlertService);
        expect(service.info('This is an info message test', null)).toBeTruthy();
    });

    it('Should run info()', () => {
        const service: AlertService = TestBed.get(AlertService);
        expect(service.success('This is a success message test', null)).toBeTruthy();
    });

    it('Should run info()', () => {
        const service: AlertService = TestBed.get(AlertService);
        expect(service.warning('This is a warning message test', null)).toBeTruthy();
    });
});
