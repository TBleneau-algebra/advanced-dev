import {TestBed} from '@angular/core/testing';
import {AppModule} from '../../../app/app.module';
import {LoaderService} from '../../../app/shared/services/loader-spinner/loader.service';

describe('LoaderService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [AppModule],
    }));

    it('Should create the LoaderService service', () => {
        const service: LoaderService = TestBed.get(LoaderService);
        expect(service).toBeTruthy();
    });

    it('Should instantiate a loader', () => {
        const service: LoaderService = TestBed.get(LoaderService);
        expect(service.register('test', false).getObservable('test')).toBeTruthy();
    });

    it('Should create a loader', async () => {
        const service: LoaderService = TestBed.get(LoaderService);
        expect(service.register('test', false).getObservable('test')).toBeTruthy();

        const observable = service.getObservable('test');

        const expectto = async (val) => {
            observable.subscribe((state) => {
                expect(state === val).toBeTruthy();
            });
        };

        const test1 = expectto(true);

        service.load('test');
        await test1;
    });

    it('Should unload a created loader', async () => {
        const service: LoaderService = TestBed.get(LoaderService);
        expect(service.register('test', false).getObservable('test')).toBeTruthy();

        const observable = service.getObservable('test');

        const expectto = (val) => {
            return new Promise((resolve, reject) => {
                observable.subscribe((state) => {
                    expect(state === val).toBeTruthy();
                    resolve();
                }, reject);
            });
        };

        service.load('test');

        const test1 = expectto(false);
        service.unload('test');
        await test1;
    });
});
