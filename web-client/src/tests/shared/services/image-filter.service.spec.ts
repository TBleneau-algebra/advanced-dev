import {TestBed} from '@angular/core/testing';
import {AppModule} from '../../../app/app.module';
import {PictureClass} from '../../../app/shared/classes/models/picture.class';

import {ImageFilterService} from '../../../app/shared/services/image-filter/image-filter.service';

describe('ImageFilterService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [AppModule],
        providers: []
    }));

    it('Should create the ImageFilterService service', () => {
        const service: ImageFilterService = TestBed.get(ImageFilterService);
        expect(service).toBeTruthy();
    });

    it('Should run serialize()', () => {
        const service: ImageFilterService = TestBed.get(ImageFilterService);
        const picture: PictureClass = new PictureClass();
        const div = document.createElement('div');
        const anchor = document.createElement('canvas');

        anchor.id = 'canvas';
        div.appendChild(anchor);
        expect(service.serialize(picture, 'image', document.getElementById('canvas') as HTMLCanvasElement)).toBeUndefined();
    });

    it('Should run draw()', () => {
        const service: ImageFilterService = TestBed.get(ImageFilterService);
        const div = document.createElement('div');
        const anchor = document.createElement('canvas');

        anchor.id = 'canvas';
        div.appendChild(anchor);
        expect(service.draw(1, 'data:image/png', document.getElementById('canvas') as HTMLCanvasElement)).toBeUndefined();
    });
});
