import {TestBed} from '@angular/core/testing';
import {AppModule} from '../../../app/app.module';
import {CustomCookieService} from '../../../app/shared/services/cookies/custom-cookie.service';

import {I18nTranslationService} from '../../../app/shared/services/i18n/i18n-translation.service';
import {LoaderSemaphoreClass} from '../../../app/shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';
import {LoaderService} from '../../../app/shared/services/loader-spinner/loader.service';

describe('I18nTranslationService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [AppModule],
        providers: [I18nTranslationService, LoaderService, CustomCookieService]
    }));

    it('Should create the I18nTranslationService service', () => {
        const service: I18nTranslationService = TestBed.get(I18nTranslationService);
        expect(service).toBeTruthy();
    });

    it('Should run changeLang()', () => {
        const service: I18nTranslationService = TestBed.get(I18nTranslationService);
        expect(service.changeLang('fr')).toBeUndefined();
        expect(service.changeLang('en')).toBeUndefined();
    });

    it('Should run getTranslation() and getTranslationObservable()', () => {
        const service: I18nTranslationService = TestBed.get(I18nTranslationService);
        expect(service.getTranslation()).toBeDefined();
        expect(service.getTranslationObservable()).toBeDefined();
    });

    it('Should run reloadTranslation()', () => {
        const service: I18nTranslationService = TestBed.get(I18nTranslationService);
        expect(service.reloadTranslation()).toBeTruthy();
    });

    it('Should run activeLoader()', () => {
        const service: I18nTranslationService = TestBed.get(I18nTranslationService);
        expect(service.activeLoader(new LoaderSemaphoreClass(1))).toBeUndefined();
    });
});
