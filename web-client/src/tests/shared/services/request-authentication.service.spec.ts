import {Injectable} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {AppModule} from '../../../app/app.module';
import {CustomCookieService} from '../../../app/shared/services/cookies/custom-cookie.service';
import {LoaderService} from '../../../app/shared/services/loader-spinner/loader.service';

import {RequestAuthenticationService} from '../../../app/shared/services/request/request-authentication.service';

@Injectable()
class MockHttpClient {
    post() {
    };
}

@Injectable()
class MockCustomCookieService {
}

@Injectable()
class MockLoaderService {
}

describe('RequestAuthenticationService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [AppModule],
        providers: [LoaderService, CustomCookieService]
    }));

    it('Should create the RequestAuthenticationService service', () => {
        const service: RequestAuthenticationService = TestBed.get(RequestAuthenticationService);
        expect(service).toBeTruthy();
    });

    /*it('Should run hasRole()', () => {
        const service: RequestAuthenticationService = TestBed.get(RequestAuthenticationService);
        expect(service.hasRole('guest')).toBeTruthy();
        expect(service.hasRole('user')).toBeFalsy();
        expect(service.hasRole('moderator')).toBeFalsy();
    });

    it('Should run getRole()', () => {
        const service: RequestAuthenticationService = TestBed.get(RequestAuthenticationService);
        expect(service.getRole()).toBeTruthy();
    });

    it('Should run isAuthenticated()', () => {
        const service: RequestAuthenticationService = TestBed.get(RequestAuthenticationService);
        expect(service.isAuthenticated()).toBeFalsy();
    });

    it('Should run setRoles() and hasRole()', () => {
        const service: RequestAuthenticationService = TestBed.get(RequestAuthenticationService);
        expect(service.setRoles([{id: '1', name: 'ROLE_GUEST'}, {id: '2', name: 'ROLE_USER'}])).toBeUndefined();
        expect(service.hasRole('guest')).toBeTruthy();
        expect(service.hasRole('user')).toBeTruthy();
    });*/
});
