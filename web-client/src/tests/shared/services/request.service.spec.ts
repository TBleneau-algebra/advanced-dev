import {TestBed} from '@angular/core/testing';
import {AppModule} from '../../../app/app.module';
import {LoaderSemaphoreClass} from '../../../app/shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';
import {LoaderService} from '../../../app/shared/services/loader-spinner/loader.service';

import {RequestService} from '../../../app/shared/services/request/request.service';


describe('RequestService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [AppModule],
        providers: [LoaderService]
    }));

    it('Should create the RequestService service', () => {
        const service: RequestService = TestBed.get(RequestService);
        expect(service).toBeTruthy();
    });

    /*it('Should run activeLoader()', () => {
        const service: RequestService = TestBed.get(RequestService);
        expect(service.activeLoader(new LoaderSemaphoreClass(1))).toBeUndefined();
    });

    it('Should run get()', () => {
        const service: RequestService = TestBed.get(RequestService);
        expect(service.get('api/v1/test/get')).toBeTruthy();
    });

    it('Should run post()', () => {
        const service: RequestService = TestBed.get(RequestService);
        expect(service.post(null, 'api/v1/test/put')).toBeTruthy();
    });
    it('Should run put()', () => {
        const service: RequestService = TestBed.get(RequestService);
        expect(service.put(null, 'api/v1/test/put')).toBeTruthy();
    });

    it('Should run patch()', () => {
        const service: RequestService = TestBed.get(RequestService);
        expect(service.patch(null, 'api/v1/test/patch')).toBeTruthy();
    });

    it('Should run delete()', () => {
        const service: RequestService = TestBed.get(RequestService);
        expect(service.delete(null, 'api/v1/test/delete')).toBeTruthy();
    });*/
});
