import {TestBed} from '@angular/core/testing';
import {CookieService} from 'ngx-cookie-service';
import {AppModule} from '../../../app/app.module';
import {CustomCookieService} from '../../../app/shared/services/cookies/custom-cookie.service';

describe('CustomCookieService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [AppModule],
        providers: [CustomCookieService, CookieService]
    }));

    it('Should create the CustomCookieService service', () => {
        const service: CustomCookieService = TestBed.get(CustomCookieService);
        expect(service).toBeTruthy();
    });

    it('Should run create()', () => {
        const service: CustomCookieService = TestBed.get(CustomCookieService);
        expect(service.create('CookieTest', 'value-cookie-test')).toBeUndefined();
    });

    it('Should run remove()', () => {
        const service: CustomCookieService = TestBed.get(CustomCookieService);
        expect(service.remove('CookieTest')).toBeUndefined();
    });

    it('Should run removeAll()', () => {
        const service: CustomCookieService = TestBed.get(CustomCookieService);
        expect(service.removeAll()).toBeUndefined();
    });

    it('Should run check()', () => {
        const service: CustomCookieService = TestBed.get(CustomCookieService);
        expect(service.check('CookieTest')).toBeFalsy();
    });

    it('Should run getAll()', () => {
        const service: CustomCookieService = TestBed.get(CustomCookieService);
        expect(service.getAll()).toBeDefined();
    });

    it('Should run get()', () => {
        const service: CustomCookieService = TestBed.get(CustomCookieService);
        expect(service.get('CookieTest')).toBeDefined();
    });
});
