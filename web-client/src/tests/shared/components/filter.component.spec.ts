import {TestBed} from '@angular/core/testing';
import {FilterComponent} from '../../../app/shared/components/filter/filter.component';

describe('FilterComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [FilterComponent]
        }).overrideComponent(FilterComponent, {}).compileComponents();
        fixture = TestBed.createComponent(FilterComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the FilterComponent component', async () => {
        expect(component).toBeTruthy();
    });
});
