import {TestBed} from '@angular/core/testing';
import {ViewComponent} from '../../../app/shared/components/view/view.component';

describe('ViewComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ViewComponent]
        }).overrideComponent(ViewComponent, {}).compileComponents();
        fixture = TestBed.createComponent(ViewComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the ViewComponent component', async () => {
        expect(component).toBeTruthy();
    });
});
