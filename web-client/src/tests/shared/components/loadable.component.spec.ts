import {TestBed} from '@angular/core/testing';
import {LoadableComponent} from '../../../app/shared/components/loadable/loadable.component';
import {LoaderService} from '../../../app/shared/services/loader-spinner/loader.service';

describe('LoadableComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [LoadableComponent],
            providers: [LoaderService]
        }).overrideComponent(LoadableComponent, {}).compileComponents();
        fixture = TestBed.createComponent(LoadableComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the LoadableComponent component', async () => {
        expect(component).toBeTruthy();
    });

    it('Should run ngOnInit()', async () => {
        expect(component.ngOnInit()).toBeUndefined();
    });
});
