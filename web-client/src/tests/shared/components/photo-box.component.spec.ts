import {HttpClientModule} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';
import {RouterModule} from '@angular/router';
import {NbMenuService} from '@nebular/theme';
import {CookieService} from 'ngx-cookie-service';
import {PhotoBoxComponent} from '../../../app/shared/components/photo-box/photo-box.component';
import {AlertService} from '../../../app/shared/services/alert/alert.service';
import {CustomCookieService} from '../../../app/shared/services/cookies/custom-cookie.service';
import {I18nTranslationService} from '../../../app/shared/services/i18n/i18n-translation.service';
import {ThemeModule} from '../../../app/shared/theme/theme.module';

describe('PhotoBoxComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ThemeModule, HttpClientModule, RouterModule.forRoot([])],
            declarations: [PhotoBoxComponent],
            providers: [AlertService, I18nTranslationService, CustomCookieService, CookieService, NbMenuService]
        }).overrideComponent(PhotoBoxComponent, {}).compileComponents();
        fixture = TestBed.createComponent(PhotoBoxComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the PhotoBoxComponent component', async () => {
        expect(component).toBeTruthy();
    });

    it('Should run ngOnInit()', async () => {
        expect(component.ngOnInit()).toBeUndefined();
    });
});
