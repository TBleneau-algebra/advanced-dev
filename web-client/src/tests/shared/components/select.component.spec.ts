import {HttpClientModule} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';
import {RouterModule} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {SelectComponent} from '../../../app/shared/components/select/select.component';
import {CustomCookieService} from '../../../app/shared/services/cookies/custom-cookie.service';
import {I18nTranslationService} from '../../../app/shared/services/i18n/i18n-translation.service';

describe('SelectComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule, RouterModule.forRoot([])],
            declarations: [SelectComponent],
            providers: [I18nTranslationService, CustomCookieService, CookieService]
        }).overrideComponent(SelectComponent, {}).compileComponents();
        fixture = TestBed.createComponent(SelectComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the SelectComponent component', async () => {
        expect(component).toBeTruthy();
    });
});
