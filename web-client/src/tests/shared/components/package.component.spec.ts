import {HttpClientModule} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {PackageComponent} from '../../../app/shared/components/package/package.component';
import {AlertService} from '../../../app/shared/services/alert/alert.service';
import {CustomCookieService} from '../../../app/shared/services/cookies/custom-cookie.service';
import {I18nTranslationService} from '../../../app/shared/services/i18n/i18n-translation.service';
import {ThemeModule} from '../../../app/shared/theme/theme.module';

describe('PackageComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ThemeModule, HttpClientModule, RouterModule.forRoot([]), FormsModule, ReactiveFormsModule],
            declarations: [PackageComponent],
            providers: [AlertService, I18nTranslationService, CustomCookieService, CookieService]
        }).overrideComponent(PackageComponent, {}).compileComponents();
        fixture = TestBed.createComponent(PackageComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the PackageComponent component', async () => {
        expect(component).toBeTruthy();
    });

    it('Should run ngOnInit()', async () => {
        component.package = {id: 1, name: 'PACKAGE_FREE'};
        expect(component.ngOnInit()).toBeUndefined();
    });
});
