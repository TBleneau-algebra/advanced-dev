import {TestBed} from '@angular/core/testing';
import {LoadableSpinnerComponent} from '../../../app/shared/components/loadable/loadable-spinner/loadable-spinner.component';
import {SpinnerComponent} from '../../../app/shared/components/loadable/loadable-spinner/spinner/spinner.component';

describe('LoadableSpinnerComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [LoadableSpinnerComponent, SpinnerComponent],
            providers: []
        }).overrideComponent(LoadableSpinnerComponent, {}).compileComponents();
        fixture = TestBed.createComponent(LoadableSpinnerComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the LoadableSpinnerComponent component', async () => {
        expect(component).toBeTruthy();
    });

    it('Should run ngOnInit()', async () => {
        expect(component.ngOnInit()).toBeUndefined();
    });
});
