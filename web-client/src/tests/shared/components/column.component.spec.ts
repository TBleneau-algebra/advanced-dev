import {HttpClientModule} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';
import {RouterModule} from '@angular/router';
import {NbThemeModule, NbThemeService} from '@nebular/theme';
import {ColumnComponent} from '../../../app/shared/components/column/column.component';
import {corporateStyle} from '../../../app/shared/theme/styles/corporate.style';
import {ThemeModule} from '../../../app/shared/theme/theme.module';

describe('ColumnComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ThemeModule, HttpClientModule, RouterModule.forRoot([])],
            declarations: [ColumnComponent],
            providers: [NbThemeService,
                ...NbThemeModule.forRoot(
                    {
                        name: 'corporate',
                    },
                    [corporateStyle],
                ).providers
            ]
        }).overrideComponent(ColumnComponent, {}).compileComponents();
        fixture = TestBed.createComponent(ColumnComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the ColumnComponent component', async () => {
        expect(component).toBeTruthy();
    });
});
