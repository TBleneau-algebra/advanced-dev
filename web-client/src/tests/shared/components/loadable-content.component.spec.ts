import {TestBed} from '@angular/core/testing';
import {LoadableContentComponent} from '../../../app/shared/components/loadable/loadable-content/loadable-content.component';

describe('LoadableContentComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [LoadableContentComponent],
            providers: []
        }).overrideComponent(LoadableContentComponent, {}).compileComponents();
        fixture = TestBed.createComponent(LoadableContentComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the LoadableContentComponent component', async () => {
        expect(component).toBeTruthy();
    });

    it('Should run ngOnInit()', async () => {
        expect(component.ngOnInit()).toBeUndefined();
    });
});
