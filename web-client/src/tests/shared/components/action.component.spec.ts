import {HttpClientModule} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';
import {RouterModule} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {ActionComponent} from '../../../app/shared/components/action/action.component';
import {AlertService} from '../../../app/shared/services/alert/alert.service';
import {CustomCookieService} from '../../../app/shared/services/cookies/custom-cookie.service';
import {I18nTranslationService} from '../../../app/shared/services/i18n/i18n-translation.service';
import {RequestAuthenticationService} from '../../../app/shared/services/request/request-authentication.service';
import {ThemeModule} from '../../../app/shared/theme/theme.module';

describe('ActionComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ThemeModule, HttpClientModule, RouterModule.forRoot([])],
            declarations: [ActionComponent],
            providers: [AlertService, RequestAuthenticationService, I18nTranslationService, CustomCookieService, CookieService]
        }).overrideComponent(ActionComponent, {}).compileComponents();
        fixture = TestBed.createComponent(ActionComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the ActionComponent component', async () => {
        expect(component).toBeTruthy();
    });

    it('Should run ngOnInit()', async () => {
        expect(component.ngOnInit()).toBeUndefined();
    });
});
