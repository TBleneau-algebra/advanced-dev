import {TestBed} from '@angular/core/testing';
import {LoadableDescriptionComponent} from '../../../app/shared/components/loadable/loadable-description/loadable-description.component';

describe('LoadableDescriptionComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [LoadableDescriptionComponent],
            providers: []
        }).overrideComponent(LoadableDescriptionComponent, {}).compileComponents();
        fixture = TestBed.createComponent(LoadableDescriptionComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the LoadableDescriptionComponent component', async () => {
        expect(component).toBeTruthy();
    });

    it('Should run ngOnInit()', async () => {
        expect(component.ngOnInit()).toBeUndefined();
    });
});
