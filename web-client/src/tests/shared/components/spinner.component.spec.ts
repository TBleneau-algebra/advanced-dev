import {TestBed} from '@angular/core/testing';
import {SpinnerComponent} from '../../../app/shared/components/loadable/loadable-spinner/spinner/spinner.component';


describe('SpinnerComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [SpinnerComponent],
            providers: []
        }).overrideComponent(SpinnerComponent, {}).compileComponents();
        fixture = TestBed.createComponent(SpinnerComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('Should create the SpinnerComponent component', async () => {
        expect(component).toBeTruthy();
    });

    it('Should run ngOnInit()', async () => {
        expect(component.ngOnInit()).toBeUndefined();
    });
});
