import {TestBed} from '@angular/core/testing';
import {AppModule} from '../../../app/app.module';
import {DatePipe} from '../../../app/shared/pipes/date.pipe';

describe('DatePipe', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [AppModule],
        providers: [DatePipe]
    }));

    it('Should create the DatePipe pipe', () => {
        const pipe: DatePipe = TestBed.get(DatePipe);
        expect(pipe).toBeTruthy();
    });

    it('Should run transform()', async () => {
        const pipe: DatePipe = TestBed.get(DatePipe);
        expect(pipe.transform('01011997', 'DDMMYYYY', 'en')).toBeTruthy();
    });
});
