import {CUSTOM_ELEMENTS_SCHEMA, Directive, Injectable, Input, NO_ERRORS_SCHEMA, Pipe, PipeTransform} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxImageCompressService} from 'ngx-image-compress';
import {Observable} from 'rxjs';
import {UploadPhotoComponent} from '../../app/pages/upload-photo/upload-photo.component';
import {CustomSubjectClass} from '../../app/shared/classes/rxjs/custom-subject.class';
import {AlertService} from '../../app/shared/services/alert/alert.service';
import {I18nClass} from '../../app/shared/services/i18n/i18n-class/i18n.class';
import {I18nTranslationService} from '../../app/shared/services/i18n/i18n-translation.service';
import {ImageFilterService} from '../../app/shared/services/image-filter/image-filter.service';
import {RequestService} from '../../app/shared/services/request/request.service';

@Injectable()
class MockI18nTranslationService {
    private _translation: CustomSubjectClass<I18nClass> = new CustomSubjectClass<I18nClass>();

    public getTranslationObservable(): Observable<I18nClass> {
        return this._translation.asObservable();
    }

    public getTranslation(): I18nClass {
        return this._translation.last;
    }
}

@Injectable()
class MockImageFilterService {
}

@Injectable()
class MockAlertService {
}

@Injectable()
class MockRequestService {
}

@Directive({selector: '[oneviewPermitted]'})
class OneviewPermittedDirective {
    @Input() oneviewPermitted;
}

@Pipe({name: 'translate'})
class TranslatePipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

@Pipe({name: 'phoneNumber'})
class PhoneNumberPipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

@Pipe({name: 'safeHtml'})
class SafeHtmlPipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

describe('UploadPhotoComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, ReactiveFormsModule],
            declarations: [
                UploadPhotoComponent,
                TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
                OneviewPermittedDirective
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
            providers: [
                NgxImageCompressService,
                {provide: I18nTranslationService, useClass: MockI18nTranslationService},
                {provide: ImageFilterService, useClass: MockImageFilterService},
                {provide: AlertService, useClass: MockAlertService},
                {provide: RequestService, useClass: MockRequestService}
            ]
        }).overrideComponent(UploadPhotoComponent, {}).compileComponents();
        fixture = TestBed.createComponent(UploadPhotoComponent);
        component = fixture.debugElement.componentInstance;
    });

    afterEach(() => {
        component.ngOnDestroy = function () {
        };
        fixture.destroy();
    });

    it('Should run constructor()', async () => {
        expect(component).toBeTruthy();
    });
});
