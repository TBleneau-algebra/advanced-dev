import {CUSTOM_ELEMENTS_SCHEMA, Directive, Input, NO_ERRORS_SCHEMA, Pipe, PipeTransform} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdministrationOptionComponent} from '../../app/pages/administration/administration-option/administration-option.component';

@Directive({selector: '[oneviewPermitted]'})
class OneviewPermittedDirective {
    @Input() oneviewPermitted;
}

@Pipe({name: 'translate'})
class TranslatePipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

@Pipe({name: 'phoneNumber'})
class PhoneNumberPipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

@Pipe({name: 'safeHtml'})
class SafeHtmlPipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

describe('AdministrationOptionComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, ReactiveFormsModule],
            declarations: [
                AdministrationOptionComponent,
                TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
                OneviewPermittedDirective
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
            providers: []
        }).overrideComponent(AdministrationOptionComponent, {}).compileComponents();
        fixture = TestBed.createComponent(AdministrationOptionComponent);
        component = fixture.debugElement.componentInstance;
    });

    afterEach(() => {
        component.ngOnDestroy = function () {
        };
        fixture.destroy();
    });

    it('Should run constructor()', async () => {
        expect(component).toBeTruthy();
    });
});
