import {CUSTOM_ELEMENTS_SCHEMA, Directive, Injectable, Input, NO_ERRORS_SCHEMA, Pipe, PipeTransform} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxImageCompressService} from 'ngx-image-compress';
import {Observable, of as observableOf} from 'rxjs';
import {SettingProfileComponent} from '../../app/pages/settings/setting-profile/setting-profile.component';
import {CustomSubjectClass} from '../../app/shared/classes/rxjs/custom-subject.class';
import {AlertService} from '../../app/shared/services/alert/alert.service';
import {I18nClass} from '../../app/shared/services/i18n/i18n-class/i18n.class';
import {I18nTranslationService} from '../../app/shared/services/i18n/i18n-translation.service';
import {RequestAuthenticationService} from '../../app/shared/services/request/request-authentication.service';

@Injectable()
class MockRouter {
    navigate() {
    };
}

@Injectable()
class MockRequestAuthenticationService {
}

@Injectable()
class MockAlertService {
}

@Injectable()
class MockI18nTranslationService {
    private _translation: CustomSubjectClass<I18nClass> = new CustomSubjectClass<I18nClass>();

    public getTranslationObservable(): Observable<I18nClass> {
        return this._translation.asObservable();
    }

    public getTranslation(): I18nClass {
        return this._translation.last;
    }
}

@Directive({selector: '[oneviewPermitted]'})
class OneviewPermittedDirective {
    @Input() oneviewPermitted;
}

@Pipe({name: 'translate'})
class TranslatePipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

@Pipe({name: 'phoneNumber'})
class PhoneNumberPipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

@Pipe({name: 'safeHtml'})
class SafeHtmlPipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

describe('SettingProfileComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, ReactiveFormsModule],
            declarations: [
                SettingProfileComponent,
                TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
                OneviewPermittedDirective
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
            providers: [
                FormBuilder,
                {provide: Router, useClass: MockRouter},
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {url: 'url', params: {}, queryParams: {}, data: {}},
                        url: observableOf('url'),
                        params: observableOf({}),
                        queryParams: observableOf({}),
                        fragment: observableOf('fragment'),
                        data: observableOf({})
                    }
                },
                {provide: RequestAuthenticationService, useClass: MockRequestAuthenticationService},
                {provide: AlertService, useClass: MockAlertService},
                NgxImageCompressService,
                {provide: I18nTranslationService, useClass: MockI18nTranslationService}
            ]
        }).overrideComponent(SettingProfileComponent, {}).compileComponents();
        fixture = TestBed.createComponent(SettingProfileComponent);
        component = fixture.debugElement.componentInstance;
    });

    afterEach(() => {
        component.ngOnDestroy = function () {
        };
        fixture.destroy();
    });

    it('Should run constructor()', async () => {
        expect(component).toBeTruthy();
    });
});
