import {CUSTOM_ELEMENTS_SCHEMA, Directive, Injectable, Input, NO_ERRORS_SCHEMA, Pipe, PipeTransform} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Observable} from 'rxjs';
import {PagesComponent} from '../../app/pages/pages.component';
import {CustomSubjectClass} from '../../app/shared/classes/rxjs/custom-subject.class';
import {I18nClass} from '../../app/shared/services/i18n/i18n-class/i18n.class';
import {I18nTranslationService} from '../../app/shared/services/i18n/i18n-translation.service';
import {LoaderService} from '../../app/shared/services/loader-spinner/loader.service';
import {RequestAuthenticationService} from '../../app/shared/services/request/request-authentication.service';

@Injectable()
class MockRequestAuthenticationService {
}

@Injectable()
class MockI18nTranslationService {

    private _translation: CustomSubjectClass<I18nClass> = new CustomSubjectClass<I18nClass>();

    public getTranslationObservable(): Observable<I18nClass> {
        return this._translation.asObservable();
    }

    public getTranslation(): I18nClass {
        return this._translation.last;
    }
}

@Injectable()
class MockLoaderService {
}

@Directive({selector: '[oneviewPermitted]'})
class OneviewPermittedDirective {
    @Input() oneviewPermitted;
}

@Pipe({name: 'translate'})
class TranslatePipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

@Pipe({name: 'phoneNumber'})
class PhoneNumberPipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

@Pipe({name: 'safeHtml'})
class SafeHtmlPipe implements PipeTransform {
    transform(value) {
        return value;
    }
}

describe('PagesComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, ReactiveFormsModule],
            declarations: [
                PagesComponent,
                TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
                OneviewPermittedDirective
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
            providers: [
                {provide: RequestAuthenticationService, useClass: MockRequestAuthenticationService},
                {provide: I18nTranslationService, useClass: MockI18nTranslationService},
                {provide: LoaderService, useClass: MockLoaderService}
            ]
        }).overrideComponent(PagesComponent, {}).compileComponents();
        fixture = TestBed.createComponent(PagesComponent);
        component = fixture.debugElement.componentInstance;
    });

    afterEach(() => {
        component.ngOnDestroy = function () {
        };
        fixture.destroy();
    });

    it('Should run constructor()', async () => {
        expect(component).toBeTruthy();
    });
});
