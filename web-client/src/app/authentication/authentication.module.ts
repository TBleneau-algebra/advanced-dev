/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';

/**
 * Import of Nebular's modules
 */
import {NbCardModule, NbLayoutModule} from '@nebular/theme';
import {NbAuthModule} from '@nebular/auth';

/**
 * Import of application's modules
 */
import {AuthenticationRoutingModule} from './authentication-routing.module';
import {LoginModule} from './login/login.module';
import {ComponentsModule} from '../shared/components/components.module';

/**
 * Import of application's components
 */
import {AuthenticationComponent} from './authentication.component';

@NgModule({
    imports: [
        AuthenticationRoutingModule,
        NbLayoutModule,
        NbAuthModule,
        NbCardModule,
        LoginModule,
        ComponentsModule
    ],
    declarations: [
        AuthenticationComponent
    ]
})
export class AuthenticationModule {
}
