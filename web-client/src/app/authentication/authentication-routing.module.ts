/**
 * Import of Angular's modules
 */
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

/**
 * Import of Nebular's modules
 */
import {
    NbLogoutComponent,
} from '@nebular/auth';

/**
 * Import of application's components
 */
import {LoginComponent} from './login/login.component';
import {AuthenticationComponent} from './authentication.component';
import {RegisterComponent} from './register/register.component';

const routes: Routes = [
    {
        path: '',
        component: AuthenticationComponent,
        children: [
            {
                path: '',
                redirectTo: 'login',
                pathMatch: 'full'
            },
            {
                path: 'login',
                component: LoginComponent,
            },
            {
                path: 'register',
                component: RegisterComponent,
            },
            {
                path: '**',
                redirectTo: '',
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [
        RouterModule
    ]
})
export class AuthenticationRoutingModule {
}
