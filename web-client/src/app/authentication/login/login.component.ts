import {Component, OnInit} from '@angular/core';
import {UserClass} from '../../shared/classes/models/user.class';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RequestAuthenticationService} from '../../shared/services/request/request-authentication.service';
import {Router} from '@angular/router';
import {AlertService} from '../../shared/services/alert/alert.service';
import {I18nTranslatableClass} from '../../shared/services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../shared/services/i18n/i18n-translation.service';
import {LoaderSemaphoreClass} from '../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent extends I18nTranslatableClass implements OnInit {

    /**
     * @description User model used to save information
     */
    private _user: UserClass;

    /**
     * @description Tracks the value and validity state of a group of FormControl instances
     */
    private _formGroup: FormGroup;

    /**
     * @description Constructor of LoginComponent component
     *
     * The constructor creates an instance of the LoginComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
     * @param authenticationService A reference to the authentication service of the application
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param alertService A reference to the alert service of the application
     * @param translationService A reference to the i18n translation service of the application
     */
    constructor(private formBuilder: FormBuilder, private authenticationService: RequestAuthenticationService,
                private router: Router, private alertService: AlertService,
                translationService: I18nTranslationService) {
        super(translationService);
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.user = new UserClass();

        this.formGroup = this.formBuilder.group(
            {
                username: [null, Validators.compose([
                    Validators.required,
                ])],
                password: [null, Validators.compose([
                    Validators.required,
                ])]
            }
        );
    }

    /**
     * @description This method allows a guest user to access the application
     */
    guest(): void {
        this.authenticationService.setRoles();
        localStorage.setItem('USER_ROLE', JSON.stringify(this.authenticationService.roles));

        this.router.navigate(['/pages']).then(response => {
        });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It allows to sign in a user of the application
     *
     * It calls the method Post of the RequestAuthenticationService service
     */
    login(): void {
        this.authenticationService.activeLoader(new LoaderSemaphoreClass(1));

        this.authenticationService.post(this.user, '/user/login', null)
            .then((response) => {
                (response.responseStatus !== 200) ? this.errorEvent(response.responseData) : this.successEvent(response.responseData);
            })
            .catch((error) => {
                this.errorEvent(error.responseError);
            });
    }

    /**
     * @description This method displays the error message received by the API
     *
     * @param data Data with errors messages sent by the API
     */
    errorEvent(data: any) {
        if (data && data.hasOwnProperty('errors')) {
            for (const item of (data.errors as Array<any>)) {
                this.alertService.error(this.translate('ERROR/' + item));
            }
        } else {
            this.alertService.error(this.translate('ERROR/API_CONNECTION'));
        }
    }

    /**
     * @description This method displays a success event received via the API
     *
     * @param responseData Response sent by the API
     */
    successEvent(responseData: any): void {
        this.authenticationService.user = responseData.data;

        if (responseData.hasOwnProperty('data')) {
            this.authenticationService.setRoles(responseData.data.roles);
            localStorage.setItem('USER_ROLE', JSON.stringify(this.authenticationService.roles));
        }
        this.router.navigate(['/pages']).then(response => {
            this.alertService.success(this.translate('SUCCESS/USER_AUTHENTICATED'));
        });
    }

    /**
     * @description This method checks if a HTML element is focused
     *
     * @param value HTML element to check
     */
    isFocused(value: HTMLElement): boolean {
        return document.activeElement === value;
    }

    /**
     * @description The method allows you to retrieve the user model in the LoginComponent
     */
    get user(): UserClass {
        return this._user;
    }

    /**
     * @description The method allows you to assign the user model in the LoginComponent
     *
     * @param value Value of the user model in the LoginComponent
     */
    set user(value: UserClass) {
        this._user = value;
    }

    /**
     * @description The method allows you to retrieve the variable which tracks the value and validity state of a group of
     * FormControl instances
     */
    get formGroup(): FormGroup {
        return this._formGroup;
    }

    /**
     * @description The method allows you to assign the variable which tracks the value and validity state of a group of
     * FormControl instances
     *
     * @param value Value of the variable which tracks the value and validity state of a group of FormControl instances
     */
    set formGroup(value: FormGroup) {
        this._formGroup = value;
    }
}
