/**
 * Import of Angular's module
 */
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

/**
 * Import of Nebular's module
 */
import {NbAlertModule, NbButtonModule, NbCardModule, NbCheckboxModule, NbIconModule, NbInputModule} from '@nebular/theme';

/**
 * Import of application's modules
 */
import {ThemeModule} from '../../shared/theme/theme.module';

/**
 * Import of application's components
 */
import {LoginComponent} from './login.component';

@NgModule({
    imports: [
        ThemeModule,
        NbIconModule,
        NbCardModule,
        NbButtonModule,
        NbAlertModule,
        NbCheckboxModule,
        NbInputModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
    ],
    declarations: [
        LoginComponent,
    ]
})
export class LoginModule {
}
