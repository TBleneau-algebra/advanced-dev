import {Component, NgZone} from '@angular/core';
import {I18nTranslationService} from '../shared/services/i18n/i18n-translation.service';
import {LoaderService} from '../shared/services/loader-spinner/loader.service';

@Component({
    selector: 'app-authentication',
    styleUrls: ['./authentication.component.scss'],
    template: `
        <nb-layout>
            <nb-layout-column>
                <nb-card>
                    <nb-card-body>
                        <div class="authentication-language">
                            <div class="m-0 h-100 text-right">
                                <img src="/assets/img/languages/flag_english.png"
                                     [class.language-selected]="selected('en')"
                                     (click)="switch('en')">
                                <img src="/assets/img/languages/flag_french.png"
                                     [class.language-selected]="selected('fr')"
                                     (click)="switch('fr')">
                            </div>
                        </div>
                        <nb-auth-block>
                            <router-outlet></router-outlet>
                        </nb-auth-block>
                    </nb-card-body>
                </nb-card>
            </nb-layout-column>
        </nb-layout>`
})
export class AuthenticationComponent {

    /**
     * @description Constructor of AuthenticationComponent component
     *
     * The constructor creates an instance of the AuthenticationComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param translationService A reference to the i18n translation service of the application
     * @param zone An injectable service for executing work inside or outside of the Angular zone.
     * @param loaderService A reference to the loader service of the application
     */
    constructor(private translationService: I18nTranslationService, private zone: NgZone, private loaderService: LoaderService) {
    }

    /**
     * @description A method to change the language of the platform and reload the Angular zone with ngZone
     *
     * @param value The new assigned value for the translation service's language
     */
    switch(value: string): void {
        if (this.translationService.lang !== value) {
            this.translationService.changeLang(value);
            this.translationService.reloadTranslation()
                .then(() => {
                    this.zone.run(() => {
                    });
                });
        }
    }

    /**
     * @description The method returns a boolean based on the current language of the application.
     *
     * @param value Value to compare
     */
    selected(value: string): boolean {
        return value === this.translationService.lang;
    }

}
