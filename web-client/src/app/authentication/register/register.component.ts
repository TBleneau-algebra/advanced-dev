import {AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {UserClass} from '../../shared/classes/models/user.class';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ValidatorClass} from '../../shared/classes/validator/validator.class';
import {Router} from '@angular/router';
import {RequestAuthenticationService} from '../../shared/services/request/request-authentication.service';
import {AlertService} from '../../shared/services/alert/alert.service';
import {PackageComponent} from '../../shared/components/package/package.component';
import {RequestParamsInterface} from '../../shared/services/request/params/request-params.interface';
import {RequestParamsClass} from '../../shared/services/request/params/request-params.class';
import {I18nTranslatableClass} from '../../shared/services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../shared/services/i18n/i18n-translation.service';
import {LoaderSemaphoreClass} from '../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html'
})
export class RegisterComponent extends I18nTranslatableClass implements OnInit, OnDestroy, AfterViewInit {

    /**
     * @description User model used to save information
     */
    private _user: UserClass;

    /**
     * @description Tracks the value and validity state of a group of FormControl instances
     */
    private _formGroup: FormGroup;

    /**
     * @description A QueryList variable of the components of type ActionComponent from the DOM
     * of content.
     *
     * Each time a child item is added, deleted or moved, the list is updated and changes are made
     * of observables in the list issue a new value.
     */
    @ViewChildren(PackageComponent) packageComponent !: QueryList<PackageComponent>;

    /**
     * @description Constructor of RegisterComponent component
     *
     * The constructor creates an instance of the RegisterComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
     * @param authenticationService A reference to the authentication service of the application
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param alertService A reference to the alert service of the application
     * @param translationService A reference to the i18n translation service of the application
     */
    constructor(private formBuilder: FormBuilder, private authenticationService: RequestAuthenticationService, private router: Router,
                private alertService: AlertService, translationService: I18nTranslationService) {
        super(translationService);
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        document.getElementsByTagName('nb-auth-block').item(0).setAttribute('style', 'max-width: 50em');

        this.user = new UserClass();
        this.formsInit();
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular has finished to initialize the view of a component.
     * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterViewInit(): void {
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        document.getElementsByTagName('nb-auth-block').item(0).removeAttribute('style');
    }

    /**
     * @description This method allows a guest user to access the application
     */
    guest(): void {
        this.authenticationService.setRoles();
        localStorage.setItem('USER_ROLE', JSON.stringify(this.authenticationService.roles));

        this.router.navigate(['/pages']).then(response => {
        });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It allows to register a user of the application
     *
     * It calls the method Post of the RequestAuthenticationService service
     */
    register(): void {
        const params: RequestParamsInterface = new RequestParamsClass();

        this.authenticationService.activeLoader(new LoaderSemaphoreClass(1));

        this.authenticationService.post(this.user, '/user/register', params)
            .then((response) => {
                (response.responseStatus !== 201) ? this.errorEvent(response.responseData) : this.successEvent(response.responseData);
            })
            .catch((error) => {
                this.errorEvent(error.responseError);
            });
    }

    /**
     * @description This method displays the error message received by the API
     *
     * @param data Data with errors messages sent by the API
     */
    errorEvent(data: any) {
        if (data.hasOwnProperty('errors')) {
            for (const item of (data.errors as Array<any>)) {
                this.alertService.error(this.translate('ERROR/' + item));
            }
        } else {
            this.alertService.error(this.translate('ERROR/API_CONNECTION'));
        }
    }

    /**
     * @description This method displays a success event and uses the Angular router to navigate through the application
     *
     * @param data Response sent by the API
     */
    successEvent(data: any): void {
        this.router.navigate(['/authentication/login']).then(response => {
            this.alertService.success(this.translate('SUCCESS/USER_CREATED'));
        });
    }

    /**
     * @description The method is used to initialize the state of the values of a group of FormControl instances
     */
    private formsInit(): void {
        const regExp = new RegExp('^((?:\\w|[\\-_ ](?![\\-_ ])|[\\u00C0\\u00C1\\u00C2\\u00C3\\u00C4\\u00C5\\u00C6\\u00C7\\u00C8\\' +
            'u00C9\\u00CA\\u00CB\\u00CC\\u00CD\\u00CE\\u00CF\\u00D0\\u00D1\\u00D2\\u00D3\\u00D4\\u00D5\\u00D6\\u00D8\\u00D9\\u00DA\\u00DB' +
            '\\u00DC\\u00DD\\u00DF\\u00E0\\u00E1\\u00E2\\u00E3\\u00E4\\u00E5\\u00E6\\u00E7\\u00E8\\u00E9\\u00EA\\u00EB\\u00EC\\u00ED\\u00' +
            'EE\\u00EF\\u00F0\\u00F1\\u00F2\\u00F3\\u00F4\\u00F5\\u00F6\\u00F9\\u00FA\\u00FB\\u00FC\\u00FD\\u00FF\\u0153])+)$', 'i');

        this.formGroup = this.formBuilder.group(
            {
                name: [null, Validators.compose([
                    Validators.required,
                    Validators.pattern(regExp)
                ])],
                lastname: [null, Validators.compose([
                    Validators.required,
                    Validators.pattern(regExp)
                ])],
                email: [null, Validators.compose([
                    Validators.required,
                    Validators.email
                ])],
                username: [null, Validators.compose([
                    Validators.required
                ])],
                password: [null, Validators.compose([
                    Validators.required,
                    ValidatorClass.patternValidator(/\d/, {hasNumber: true}),
                    ValidatorClass.patternValidator(/[A-Z]/, {hasCapitalCase: true}),
                    ValidatorClass.patternValidator(/[a-z]/, {hasSmallCase: true}),
                    ValidatorClass.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/, {hasSpecialCharacters: true}),
                    Validators.minLength(8),
                    Validators.maxLength(64)
                ])]
            }
        );
    }

    /**
     * @description This method checks if a HTML element is focused
     *
     * @param value HTML element to check
     */
    isFocused(value: HTMLElement): boolean {
        return document.activeElement === value;
    }

    /**
     * @description The method allows you to retrieve the user model in the RegisterComponent
     */
    get user(): UserClass {
        return this._user;
    }

    /**
     * @description The method allows you to assign the user model in the RegisterComponent
     *
     * @param value Value of the user model in the RegisterComponent
     */
    set user(value: UserClass) {
        this._user = value;
    }

    /**
     * @description The method allows you to retrieve the variable which tracks the value and validity state of a group of
     * FormControl instances
     */
    get formGroup(): FormGroup {
        return this._formGroup;
    }

    /**
     * @description The method allows you to assign the variable which tracks the value and validity state of a group of
     * FormControl instances
     *
     * @param value Value of the variable which tracks the value and validity state of a group of FormControl instances
     */
    set formGroup(value: FormGroup) {
        this._formGroup = value;
    }
}
