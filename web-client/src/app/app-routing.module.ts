/**
 * Import of Angular's modules
 */
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthenticationGuardService} from './shared/services/guards/authentication-guard.service';

const routes: Routes = [
    {
        path: 'pages',
        canActivate: [AuthenticationGuardService],
        loadChildren: () => import('./pages/pages.module')
            .then(m => m.PagesModule),
    },
    {
        path: 'authentication',
        canActivate: [AuthenticationGuardService],
        loadChildren: () => import('./authentication/authentication.module')
            .then(m => m.AuthenticationModule),
    },
    {path: '', redirectTo: 'pages', pathMatch: 'full'},
    {path: '**', redirectTo: 'pages'},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {useHash: false})
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
