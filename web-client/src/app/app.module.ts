/**
 * Import of Angular's modules
 */
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
/**
 * Import of Nebular's modules
 */
import {NbDatepickerModule, NbDialogModule, NbMenuModule, NbSidebarModule, NbToastrModule, NbWindowModule} from '@nebular/theme';
import {NbAuthModule} from '@nebular/auth';
import {NbRoleProvider, NbSecurityModule} from '@nebular/security';
/**
 * Import of application's modules
 */
import {ThemeModule} from './shared/theme/theme.module';
import {AppRoutingModule} from './app-routing.module';
import {ComponentsModule} from './shared/components/components.module';
/**
 * Import of application's components
 */
import {AppComponent} from './app.component';
/**
 * Import of application's services
 */
import {HttpInterceptorService} from './shared/services/http-interceptor/http-interceptor.service';
import {RequestAuthenticationService} from './shared/services/request/request-authentication.service';
import {I18nTranslationService} from './shared/services/i18n/i18n-translation.service';
/**
 * Import of NgxCookieService service
 */
import {CookieService} from 'ngx-cookie-service';
/**
 * Import of NgxImageCompressService service
 */
import {NgxImageCompressService} from 'ngx-image-compress';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,
        ComponentsModule,
        ThemeModule.forRoot(),
        NbSidebarModule.forRoot(),
        NbMenuModule.forRoot(),
        NbDatepickerModule.forRoot(),
        NbDialogModule.forRoot(),
        NbWindowModule.forRoot(),
        NbToastrModule.forRoot(),
        NbAuthModule.forRoot(),
        NbSecurityModule.forRoot()
    ],
    bootstrap: [AppComponent],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true},
        {provide: NbRoleProvider, useClass: RequestAuthenticationService},
        CookieService,
        NgxImageCompressService,
        I18nTranslationService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
