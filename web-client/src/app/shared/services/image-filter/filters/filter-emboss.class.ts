import {FilterInterface} from './filter.interface';

export class FilterEmbossClass implements FilterInterface {

    /**
     * @description Constructor of FilterEmbossClass class
     *
     * The constructor creates an instance of the FilterEmbossClass class and specifies the default values
     * the input and output variables of the class.
     */
    constructor() {
    }
    
    /**
     * @description This method applies the filter emboss
     *
     * @param editor Editor which allows you to add filter to the image
     * @param image MarvinImage object with data url
     */
    apply(editor: any, image: any): any {
        if (image.imageData) {
            const imageFilter: any = new editor.marvinImage(image.getWidth(), image.getHeight());

            editor.marvin.emboss(image, imageFilter);
            return imageFilter;
        }
    }

    /**
     * @description This method allows you to draw an image in the canvas passed in parameter
     *
     * @param canvas HTML canvas element
     * @param image MarvinImage object with data url
     */
    draw(image: any, canvas: HTMLCanvasElement): string {
        if (image !== undefined && image.imageData) {
            image.draw(canvas);
            return canvas.toDataURL();
        }
    }
}
