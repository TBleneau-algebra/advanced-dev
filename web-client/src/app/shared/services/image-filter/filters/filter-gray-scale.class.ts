import {FilterInterface} from './filter.interface';

export class FilterGrayScaleClass implements FilterInterface {

    /**
     * @description Constructor of FilterGrayScaleClass class
     *
     * The constructor creates an instance of the FilterGrayScaleClass class and specifies the default values
     * the input and output variables of the class.
     */
    constructor() {
    }
    
    /**
     * @description This method applies the filter gray scale
     *
     * @param editor Editor which allows you to add filter to the image
     * @param image MarvinImage object with data url
     */
    apply(editor: any, image: any): any {
        if (image.imageData) {
            const imageFilter: any = new editor.marvinImage(image.getWidth(), image.getHeight());

            editor.marvin.grayScale(image, imageFilter);
            return imageFilter;
        }
    }

    /**
     * @description This method allows you to draw an image in the canvas passed in parameter
     *
     * @param canvas HTML canvas element
     * @param image MarvinImage object with data url
     */
    draw(image: any, canvas: HTMLCanvasElement): string {
        if (image !== undefined && image.imageData) {
            image.draw(canvas);
            return canvas.toDataURL();
        }
    }
}
