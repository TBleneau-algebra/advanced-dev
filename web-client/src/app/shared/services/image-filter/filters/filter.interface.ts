export interface FilterInterface {

    /**
     * @description This method applies the filter selected
     *
     * @param editor Editor which allows you to add filter to the image
     * @param image MarvinImage object with data url
     */
    apply(editor: any, image: any): any;

    /**
     * @description This method allows you to draw an image in the canvas passed in parameter
     *
     * @param canvas HTML canvas element
     * @param image MarvinImage object with data url
     */
    draw(image: any, canvas: HTMLCanvasElement): string;
}
