import {Injectable} from '@angular/core';
import {PictureClass} from '../../classes/models/picture.class';
import {ImageFilterFactoryClass} from './factory/image-filter-factory.class';
import {FilterDecoratorApplyClass} from './decorator/filter-decorator-apply.class';
import {FilterDecoratorInterface} from './decorator/filter-decorator.interface';
import {FilterDecoratorDrawClass} from './decorator/filter-decorator-draw.class';

@Injectable({
    providedIn: 'root'
})
export class ImageFilterService {

    /**
     * @description Current value of the filter
     */
    private _current: number = -1;

    /**
     * @description Constructor of ImageFilterService service
     *
     * The constructor creates an instance of the ImageFilterService service and specifies the default values
     * the input and output variables of the service.
     */
    constructor() {
    }

    /**
     * @description This method applies the filter passed in parameter
     *
     * @param value Filter value
     * @param editor Editor which allows you to add filter to the image
     * @param image MarvinImage object with data url
     * @param canvas HTML canvas element
     */
    apply(value: number, editor: any, image: any, canvas: HTMLCanvasElement): any {
        const decoratorInterface: FilterDecoratorInterface =
            new FilterDecoratorApplyClass(ImageFilterFactoryClass.create(value));

        return decoratorInterface.apply(editor, image);
    }

    /**
     * @description This method allows you to draw an image in the canvas passed in parameter
     *
     * @param value Filter value
     * @param canvas HTML canvas element
     * @param image MarvinImage object with data url
     */
    draw(value: number, image: any, canvas: HTMLCanvasElement): string {
        const decoratorInterface: FilterDecoratorInterface =
            new FilterDecoratorDrawClass(ImageFilterFactoryClass.create(value));

        return decoratorInterface.draw(image, canvas);
    }

    /**
     * @description This method is used to serialize the object to be sent to the API
     *
     * @param model object to serialize
     * @param property property
     * @param canvas HTML canvas element
     */
    serialize(model: PictureClass, property: string, canvas: HTMLCanvasElement): void {
        if (model[property]) {
            model.width = model[property].imageData.width;
            model.height = model[property].imageData.height;

            canvas.width = model.width;
            canvas.height = model.height;
            this.draw(-1, model[property], canvas);

            model[property] = canvas.toDataURL(model.format);
        }
    }

    /**
     * @description This method allows you to retrieve the current value of the filter
     */
    get current(): number {
        return this._current;
    }

    /**
     * @description This method allows you to assign the current value of the filter
     *
     * @param value Value of the new current filter
     */
    set current(value: number) {
        this._current = value;
    }
}
