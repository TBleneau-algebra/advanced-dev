import {FilterInterface} from '../filters/filter.interface';

export interface FilterDecoratorInterface extends FilterInterface {
}
