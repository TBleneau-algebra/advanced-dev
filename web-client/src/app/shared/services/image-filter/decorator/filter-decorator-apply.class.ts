import {FilterInterface} from '../filters/filter.interface';
import {FilterDecoratorInterface} from './filter-decorator.interface';

export class FilterDecoratorApplyClass implements FilterDecoratorInterface {

    /**
     * @description Constructor of FilterDecoratorApplyClass class
     *
     * The constructor creates an instance of the FilterDecoratorApplyClass class and specifies the default values
     * the input and output variables of the class.
     *
     * @param ImageFilterInterface A reference to the filter object
     */
    constructor(private ImageFilterInterface: FilterInterface) {
    }

    /**
     * @description This method applies the filter passed in parameter
     *
     * @param editor Editor which allows you to add filter to the image
     * @param image MarvinImage object with data url
     */
    apply(editor: any, image: any): any {
        return this.ImageFilterInterface.apply(editor, image);
    }

    /**
     * @description This method allows you to draw an image in the canvas passed in parameter
     *
     * @param canvas HTML canvas element
     * @param image MarvinImage object with data url
     */
    draw(image: any, canvas: HTMLCanvasElement): string {
        return this.ImageFilterInterface.draw(image, canvas);
    }
}
