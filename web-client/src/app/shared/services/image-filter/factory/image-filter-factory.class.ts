import {FilterInterface} from '../filters/filter.interface';
import {FilterGrayScaleClass} from '../filters/filter-gray-scale.class';
import {FilterBlackWhiteClass} from '../filters/filter-black-white.class';
import {FilterSepiaClass} from '../filters/filter-sepia.class';
import {FilterInvertClass} from '../filters/filter-invert.class';
import {FilterEmbossClass} from '../filters/filter-emboss.class';
import {FilterHalftoneClass} from '../filters/filter-halftone.class';
import {FilterEdgeClass} from '../filters/filter-edge.class';
import {FilterHandDrawingClass} from '../filters/filter-hand-drawing.class';

export abstract class ImageFilterFactoryClass {

    /**
     * @description This method allows you to create a filter based on the filter ID passed in parameter
     *
     * @param id Filter identifier
     */
    static create(id: number): FilterInterface {
        switch (id) {
            case 1:
                return new FilterBlackWhiteClass();
            case 2:
                return new FilterSepiaClass();
            case 3:
                return new FilterEmbossClass();
            case 4:
                return new FilterHalftoneClass();
            case 5:
                return new FilterEdgeClass();
            case 6:
                return new FilterInvertClass();
            case 7:
                return new FilterHandDrawingClass();
            default:
                return new FilterGrayScaleClass();
        }
    }
}
