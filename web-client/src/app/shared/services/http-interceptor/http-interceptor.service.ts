import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

    /**
     * @description Constructor of HttpInterceptorService service
     *
     * The constructor creates an instance of the HttpInterceptorService service and specifies the default values
     * the input and output variables of the service.
     */
    constructor() {
    }

    /**
     * @description This method intercepts an http request to add information such as headers, JavaScript cookies...
     *
     * @param request An outgoing HTTP request with an optional typed body.
     * @param next Transforms an HttpRequest into a stream of HttpEvents, one of which will likely be a HttpResponse.
     * @return Observable<HttpEvent<any>>
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({withCredentials: true});
        request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
        return next.handle(request);
    }
}
