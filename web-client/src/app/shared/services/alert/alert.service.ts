import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import Swal, {SweetAlertType} from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    /**
     * @description Constructor of AlertService service
     *
     * The constructor creates an instance of the AlertService service and specifies the default values
     * the input and output variables of the service.
     */
    constructor() {
    }

    /**
     * @description This method ask the user to confirm something or not
     *
     * @param question The question to ask
     * @param acceptText The accept button label
     * @param denyText The deny button label
     * @param cancelColor The deny button color
     */
    public confirm(question: string, acceptText: string = 'Oui', denyText: string = 'Non', cancelColor: string = '#d33'):
        Observable<boolean> {
        const subj = new Subject<boolean>();
        const ref = Swal.fire({
            text: question,
            type: 'question',
            confirmButtonText: acceptText,
            showCancelButton: true,
            cancelButtonText: denyText,
            cancelButtonColor: cancelColor,
        });

        ref.then((val) => {
            if (val.value === true) {
                subj.next(true);
            } else {
                subj.next(false);
            }
        }).catch(subj.error);

        return subj.asObservable();
    }

    /**
     * @description This method ask a data to the user
     *
     * @param question The question to ask
     * @param submitText The submit button label
     * @param dismissText The cancel or dismiss button label
     */
    public question(question: string,
                    submitText: string = 'Envoyer',
                    dismissText: string = 'Annuler'): Observable<string> {
        const subj = new Subject<string>();
        const ref = Swal.fire({
            text: question,
            input: 'text',
            confirmButtonText: submitText,
            showCancelButton: true,
            cancelButtonText: dismissText,
        });

        ref.then((val) => {
            if (val.value) {
                subj.next(val.value);
            } else {
                subj.next(null);
            }
        }).catch(subj.error);

        return subj.asObservable();
    }

    /**
     * @description This method throw an alert with title as title, message as text body and type to specify the layout to use.
     *
     * @param message The message to display
     * @param title The title of the message
     * @param type The type of the message
     */
    private _alert(message: string, title: string = null, type: SweetAlertType): Observable<boolean> {
        const subj = new Subject<boolean>();
        const ref = Swal.fire({
            titleText: title,
            text: message,
            timer: (type === 'success') ? 2000 : undefined,
            showConfirmButton: (type !== 'success' && type !== 'error'),
            type
        });

        ref.then((val) => {
            if (val.value) {
                subj.next(val.value);
            } else {
                subj.next(null);
            }
        }).catch(subj.error);
        return subj.asObservable();
    }

    /**
     * @description This method is a shortcut for _alert(xx, xx, 'error')
     *
     * @param message The message to display
     * @param title The title of the message
     */
    public error(message: string, title: string = null): Observable<boolean> {
        return this._alert(message, title, 'error');
    }

    /**
     * @description This method is a shortcut for _alert(xx, xx, 'warning')
     *
     * @param message The message to display
     * @param title The title of the message
     */
    public warning(message: string, title: string = null): Observable<boolean> {
        return this._alert(message, title, 'warning');
    }

    /**
     * @description This method is a shortcut for _alert(xx, xx, 'info')
     *
     * @param message The message to display
     * @param title The title of the message
     */
    public info(message: string, title: string = null): Observable<boolean> {
        return this._alert(message, title, 'info');
    }

    /**
     * @description This method is a shortcut for _alert(xx, xx, 'success')
     *
     * @param message The message to display
     * @param title The title of the message
     */
    public success(message: string, title: string = null): Observable<boolean> {
        return this._alert(message, title, 'success');
    }

    /**
     * @description This method displays an alert with an image dialog
     *
     * @param path Path of the image to display
     * @param width Width of the image to display
     * @param height Height of the image to display
     */
    public image(path: string, width: number, height: number): Observable<boolean> {
        const subj = new Subject<boolean>();
        const ref = Swal.fire({
            imageUrl: path,
            imageWidth: width,
            imageHeight: height,
            customClass: 'swal-popup-img',
            showCloseButton: true,
            showConfirmButton: false
        });

        ref.then((val) => {
            if (val.value) {
                subj.next(val.value);
            } else {
                subj.next(null);
            }
        }).catch(subj.error);
        return subj.asObservable();
    }

    /**
     * @description This method displays an alert with a html template
     *
     * @param html Html template
     */
    public html(html: string): Observable<boolean> {
        const subj = new Subject<boolean>();
        const ref = Swal.fire({
            html: html,
            customClass: 'swal-popup-html',
            showCloseButton: true,
            showConfirmButton: false
        });

        ref.then((val) => {
            if (val.value) {
                subj.next(val.value);
            } else {
                subj.next(null);
            }
        }).catch(subj.error);
        return subj.asObservable();
    }
}
