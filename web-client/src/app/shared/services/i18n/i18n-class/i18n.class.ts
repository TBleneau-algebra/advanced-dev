import {I18nTranslatableClass} from './i18n-translatable.class';

export class I18nClass {

    /**
     * @description Data to translate
     */
    private readonly translationCollection: I18nTranslatableClass;

    /**
     * @description Current language in which the application is translated
     */
    private readonly lang: string;

    /**
     * @description Constructor of the I18nClass class
     *
     * The constructor creates an instance of the I18nClass class and specifies the default values
     * the input and output variables of the class.
     *
     * @param collection Data to translate
     * @param lang Application's language
     */
    constructor(collection: I18nTranslatableClass, lang: string) {
        this.translationCollection = collection;
        this.lang = lang;
    }

    /**
     * @description This method return the corresponding text.
     *
     * @param code code to retrieve the associated text
     */
    public translation(code: string): string {
        if (code) {
            const split = code.split('/');

            if (!split || !this.translationCollection) {
                return null;
            }

            let ret: I18nTranslatableClass | string = this.translationCollection;
            for (const s of split) {
                ret = ret[s];
                if (typeof ret === 'string') {
                    break;
                }
                if (!ret) {
                    ret = null;
                    break;
                }
            }
            if (typeof ret === 'string') {
                return ret;
            }
        }
        return '';
    }
}
