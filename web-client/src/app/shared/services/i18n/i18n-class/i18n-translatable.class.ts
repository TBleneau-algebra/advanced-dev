import {OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {I18nTranslationService} from '../i18n-translation.service';
import {I18nClass} from './i18n.class';

/**
 * @description This class is used to enable translations inside a component. Just extend it from your component.
 */
export class I18nTranslatableClass implements OnDestroy {

    /**
     * @description The event listener of the translation service.
     * Note: You shoud never access it or override it. That's the reason why it don't have any getter or setter.
     */
    private subscription: Subscription;

    /**
     * @description current loaded translation.
     * Note: It is dangerous to access it directly. That's the reason why it don't have any getter or setter.
     */
    private i18nTranslation: I18nClass;

    /**
     * @description Constructor of I18nTranslatableClass class
     *
     * The constructor creates an instance of the I18nTranslatableClass class and specifies the default values
     * the input and output variables of the component.
     *
     * @param translationService A reference to the i18n translation service of the application
     */
    constructor(protected translationService: I18nTranslationService) {
        this.i18nTranslation = translationService.getTranslation();
        this.subscription = translationService.getTranslationObservable().subscribe(translation => {
            this.i18nTranslation = translation;
        });
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional deletion tasks.
     */
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    /**
     * @description The method provides access to the translation of the passed element
     *
     * @param wildcard Data to translate
     */
    public translate(wildcard: string): string {
        if (!this.i18nTranslation || !this.i18nTranslation.translation) {
            return '';
        }
        return this.i18nTranslation.translation(wildcard);
    }
}
