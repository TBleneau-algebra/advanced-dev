/**
 * Import of english data
 */
import {data as APP} from './en/pages/app.data';
import {data as SUCCESS} from './en/message/success.data';
import {data as ERROR} from './en/message/error.data';
import {data as AUTHENTICATION} from './en/pages/authentication.data';
import {data as ADMINISTRATION} from './en/pages/administration.data';
import {data as DISCOVER} from './en/pages/discover.data';
import {data as HEADER} from './en/components/header.data';
import {data as PACKAGE} from './en/components/package.data';
import {data as NOT_FOUND} from './en/pages/not-found.data';
import {data as UPLOAD_PHOTO} from './en/pages/upload-photo.data';
import {data as SETTINGS_PROFILE} from './en/pages/settings-profile.data';
import {data as SETTINGS_HISTORY} from './en/pages/settings-history.data';
import {data as ACTION} from './en/components/action.data';
import {data as PHOTO_BOX} from './en/components/photo-box.data';
import {data as NOTIFICATION} from './en/message/notification.data';

/**
 * @description The English data for translation management
 * To add a new sample of data, simply create a data and store it in the components, pages or message folder
 */
export const data: any = {
    code: 200,
    message: null,
    data: {
        ERROR,
        SUCCESS,
        AUTHENTICATION,
        ADMINISTRATION,
        DISCOVER,
        HEADER,
        PACKAGE,
        APP,
        NOT_FOUND,
        UPLOAD_PHOTO,
        SETTINGS_PROFILE,
        SETTINGS_HISTORY,
        ACTION,
        PHOTO_BOX,
        NOTIFICATION
    }
};
