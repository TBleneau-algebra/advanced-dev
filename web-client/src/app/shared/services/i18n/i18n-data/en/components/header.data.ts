export const data = {
    FRENCH: 'French',
    ENGLISH: 'English',
    LANGUAGES: 'Languages',
    LOGOUT_QUESTION: 'Are you sure you want to log out?',
    YES: 'Yes',
    NO: 'No'
};
