export const data = {
    OK: 'Successful request',
    USER_CREATED: 'Register successful',
    USER_AUTHENTICATED: 'Login successful',
    USER_DISCONNECTED: 'Logout successful',
    USER_DELETED: 'User has been correctly deleted',
    USER_UPDATED: 'User has been correctly updated',
    PASSWORD_UPDATED: 'Password has been correctly updated',
    PHOTO_UPLOADED: 'Your photo has been successfully uploaded',
    PHOTO_DELETED: 'Your photo has been successfully deleted',
    PHOTO_UPDATED: 'Your photo has been successfully updated',
};
