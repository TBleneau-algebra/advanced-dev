export const data = {
    DELETE_QUESTION: 'Are you sure you want to delete this image?',
    DOWNLOAD_QUESTION: 'Which image would you like to download?',
    YES: 'Yes',
    NO: 'No',
    IMAGE: 'Original image',
    IMAGE_FILTER: 'Image with filter'
};
