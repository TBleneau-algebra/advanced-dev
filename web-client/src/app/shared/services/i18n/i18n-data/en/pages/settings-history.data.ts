export const data = {
    HISTORY_AUTH_TITLE: 'Authentication information history',
    HISTORY_PHOTO_TITLE: 'History of actions performed on photos',
    HISTORY_PROFILE_TITLE: 'User details history',
    HISTORY_ADMIN_TITLE: 'History of administrator\'s action'
};
