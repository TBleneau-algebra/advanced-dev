export const data = {
    TITLE: 'Application user\'s list',
    SEARCH: 'Search',
    MODAL_TITLE: 'Select an action',
    MODAL_CLOSE: 'Close',
    MODAL_OPTION_HISTORY: 'User\'s history',
    MODAL_OPTION_PROFILE: 'User\'s profile',
    MODAL_OPTION_PHOTOS: 'User\'s photos',
    DELETE_QUESTION: 'Are you sure you want to delete this user?',
    YES: 'Yes',
    NO: 'No'
};
