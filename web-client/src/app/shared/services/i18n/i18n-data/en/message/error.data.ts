export const data = {
    USER_NOT_FOUND: 'User doesn\'t exist. Check username and password',
    USER_NOT_AUTHORIZED: 'You are not authorized',
    USER_NOT_AUTHENTICATED: 'Session expired. You are not authenticated',
    USER_NOT_USER: 'You don\'t have the user rights',
    USER_NOT_ADMIN: 'You don\'t have the admin rights',
    USER_NOT_SUPER_ADMIN: 'You don\'t have the super admin rights',
    USER_BAD_REQUEST: 'Bad request. An error has occured',
    USER_DELETE_ADMIN: 'An administrator user cannot be deleted and cannot be deleted by another administrator user',
    USER_UPDATE_ADMIN: 'Admin information cannot be updated by another admin user',
    USER_UPDATE_PACKAGE: 'You can\'t change your offer more than one time a day',

    LOGIN_BAD: 'Wrong username or email address',
    LOGIN_MISSING: 'Username or email address is required',

    PASSWORD_MISSING: 'Password is required',
    PASSWORD_MIN_LENGTH: 'Password must be at least 8 characters long',
    PASSWORD_MAX_LENGTH: 'Password must be 64 characters maximum',
    PASSWORD_COMPOSITION: 'Must contains at least a number, a special character, an uppercase and a lowercase letter',

    CONFIRM_PASSWORD_MISSING: 'Password confirmation is required',
    CONFIRM_PASSWORD_MATCH: 'Password confirmation doesn\'t match',

    USERNAME_ALREADY_USED: 'Username already used',
    USERNAME_MISSING: 'Username is required',

    NAME_MISSING: 'Name is required',
    NAME_BAD_FORMAT: 'Name should not contain special characters',

    LAST_NAME_MISSING: 'Name is required',
    LAST_NAME_BAD_FORMAT: 'Name should not contain special characters',

    EMAIL_BAD_FORMAT: 'Wrong email address format',
    EMAIL_MISSING: 'Email address is required',
    EMAIL_ALREADY_USED: 'Email address already used',

    API_CONNECTION: 'An error has occurred. Problem connecting to the API',

    IMAGE_NOT_FOUND: 'Image doesn\'t exist.',
    IMAGE_DAILY_PHOTOS_REACHED: 'The daily number of photos you can upload has been reached',
    IMAGE_DAILY_UPLOAD_SIZE_REACHED: 'Total size of downloaded images exceeds the allowed download size'
};
