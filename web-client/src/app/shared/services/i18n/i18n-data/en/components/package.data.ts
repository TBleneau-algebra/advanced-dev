export const data = {
    PICTURES_BY_DAY: 'pictures by day',
    PICTURES_UNLIMITED: 'Unlimited pictures by day',
    CURRENCY: '£',
    MONTHLY: 'monthly',
    PICTURES_UPLOADED_PLURAL: 'photos uploaded today',
    PICTURES_UPLOADED_SINGULAR: 'photo uploaded today',
    MEGA_BYTES: 'Mb',
    FREE_PACKAGE: 'Free package',
    GOLD_PACKAGE: 'Gold package',
    PROFESSIONAL_PACKAGE: 'Professional package',
};
