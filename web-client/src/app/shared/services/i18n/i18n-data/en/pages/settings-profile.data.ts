export const data = {
    USER_SECTION: 'User\'s information',
    PASSWORD_SECTION: 'Change my password',
    PACKAGE_SECTION: 'Package offers',
    CHANGE_ACTION: 'Change',
    UPLOAD_ACTION: 'Upload my photo',
    UPDATE_ACTION: 'Update',
    CLEAR_ACTION: 'Clear',
    PACKAGE_QUESTION: 'Are you sure you want to change your offer? Please note that you can only change your offer once a day',
    YES: 'Yes',
    NO: 'No'
};
