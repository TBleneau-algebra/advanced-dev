export const data = {
    ACTION_REGISTER: 'Application registration',
    ACTION_LOGIN: 'Application connection',
    ACTION_LOGOUT: 'Application logout',
    ACTION_UPDATE_INFO: 'Changing my personal information',
    ACTION_UPDATE_PASSWORD: 'Changing my password',
    ACTION_UPDATE_PROFILE_PICTURE: 'Changing my profile picture',
    ACTION_UPDATE_PACKAGE: 'Changing my package offer',
    ACTION_ADMIN_DELETE_USER: 'Deletion of a user',
    ACTION_ADMIN_DELETE_USER_IMAGE: 'Deletion of a user\'s image',
    ACTION_ADMIN_UPDATE_USER: 'User information modification',
    ACTION_IMAGE_POST: 'Adding an image',
    ACTION_IMAGE_UPDATE: 'Image editing',
    ACTION_IMAGE_DELETE: 'Deleting an image'
};
