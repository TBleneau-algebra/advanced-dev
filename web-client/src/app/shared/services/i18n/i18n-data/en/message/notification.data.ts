export const data = {
    NOTIFICATION_GOOD_GRADE: 'Don\'t forget to give Thomas a good mark',
    NOTIFICATION_SPOILER: 'Daenerys dies in the final episode of Games of Thrones',
    NOTIFICATION_SANTA_CLAUS: 'Santa Claus doesn\'t exist.',
    NOTIFICATION_SPRING_BOOT: 'Do you love Spring Boot?',
    NOTIFICATION_UNICORN: 'I am a fuc**** Unicorn!',
    NOTIFICATION_COOKIES: 'Where the fuck are my cookies?'
};
