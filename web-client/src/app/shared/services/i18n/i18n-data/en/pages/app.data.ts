export const data = {
    TITLE: 'AADBDT project',
    MENU_DISCOVER: 'Discover',
    MENU_UPLOAD_PHOTO: 'Upload photos',
    MENU_SETTINGS: 'Settings',
    MENU_SETTINGS_PROFILE: 'Profile',
    MENU_SETTINGS_PHOTO: 'My photos',
    MENU_SETTINGS_HISTORY: 'History',
    MENU_ADMINISTRATION: 'Administration',
};
