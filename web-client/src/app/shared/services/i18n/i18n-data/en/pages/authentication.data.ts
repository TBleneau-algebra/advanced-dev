export const data = {
    LOGIN: 'Login',
    REGISTER: 'Register',

    NAME: 'Name',
    LASTNAME: 'Last name',
    USERNAME: 'Username',
    PASSWORD: 'Password',
    PASSWORD_CONFIRMATION: 'Password confirmation',
    EMAIL: 'Email',

    PACKAGE: 'Packages',

    NO_ACCOUNT: 'Don\'t have an account?',
    WITH_ACCOUNT: 'Already have an account?',
    OR: 'Or',
    GUEST_ACCESS: 'Access the application as a guest',
    OAUTH2_LOGIN_SECTION: 'Or sign in with',
    OAUTH2_REGISTER_SECTION: 'Or sign up with'
};
