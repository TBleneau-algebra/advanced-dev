export const data = {
    NOT_FOUND_TITLE: 'Page not found',
    NOT_FOUND_DESCRIPTION: 'The page you are looking for might has been removed or its name has been changed or is temporarily \n' +
        'unavailable.',
    NOT_FOUND_BUTTON: 'Go To Homepage'
};
