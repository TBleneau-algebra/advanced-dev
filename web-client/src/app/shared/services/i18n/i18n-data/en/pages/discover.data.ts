export const data = {
    SEARCH: 'Search',
    SEARCH_TAGS: 'Search by tags',
    SEARCH_SIZES: 'Search by sizes',
    SEARCH_KEYWORDS: 'Search by title, author, description',
    ENTER_KEYWORDS: 'Title, author, description',
    ENTER_SIZES: 'Sizes (width, height)',
    SELECT_TAGS: 'Select tags',
    LOADING: 'Loading',
    TAGS_NOT_FOUND: 'No tags have been found',
};
