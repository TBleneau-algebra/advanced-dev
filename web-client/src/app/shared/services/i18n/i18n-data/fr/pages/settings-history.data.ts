export const data = {
    HISTORY_AUTH_TITLE: 'Historique des informations d\'authentification',
    HISTORY_PHOTO_TITLE: 'Historique des actions effectuées sur les photos',
    HISTORY_PROFILE_TITLE: 'Historique du profil utilisateur',
    HISTORY_ADMIN_TITLE: 'Historique des actions administrateur'
};
