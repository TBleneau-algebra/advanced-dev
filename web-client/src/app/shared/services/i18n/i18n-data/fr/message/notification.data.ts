export const data = {
    NOTIFICATION_GOOD_GRADE: 'Pensez à mettre une bonne note à Thomas',
    NOTIFICATION_SPOILER: 'Daenerys meurt dans le dernier épisode de Games of Thrones',
    NOTIFICATION_SANTA_CLAUS: 'Le père Noël n\'existe pas',
    NOTIFICATION_SPRING_BOOT: 'Aimez-vous Spring Boot?',
    NOTIFICATION_UNICORN: 'Je suis une put*** de Licorne!',
    NOTIFICATION_COOKIES: 'Où sont mes biscuits, bordel ?'
};
