export const data = {
    DELETE_QUESTION: 'Etes-vous sure de vouloir supprimer cette image ?',
    DOWNLOAD_QUESTION: 'Quel image souhaitez-vous télécharger ?',
    YES: 'Oui',
    NO: 'Non',
    IMAGE: 'Image original',
    IMAGE_FILTER: 'Image avec un filtre'
};
