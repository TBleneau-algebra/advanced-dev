export const data = {
    NOT_FOUND_TITLE: 'Page non trouvée',
    NOT_FOUND_DESCRIPTION: 'La page que vous recherchez a peut-être été supprimée ou son nom a été changé ou est temporairement \n' +
        'indisponible',
    NOT_FOUND_BUTTON: 'Retourner à la page d\'accueil'
};
