export const data = {
    OK: 'Requête réussie',
    USER_CREATED: 'Inscription réussie',
    USER_AUTHENTICATED: 'Connexion réussie',
    USER_DISCONNECTED: 'Déconnexion réussie',
    USER_DELETED: 'L\'utilisateur a correctement été supprimé',
    USER_UPDATED: 'Les informations ont correctement été mises à jour',
    PASSWORD_UPDATED: 'Le mot de passe a correctement été mis à jour',
    PHOTO_UPLOADED: 'Votre photo a bien été sauvegardé',
    PHOTO_DELETED: 'Votre photo a bien été supprimée',
    PHOTO_UPDATED: 'Votre photo a bien été mise à jour',
};
