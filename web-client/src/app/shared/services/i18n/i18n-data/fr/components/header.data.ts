export const data = {
    FRENCH: 'Français',
    ENGLISH: 'Anglais',
    LANGUAGES: 'Langues',
    LOGOUT_QUESTION: 'Etes vous sûr de vouloir vous déconnecter ?',
    YES: 'Oui',
    NO: 'Non'
};
