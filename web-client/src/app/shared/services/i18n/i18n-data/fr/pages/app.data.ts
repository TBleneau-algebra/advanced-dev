export const data = {
    TITLE: 'Projet AADBDT',
    MENU_DISCOVER: 'Découverte',
    MENU_UPLOAD_PHOTO: 'Télécharger des photos',
    MENU_SETTINGS: 'Paramètres',
    MENU_SETTINGS_PROFILE: 'Profil',
    MENU_SETTINGS_PHOTO: 'Mes photos',
    MENU_SETTINGS_HISTORY: 'Historique',
    MENU_ADMINISTRATION: 'Administration',
};
