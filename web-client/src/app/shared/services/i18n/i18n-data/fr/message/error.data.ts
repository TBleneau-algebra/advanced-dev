export const data = {
    USER_NOT_FOUND: 'L\'utilisateur n\'existe pas. Vérifier le nom d\'utilisateur et le mot de passe',
    USER_NOT_AUTHORIZED: 'Vous n\'êtes pas autorisé',
    USER_NOT_AUTHENTICATED: 'La session est expirée. Vous n\'êtes pas authentifié',
    USER_NOT_USER: 'Vous n\'avez pas les droits utilisateur',
    USER_NOT_ADMIN: 'Vous n\'avez pas les droits administrateur',
    USER_NOT_SUPER_ADMIN: 'Vous n\'avez pas les droits super-administrateur',
    USER_BAD_REQUEST: 'Mauvaise requête. Une erreur s\'est produite',
    USER_DELETE_ADMIN: 'Un utilisateur administrateur ne peut pas être supprimé et ne peut pas être supprimé par un autre ' +
        'utilisateur administrateur',
    USER_UPDATE_ADMIN: 'Les informations de l\'administrateur ne peuvent pas être mises à jour par un autre administrateur',
    USER_UPDATE_PACKAGE: 'Vous ne pouvez pas changer votre offre plus d\'une fois par jour',

    LOGIN_BAD: 'Mauvais nom d\'utilisateur ou adresse e-mail',
    LOGIN_MISSING: 'Le nom d\'utilisateur ou l\'adresse email est requis',

    PASSWORD_MISSING: 'Le mot de passe est requis',
    PASSWORD_MIN_LENGTH: 'Le mot de passe doit comporter au moins 8 caractères',
    PASSWORD_MAX_LENGTH: 'Le mot de passe doit avoir 64 caractères maximum',
    PASSWORD_COMPOSITION: 'Doit contenir au moins un chiffre, un caractère spécial, une majuscule et une minuscule',

    CONFIRM_PASSWORD_MISSING: 'La confirmation du mot de passe est requise',
    CONFIRM_PASSWORD_MATCH: 'La confirmation du mot de passe ne correspond pas',

    USERNAME_ALREADY_USED: 'Nom d\'utilisateur déjà utilisé',
    USERNAME_MISSING: 'Le nom d\'utilisateur est requis',

    NAME_MISSING: 'Le prénom est requis',
    NAME_BAD_FORMAT: 'Le prénom ne doit pas contenir de caractères spéciaux',

    LAST_NAME_MISSING: 'Le nom de famille est requis',
    LAST_NAME_BAD_FORMAT: 'Le nom de famille ne doit pas contenir de caractères spéciaux',

    EMAIL_BAD_FORMAT: 'Mauvais format d\'adresse e-mail',
    EMAIL_MISSING: 'L\'adresse e-mail est requise',
    EMAIL_ALREADY_USED: 'Adresse email déjà utilisée',

    API_CONNECTION: 'Une erreur s\'est produite. Problème de connexion à l\'API',

    IMAGE_NOT_FOUND: 'L\'image n\'existe pas',
    IMAGE_DAILY_PHOTOS_REACHED: 'Le nombre de photos journalier que vous pouvez upload a été atteint',
    IMAGE_DAILY_UPLOAD_SIZE_REACHED: 'La taille total des images téléchargées dépasse la taille de téléchargement autorisées'
};

