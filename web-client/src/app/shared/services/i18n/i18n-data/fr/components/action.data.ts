export const data = {
    ACTION_REGISTER: 'Inscription à l\'application',
    ACTION_LOGIN: 'Connexion à l\'application',
    ACTION_LOGOUT: 'Déconnexion de l\'application',
    ACTION_UPDATE_INFO: 'Modification de mes informations personnelles',
    ACTION_UPDATE_PASSWORD: 'Modification de mon mot de passe',
    ACTION_UPDATE_PROFILE_PICTURE: 'Modification de ma photo de profil',
    ACTION_UPDATE_PACKAGE: 'Modification de mon forfait',
    ACTION_ADMIN_DELETE_USER: 'Supression d\'un utilisateur',
    ACTION_ADMIN_UPDATE_USER: 'Modification des informations d\'un utilisateur\n',
    ACTION_IMAGE_POST: 'Ajout d\'une image',
    ACTION_IMAGE_UPDATE: 'Modification d\'une image',
    ACTION_IMAGE_DELETE: 'Suppression d\'une image'
};
