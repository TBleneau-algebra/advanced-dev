export const data = {
    PICTURES_BY_DAY: 'photos par jours',
    PICTURES_UNLIMITED: 'Nombre illimité de photos par jour',
    CURRENCY: '€',
    MONTHLY: 'mensuellement',
    PICTURES_UPLOADED_PLURAL: 'photos téléchargées aujourd\'hui',
    PICTURES_UPLOADED_SINGULAR: 'photo téléchargé aujourd\'hui',
    MEGA_BYTES: 'Mo',
    FREE_PACKAGE: 'Offre gratuite',
    GOLD_PACKAGE: 'Offre Gold',
    PROFESSIONAL_PACKAGE: 'Offre professionnel',
};
