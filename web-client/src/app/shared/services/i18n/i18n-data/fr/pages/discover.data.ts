export const data = {
    SEARCH: 'Rechercher',
    SEARCH_KEYWORDS: 'Rechercher par titre, auteur, description',
    SEARCH_TAGS: 'Rechercher par tags',
    SEARCH_SIZES: 'Rechercher par tailles',
    ENTER_KEYWORDS: 'Titre, auteur, description',
    ENTER_SIZES: 'Taille (largeur, hauteur)',
    SELECT_TAGS: 'Sélectionner un ou plusieurs tag(s)',
    LOADING: 'Chargement',
    TAGS_NOT_FOUND: 'Aucun tag n\'a été trouvé',
};
