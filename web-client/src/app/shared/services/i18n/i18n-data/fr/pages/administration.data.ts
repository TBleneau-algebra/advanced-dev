export const data = {
    TITLE: 'Liste des utilisateurs de l\'application',
    SEARCH: 'Rechercher',
    MODAL_TITLE: 'Choisir une action',
    MODAL_CLOSE: 'Fermer',
    MODAL_OPTION_HISTORY: 'Historique de l\'utilisateur',
    MODAL_OPTION_PROFILE: 'Profile de l\'utilisateur',
    MODAL_OPTION_PHOTOS: 'Photos de l\'utilisateur',
    DELETE_QUESTION: 'Etes-vous sûr de vouloir supprimer cette utilisateur ?',
    YES: 'Oui',
    NO: 'Non'

};
