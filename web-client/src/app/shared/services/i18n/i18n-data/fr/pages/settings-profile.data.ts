export const data = {
    USER_SECTION: 'Informations utilisateur',
    PASSWORD_SECTION: 'Changer mon mot de passe',
    PACKAGE_SECTION: 'Offres',
    CHANGE_ACTION: 'Valider',
    UPLOAD_ACTION: 'Télécharger ma photo',
    UPDATE_ACTION: 'Mettre à jour',
    CLEAR_ACTION: 'Effacer',
    PACKAGE_QUESTION: 'Etes-vous sûr de vouloir changer votre offre ? Veuillez noter que vous ne pouvez changer ' +
        'votre offre qu\'une fois par jour',
    YES: 'Oui',
    NO: 'Non'
};
