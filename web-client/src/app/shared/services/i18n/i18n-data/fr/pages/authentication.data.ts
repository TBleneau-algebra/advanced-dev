export const data = {
    LOGIN: 'Se connecter',
    REGISTER: 'S\'inscrire',

    NAME: 'Prénom',
    LASTNAME: 'Nom',
    USERNAME: 'Nom d\'utilisateur',
    PASSWORD: 'Mot de passe',
    PASSWORD_CONFIRMATION: 'Confirmation du mot de passe',
    EMAIL: 'Adresse E-mail',

    PACKAGE: 'Offres',

    NO_ACCOUNT: 'Pas encore de compte ?',
    WITH_ACCOUNT: 'Vous avez déjà un compte ?',
    OR: 'Ou',
    GUEST_ACCESS: 'Accéder à l\'application en tant qu\'invité',
    OAUTH2_LOGIN_SECTION: 'Ou connectez-vous avec',
    OAUTH2_REGISTER_SECTION: 'Ou inscrivez-vous avec'
};
