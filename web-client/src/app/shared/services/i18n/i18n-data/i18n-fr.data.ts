/**
 * Import of french data
 */
import {data as APP} from './fr/pages/app.data';
import {data as SUCCESS} from './fr/message/success.data';
import {data as ERROR} from './fr/message/error.data';
import {data as AUTHENTICATION} from './fr/pages/authentication.data';
import {data as ADMINISTRATION} from './fr/pages/administration.data';
import {data as DISCOVER} from './fr/pages/discover.data';
import {data as HEADER} from './fr/components/header.data';
import {data as PACKAGE} from './fr/components/package.data';
import {data as NOT_FOUND} from './fr/pages/not-found.data';
import {data as UPLOAD_PHOTO} from './fr/pages/upload-photo.data';
import {data as SETTINGS_PROFILE} from './fr/pages/settings-profile.data';
import {data as SETTINGS_HISTORY} from './fr/pages/settings-history.data';
import {data as ACTION} from './fr/components/action.data';
import {data as PHOTO_BOX} from './fr/components/photo-box.data';
import {data as NOTIFICATION} from './fr/message/notification.data';

/**
 * @description The French data for translation management
 * To add a new sample of data, simply create a data and store it in the components, pages or message folder
 */
export const data: any = {
    code: 200,
    message: null,
    data: {
        ERROR,
        SUCCESS,
        AUTHENTICATION,
        ADMINISTRATION,
        DISCOVER,
        HEADER,
        PACKAGE,
        APP,
        NOT_FOUND,
        UPLOAD_PHOTO,
        SETTINGS_PROFILE,
        SETTINGS_HISTORY,
        ACTION,
        PHOTO_BOX,
        NOTIFICATION
    }
};
