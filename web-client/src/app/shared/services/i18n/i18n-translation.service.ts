import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {CustomSubjectClass} from '../../classes/rxjs/custom-subject.class';
import {CustomCookieService} from '../cookies/custom-cookie.service';
import {LoaderService} from '../loader-spinner/loader.service';
import {RequestClass} from '../request/request.class';
import {I18nClass} from './i18n-class/i18n.class';

@Injectable({
    providedIn: 'root',
})
export class I18nTranslationService extends RequestClass {

    /**
     * @description Current language of the application
     */
    private _lang: string;

    /**
     * @description Custom Subject which allows to keep the last value of next() call
     */
    private _translation: CustomSubjectClass<I18nClass>;

    /**
     * @description Constructor of I18nTranslationService service
     *
     * The constructor creates an instance of the I18nTranslationService service and specifies the default values
     * the input and output variables of the service.
     *
     * @param httpClient A reference to Angular's HttpClient internal service
     * @param location A reference to the rental service that allows you to build a URL from the root
     * @param cookies A reference to the custom cookie service of the application
     * @param loaderService A reference to the loader service of the application
     */
    constructor(httpClient: HttpClient, location: Location, private cookies: CustomCookieService, loaderService: LoaderService) {
        super(httpClient, location, environment.serverApiBase, loaderService);
        this._lang = 'en';

        if (this.cookies.get('lang')) {
            this._lang = this.cookies.get('lang');
        } else {
            this.cookies.create('lang', 'en');
        }

        this._translation = new CustomSubjectClass<I18nClass>();
        this.reloadTranslation().catch(console.error);
    }

    /**
     * @description This method returns an observable to translation.
     */
    public getTranslationObservable(): Observable<I18nClass> {
        return this._translation.asObservable();
    }

    /**
     * @description This method returns the last used translation
     */
    public getTranslation(): I18nClass {
        return this._translation.last;
    }

    /**
     * @description This methods reload the Translation (used if the Translation change at runtime)
     */
    public async reloadTranslation(): Promise<any> {
        const translationData = await this.get('/translations/' + this._lang);

        this._translation.next(new I18nClass(translationData.responseData.data, this._lang));
        return translationData.responseData.data;
    }

    /**
     * @description This method changes the language of the application
     *
     * @param value New value of the language of the application
     */
    public changeLang(value: string) {
        this.lang = value;
        this.cookies.create('lang', value);
    }

    /**
     * @description This method allows to retrieve the current language of the application
     */
    get lang(): string {
        return this._lang;
    }

    /**
     * @description This method allows to assign the current language of the application
     *
     * @param value New value of the language of the application
     */
    set lang(value: string) {
        this._lang = value;
    }
}
