import {Inject, Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {DOCUMENT} from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class CustomCookieService {

    /**
     * @description Constructor of CustomCookieService servicel
     *
     * The constructor creates an instance of the CustomCookieService service and specifies the default values
     * the input and output variables of the service.
     *
     * @param cookieService  An (AOT ready) service for cookies. Originally based on the ng2-cookies library
     * @param document A DI Token representing the main rendering context. In a browser this is the DOM Document
     */
    constructor(private cookieService: CookieService, @Inject(DOCUMENT) private document) {
    }

    /**
     * @description This method creates a cookie by asking to provide a name, a value and an expiration date
     *
     * @param name Cookie name
     * @param value Cookie value
     * @param expire Cookie expiration date
     */
    public create(name: string, value: any, expire?: Date): void {
        const domain = this.removeSubdomains();

        if (domain) {
            (expire) ? this.cookieService.set(name, value, expire, '/', '.' + domain, false, 'Lax') :
                this.cookieService.set(name, value, null, '/', '.' + domain, false, 'Lax');
        } else {
            (expire) ? this.cookieService.set(name, value, expire, '/', this.document.location.hostname, false, 'Lax') :
                this.cookieService.set(name, value, null, '/', this.document.location.hostname, false, 'Lax');
        }
    }

    /**
     * @description This method removes a cookie by its name (if it's exposed for the application)
     *
     * @param name Cookie name
     */
    public remove(name: string): void {
        this.cookieService.delete(name);
    }

    /**
     * @description This method removes all the available cookie exposed for the application
     */
    public removeAll(): void {
        this.cookieService.deleteAll(null, this.document.location.hostname);
    }

    /**
     * @description This method gets a cookie by its name (if it's exposed for the application)
     *
     * @param name Cookie name
     */
    public get(name: string): string {
        return this.cookieService.get(name);
    }

    /**
     * @description This method gets all the available cookie exposed for the application
     */
    public getAll(): any {
        return this.cookieService.getAll();
    }

    /**
     * @description This method checks if a cookie exists (if a cookie has a value)
     *
     * @param name Cookie name
     */
    public check(name: string): boolean {
        return this.cookieService.check(name);
    }

    /**
     * @description This method refers to the domain on which the application is launched and removes sub-domain prefixes
     * to assign the domain of the cookie
     */
    private removeSubdomains(): string {
        let subdomains: string;

        if (this.document.domain.length) {
            const parts = document.domain.replace(/^(www\.)/, '').split('.');

            while (parts.length > 2) {
                subdomains = parts.shift();
            }

            const domain = parts.join('.');
            return domain.replace(/(^\.*)|(\.*$)/g, '');
        }
        return null;
    }
}
