import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {RequestAuthenticationService} from '../request/request-authentication.service';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationGuardService implements CanActivate {

    /**
     * @description Constructor of AuthenticationGuardService service
     *
     * The constructor creates an instance of the AuthenticationGuardService service and specifies the default values
     * the input and output variables of the service.
     *
     * @param authenticationService A reference to the RequestAuthenticationService service
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private authenticationService: RequestAuthenticationService, private router: Router) {
    }

    /**
     *  @description This method is a guard and decides if a route can be activated
     *  If all guards return true, navigation will continue.
     *  If any guard returns false, navigation will be cancelled.
     *  If any guard returns a UrlTree, current navigation will be cancelled and a new navigation will be kicked off to the UrlTree
     *  returned from the guard.
     *
     * @param route contains the information about a route associated with a component loaded in an outlet at a particular moment in time.
     * @param state represents the state of the router at a moment in time.
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> |
        boolean | UrlTree {
        return new Promise(resolve => {
            this.authenticationService.roles = (JSON.parse(localStorage.getItem('USER_ROLE')) == null) ?
                ['guest'] : JSON.parse(localStorage.getItem('USER_ROLE'));

            if (this.authenticationService.roles !== undefined && this.authenticationService.roles !== null &&
                this.authenticationService.hasRole('guest')) {
                resolve(true);
            } else {
                if (this.authenticationService.isAuthenticated()) {
                    (state.url.slice(0, 15) === '/authentication') ? resolve(this.router.parseUrl('/pages')) : resolve(true);
                } else {
                    (state.url.slice(0, 15) === '/authentication') ? resolve(true) : resolve(this.router.parseUrl('/authentication'));
                }
            }
        });
    }
}
