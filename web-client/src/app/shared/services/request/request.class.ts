import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Location} from '@angular/common';
import {LoaderService} from '../loader-spinner/loader.service';
import {LoaderSemaphoreClass} from '../loader-spinner/loader-semaphore/loader-semaphore.class.';
import {RequestResponseInterface} from './response/request-response.interface';
import {RequestResponseClass} from './response/request-response.class';
import {RequestParamsInterface} from './params/request-params.interface';
import {getTranslationData} from '../../functions/get-translation-data.function';

export class RequestClass {

    /**
     * @description Value of the API context path
     */
    private readonly apiHost: string;

    /**
     * @description Loader spinner id
     */
    private loaderId: string;

    /**
     * @description LoaderSemaphoreClass used by the loader spinner service
     */
    private semaphore: LoaderSemaphoreClass;

    /**
     * @description Value of the url to be requested
     */
    private _urlToRequest: string;

    /**
     * @description Constructor of RequestClass class
     *
     * The constructor creates an instance of the RequestClass class and specifies the default values
     * the input and output variables of the class.
     *
     * @param httpClient A reference to Angular's HttpClient internal service
     * @param location A reference to the rental service that allows you to build a URL from the root
     * @param target A reference to the target URL used to execute the HTTP request
     * @param loaderService  A reference to the LoaderService service
     */
    constructor(private httpClient: HttpClient, private location: Location, private target: string,
                private loaderService: LoaderService) {
        this.loaderId = 'GLOBAL';
        this.semaphore = null;
        this.apiHost = target.lastIndexOf('http', 0) === 0
            ? target
            : location.prepareExternalUrl(target);
    }

    /**
     * @description This method is used to add several request parameters to the url of an Http request
     *
     * @param values All parameters to be provided to the Http request
     */
    private createQueryParams(values: Array<{ name: string, value: any }>): { [key: string]: any } {
        const queries: { [key: string]: any } = {};

        values.forEach((value) => {
            queries[value.name] = value.value;
        });
        return queries;
    }

    /**
     * @description This method is used to provide several headers to an Http request
     *
     * @param values All headers filled in in table form
     */
    private createHeaders(values: Array<{ name: string, value: string }>): { [key: string]: string[] } {
        const headers: { [key: string]: string[] } = {};

        values.forEach((header) => {
            if (!headers[header.name]) {
                headers[header.name] = [];
            }
            headers[header.name].push(header.value);
        });
        return headers;
    }

    /**
     * @description This method executes an Http request and returns a Promise.
     *
     * @param method Method used for HTTP request ('POST','PUT','GET','PATCH','DELETE')
     * @param data Data to be sent to the API when requesting HTTP
     * @param params parameters (optional) to be attached to the HTTP request if necessary
     */
    protected execute(method: string, data?: any, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        return new Promise<RequestResponseInterface>((resolve, reject) => {
            const response: RequestResponseInterface = new RequestResponseClass();

            this.urlToRequest = this.urlToRequest.startsWith('/') ? this.urlToRequest : '/' + this.urlToRequest;

            try {
                if (this.semaphore) {
                    this.loaderService.load(this.loaderId);
                }

                const sample = getTranslationData(this.urlToRequest);
                if (sample) {
                    response.mapTranslation(sample);
                    return resolve(response);
                }

                this.httpClient
                    .request<any>(method, this.apiHost + this.urlToRequest, {
                        headers: (params !== undefined && params !== null) ? this.createHeaders(params.headers) : {},
                        params: (params !== undefined && params !== null) ? this.createQueryParams(params.params) : {},
                        body: (data !== undefined && data !== null) ? data : null,
                        observe: (params !== undefined && params !== null) ? params.observe : 'response',
                        withCredentials: (params !== undefined && params !== null) ? params.withCredentials : false,
                        responseType: (params !== undefined && params !== null) ? params.responseType : 'json',
                        reportProgress: (params !== undefined && params !== null) ? params.reportProgress : false
                    }).subscribe(
                    (res: HttpResponse<any>) => {
                        if (this.semaphore) {
                            this.loaderService.unload(this.loaderId);
                            this.semaphore.sub();
                            delete this.semaphore;
                        }
                        response.map(res);
                        resolve(response);
                    }, (error: HttpErrorResponse) => {
                        if (this.semaphore) {
                            this.loaderService.unload(this.loaderId);
                            this.semaphore.sub();
                            delete this.semaphore;
                        }
                        response.mapError(error);
                        reject(response);
                    }
                );
            } catch (error) {
                reject(error);
            }
        });
    }

    /**
     * @description This method is used to request a representation of the specified resource.
     * This GET request should only be used to retrieve data.
     *
     * @param path Value of the url to be requested
     * @param data Optional Entity(ies) to send to the specified resource
     * @param params Optional parameters to be attached to the request
     */
    public async get(path: string, data?: any, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        this.urlToRequest = path;
        return await this.execute('GET', data, params);
    }

    /**
     * @description This method is used to send an entity to the specified resource (POST request).
     * This usually results in a change of state or edge effects on the server.
     *
     * @param data Entity(ies) to be sent to the specified resource
     * @param path Value of the url to be requested
     * @param params Optional parameters to be attached to the request
     */
    public async post(data: any, path: string, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        this.urlToRequest = path;
        return await this.execute('POST', data, params);
    }

    /**
     * @description This method replaces all current representations of the resource covered by the content of the request (PUT request).
     *
     * @param data Entity(ies) to be sent to the specified resource
     * @param path Value of the url to be requested
     * @param params Optional parameters to be attached to the request
     */
    public async put(data: any, path: string, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        this.urlToRequest = path;
        return await this.execute('PUT', data, params);
    }

    /**
     * @description This method replaces all current representations of the resource covered by
     * the content of the request (PATCH request).
     *
     * @param data Entity(ies) to be sent to the specified resource
     * @param path Value of the url to be requested
     * @param params Optional parameters to be attached to the request
     */
    public async patch(data: any, path: string, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        this.urlToRequest = path;
        return await this.execute('PATCH', data, params);
    }

    /**
     * @description This DELETE method deletes the specified resource.
     *
     * @param data Entity to send to the specified resource
     * @param path Value of the url to be requested
     * @param params Optional parameters to be attached to the request
     */
    public async delete(data: any, path: string, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        this.urlToRequest = path;
        return await this.execute('DELETE', data, params);
    }

    /**
     * @description This method allows to activate the loader spinner of the application by providing the corresponding LoaderSemaphoreClass
     * and the loader spinner identifier
     *
     * @param semaphore LoaderSemaphoreClass used by the loader service to start and stop the loading
     */
    public activeLoader(semaphore: LoaderSemaphoreClass): void {
        this.loaderService.attachSemaphore(this.loaderId, semaphore);
        this.semaphore = semaphore;
    }

    /**
     * @description This method returns the value of the source url to be requested to request the data from the API
     */
    get urlToRequest(): string {
        return this._urlToRequest;
    }

    /**
     * @description This method assigns the value of the source url to be requested to request the data from the API
     *
     * @param value The value of the source url to request to request data from the API
     */
    set urlToRequest(value: string) {
        this._urlToRequest = value;
    }
}
