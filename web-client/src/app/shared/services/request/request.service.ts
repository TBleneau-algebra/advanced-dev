import {Injectable} from '@angular/core';
import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {RequestClass} from './request.class';
import {environment} from '../../../../environments/environment';
import {LoaderService} from '../loader-spinner/loader.service';

@Injectable({
    providedIn: 'root'
})
export class RequestService extends RequestClass {

    /**
     * @description Constructor of RequestService service
     *
     * The constructor creates an instance of the RequestService service and specifies the default values
     * the input and output variables of the service.
     *
     * @param httpClient A reference to Angular's HttpClient internal service
     * @param location A reference to the rental service that allows you to build a URL from the root
     * @param loaderService A reference to the loader service of the application
     */
    constructor(httpClient: HttpClient, location: Location, loaderService: LoaderService) {
        super(httpClient, location, environment.serverApiBase, loaderService);
    }
}
