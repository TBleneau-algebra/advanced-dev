import {Injectable} from '@angular/core';
import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {RequestClass} from './request.class';
import {environment} from '../../../../environments/environment';
import {NbRoleProvider} from '@nebular/security';
import {Observable} from 'rxjs';
import {UserRoleClass} from '../../classes/models/user-role.class';
import {UserClass} from '../../classes/models/user.class';
import {LoaderService} from '../loader-spinner/loader.service';
import {CustomCookieService} from '../cookies/custom-cookie.service';

@Injectable({
    providedIn: 'root'
})
export class RequestAuthenticationService extends RequestClass implements NbRoleProvider {

    /**
     * @description User's role
     */
    private _roles: string[];

    /**
     * @description User entity sent by the API
     */
    private _user: UserClass;

    /**
     * @description Service constructor RequestAuthenticationService
     *
     * @param httpClient A reference to Angular's HttpClient internal service
     * @param location A reference to the rental service that allows you to build a URL from the root
     * @param cookieService A reference to the NgxCookieService originally based on the ng2-cookies library
     * @param spinnerService
     */
    constructor(httpClient: HttpClient, location: Location, private cookieService: CustomCookieService,
                spinnerService: LoaderService) {
        super(httpClient, location, environment.serverApiBase, spinnerService);
        this.roles = ['guest'];
    }

    /**
     * @description This method overrides the method present in the interface NbRoleProvider to give the NbSecurityModule module
     * the roles of the user
     */
    getRole(): Observable<string | string[]> {
        return Observable.create((observer) => {
            observer.next(this.roles);
        });
    }

    /**
     * @description This method assigns the user roles to the corresponding format of the NbSecurityModule module
     *
     * @param roles User's roles
     */
    setRoles(roles?: Array<UserRoleClass>): void {
        let result: string[] = [];
        enum Roles { ROLE_GUEST = 'guest', ROLE_USER = 'user', ROLE_ADMIN = 'moderator'}

        (roles) ? result = roles.map(role => Roles[role.name]) : undefined;

        if (result.length === 0) {
            this.roles = ['guest'];
        } else {
            this.roles = result;
        }
    }

    /**
     * @description The methods allows you to check if the role passed as parameter is contained in the user's role
     *
     * @param role Role to check
     */
    hasRole(role: string): boolean {
        return !!this.roles.find(item => item === role);
    }

    /**
     * @description The method defines if the user is authenticated or not
     */
    isAuthenticated(): boolean {
        const auth: string = this.cookieService.get('AUTHENTICATED');

        if (auth === null || auth === '') {
            return false;
        }
        return Boolean(JSON.parse(auth));
    }

    /**
     * @description The method allows you to retrieve the user's role sent by the API
     */
    get roles(): string[] {
        return this._roles;
    }

    /**
     * @description The method is used to assign the user's role sent by the API
     *
     * @param value Value of the the user's role
     */
    set roles(value: string[]) {
        this._roles = value;
    }


    /**
     * @description The method allows you to retrieve the user entity sent by the API
     */
    get user(): UserClass {
        return this._user;
    }

    /**
     * @description The method allows you to assign the user entity sent by the API
     *
     * @param value Value of the user entity sent by the API
     */
    set user(value: UserClass) {
        this._user = value;
    }
}
