import {Observable} from 'rxjs';
import {LoaderSemaphoreEventClass} from './loader-semaphore-event.class';
import {LoaderSemaphoreEventsClass} from './loader-semaphore-events.class';

/**
 * @description This class defines the behavior of the semaphore class.
 *
 * This implementation is a light version of Unix C/C++ LoaderSemaphoreClass.
 * This implementation allows you to release an event only after x decrementing
 */
export class LoaderSemaphoreClass {

  /**
   * @description number of lock to release to throw the event
   */
  private count: number = -1;

  /**
   * @description Container for the events managed by semaphore
   */
  private events = new LoaderSemaphoreEventsClass();

  /**
   * @description Constructor of LoaderSemaphoreClass class
   *
   * The constructor creates an instance of the LoaderSemaphoreClass class and specifies the default values
   * the input and output variables of the class.
   *
   * @param initial The initial number of lock to release to throw the event.
   */
  constructor(initial: number) {
    this.count = initial;
  }

  /**
   * @description This method is used emit an event
   *
   * @param event The event to emit
   * @param value The value to emit
   */
  protected emit(event: LoaderSemaphoreEventClass<any>, value: any) {
    if (event.callable) {
      event.callable(value);
    }
    if (event.subject) {
      event.subject.next(value);
    }
  }

  /**
   * @description This method is used to check if an event should be emitted and if its the case, which event.
   */
  protected shouldRelease(): boolean {
    let ret: boolean = null;

    if (this.count > 0) {
      --this.count;
      this.emit(this.events.release, false);
      ret = false;
    } else if (this.count < 0) {
      throw new Error('Semapohre already released');
    } else {
      --this.count;
      this.emit(this.events.release, true);
      ret = true;
    }
    this.emit(this.events.change, this.count);

    return ret;
  }

  /**
   * @description This method decrement the number of locks
   */
  public sub(): boolean {
    if (this.count > 1) {
      --this.count;
    }
    return this.shouldRelease();
  }

  /**
   * @description This method increment the number of locks
   */
  public add(): boolean {
    ++this.count;
    return this.shouldRelease();
  }

  /**
   * @description This method force the semaphore to release the event. And sets the locks number to 0 (no pending locks)
   */
  public forceRelease(): boolean {
    this.count = -1;
    return this.shouldRelease();
  }

  /** observable
   * @description This method allows you to retrieve the OnChange event observable
   */
  get change(): Observable<number> {
    return this.events.change.subject.asObservable();
  }

  /**
   * @description This method allows you to retrieve the OnRelease event observable
   */
  get release(): Observable<boolean> {
    return this.events.release.subject.asObservable();
  }
}

