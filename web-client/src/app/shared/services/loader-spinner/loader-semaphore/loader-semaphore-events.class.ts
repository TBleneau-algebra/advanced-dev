import {LoaderSemaphoreEventClass} from './loader-semaphore-event.class';

/**
 * @description This class represents a container for the events managed by semaphore
 */
export class LoaderSemaphoreEventsClass {
  public change = new LoaderSemaphoreEventClass<number>();
  public release = new LoaderSemaphoreEventClass<boolean>();
}
