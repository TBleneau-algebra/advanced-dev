import {NbMenuItem} from '@nebular/theme';

/**
 * @description This array represents the definition of the registered user's menu in the application
 */
export const UserMenuData: Array<NbMenuItem> = [
    {
        title: 'Discover',
        icon: 'globe-2-outline',
        link: '/pages/discover',
        data: 'APP/MENU_DISCOVER'
    },
    {
        title: 'Upload photos',
        icon: 'cloud-upload-outline',
        link: '/pages/upload',
        data: 'APP/MENU_UPLOAD_PHOTO'
    },
    {
        title: 'Settings',
        icon: 'settings-2-outline',
        link: '/pages/settings',
        data: 'APP/MENU_SETTINGS',
        children: [
            {
                title: 'Profile',
                icon: 'person-outline',
                link: '/pages/settings/profile',
                data: 'APP/MENU_SETTINGS_PROFILE'
            },
            {
                title: 'My photos',
                icon: 'image-outline',
                link: '/pages/settings/photos',
                data: 'APP/MENU_SETTINGS_PHOTO'
            },
        ]
    },
];
