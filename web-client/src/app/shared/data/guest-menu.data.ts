import {NbMenuItem} from '@nebular/theme';

/**
 * @description This array represents the basic definition of the menu in the application
 */
export const GuestMenuData: Array<NbMenuItem> = [
    {
        title: 'Discover',
        icon: 'globe-2-outline',
        link: '/pages/discover',
        data: 'APP/MENU_DISCOVER'
    },
];
