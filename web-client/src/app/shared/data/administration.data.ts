/**
 * @description Parameters to be provided to the Ng2SmartTableComponent in the French version of the application
 */
export const SettingsFr: any = {
    actions: {
        add: true,
        edit: true,
        delete: true
    },
    add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true
    },
    delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
    },
    columns: {
        id: {
            title: 'ID',
            type: 'number',
            filter: false,
            editable: false
        },
        name: {
            title: 'Prénom',
            type: 'string',
            filter: false,
            editable: true
        },
        lastname: {
            title: 'Nom',
            type: 'string',
            filter: false,
            editable: true
        },
        username: {
            title: 'Nom d\'utilisateur',
            type: 'string',
            filter: false,
            editable: false
        },
        email: {
            title: 'Adresse e-mail',
            type: 'string',
            filter: false,
            editable: true
        },
        rolesJoined: {
            title: 'Roles',
            type: 'string',
            filter: false,
            editable: false
        },
        packageSelected: {
            title: 'Offre',
            type: 'string',
            filter: false,
            editable: false
        }
    },
};

/**
 * Parameters to be provided to the Ng2SmartTableComponent in the English version of the application
 */
export const SettingsEn: any = {
    actions: {
        add: true,
        edit: true,
        delete: true
    },
    add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true
    },
    delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
    },
    columns: {
        id: {
            title: 'ID',
            type: 'number',
            filter: false,
            editable: false
        },
        name: {
            title: 'First Name',
            type: 'string',
            filter: false,
            editable: true
        },
        lastname: {
            title: 'Last Name',
            type: 'string',
            filter: false,
            editable: true
        },
        username: {
            title: 'Username',
            type: 'string',
            filter: false,
            editable: false
        },
        email: {
            title: 'Email',
            type: 'string',
            filter: false,
            editable: true
        },
        rolesJoined: {
            title: 'Roles',
            type: 'string',
            filter: false,
            editable: false
        },
        packageSelected: {
            title: 'Package',
            type: 'string',
            filter: false,
            editable: false
        }
    },
};

