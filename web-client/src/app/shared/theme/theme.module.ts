/**
 * Import of Angular's modules
 */
import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

/**
 * Import of Nebular's modules
 */
import {
    NbActionsModule,
    NbLayoutModule,
    NbMenuModule,
    NbSearchModule,
    NbSidebarModule,
    NbUserModule,
    NbContextMenuModule,
    NbButtonModule,
    NbSelectModule,
    NbIconModule,
    NbThemeModule,
    NbToggleModule,
    NbCardModule,
    NbListModule,
    NbAccordionModule,
    NbToastrModule,
    NbDialogModule,
    NbStepperModule,
    NbAlertModule,
    NbTabsetModule,
    NbProgressBarModule,
    NbInputModule,
    NbDatepickerModule,
    NbCheckboxModule,
    NbPopoverModule,
    NbSidebarService, NbThemeService,
} from '@nebular/theme';
import {NbEvaIconsModule} from '@nebular/eva-icons';
import {NbSecurityModule} from '@nebular/security';

/**
 * Import of application's theme style
 */
import {corporateStyle} from './styles/corporate.style';
import {NbAuthModule} from '@nebular/auth';


const NB_MODULES = [
    NbLayoutModule,
    NbMenuModule,
    NbUserModule,
    NbActionsModule,
    NbSearchModule,
    NbSidebarModule,
    NbContextMenuModule,
    NbSecurityModule,
    NbButtonModule,
    NbSelectModule,
    NbIconModule,
    NbEvaIconsModule,
    NbCardModule,
    NbListModule,
    NbAccordionModule,
    NbToastrModule,
    NbDialogModule,
    NbStepperModule,
    NbToggleModule,
    NbAlertModule,
    NbTabsetModule,
    NbProgressBarModule,
    NbAuthModule,
    NbInputModule,
    NbDatepickerModule,
    NbCheckboxModule,
    NbPopoverModule
];

const NB_PROVIDERS = [
    NbSidebarService,
    NbThemeService
];

@NgModule({
    imports: [
        CommonModule,
        ...NB_MODULES
    ],
    exports: [
        CommonModule,
        ...NB_MODULES
    ],
    providers: [
        ...NB_PROVIDERS
    ],
    declarations: []
})
export class ThemeModule {
    static forRoot(): ModuleWithProviders {
        return <ModuleWithProviders>{
            ngModule: ThemeModule,
            providers: [
                ...NbThemeModule.forRoot(
                    {
                        name: 'corporate',
                    },
                    [corporateStyle],
                ).providers
            ],
        };
    }
}
