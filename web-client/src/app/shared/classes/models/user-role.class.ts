/**
 * @description This class implements what send to the API to update the role of the user or what the API respond
 */
export class UserRoleClass {
    id: string;
    name: string;
}
