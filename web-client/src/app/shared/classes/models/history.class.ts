/**
 * @description This class implements a representation of an user history received by the API
 */
export class HistoryClass {
    data: Array<any>;
    loading: boolean;
    pageSize: string;
    pageFrom: string;
    pageTotal: string;
    elementTotal: string;

    constructor(pageSize: string, pageFrom: string, pageTotal: string) {
        this.data = new Array<any>();
        this.loading = false;
        this.pageSize = pageSize;
        this.pageFrom = pageFrom;
        this.pageTotal = pageTotal;
        this.elementTotal = '0';
    }
}
