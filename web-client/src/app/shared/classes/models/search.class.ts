/**
 * @description This class represents the search model used to filter the pictures research
 */
export class SearchClass {
    term?: string;
    size: string;
    tags?: Array<number>;
    prev: {
        term: string;
        size: string;
        tags?: Array<number>;
    };

    constructor() {
        this.tags = [];
        this.prev = {term: null, size: null, tags: []};
    }
}
