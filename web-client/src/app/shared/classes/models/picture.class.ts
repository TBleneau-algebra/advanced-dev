/**
 * @description This class implements what send to the API or what the API respond about picture entity
 */
export class PictureClass {
    id?: number;
    title: string;
    description: string;
    tags?: Array<any>;
    filter?: any;
    image?: any;
    imageBackup?: any;
    imageFilter?: any;
    width?: number;
    height?: number;
    createdAt?: Date;
    user?: any;
    format?: string;

    /**
     * @description Constructor of PictureClass class
     *
     * The constructor creates an instance of the PictureClass class and specifies the default values of
     * the input and output variables of the class.
     */
    constructor() {
        this.tags = [];
        this.title = '';
        this.description = '';
        this.width = 0;
        this.height = 0;
        this.format = 'image/png';
    }

    /**
     * @description This method allows to decrease the proportions of an image in the canvas passed in parameter
     *
     * @param editor Editor which allows you to add filter to the image
     */
    process(editor: any): void {
        editor.marvin.scale(this.image.clone(), this.image, this.width, this.height);
    }
}
