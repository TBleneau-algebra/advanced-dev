import {UserRoleClass} from './user-role.class';

/**
 * @description This class implements what send to the API to update the user or what the API respond
 */
export class UserClass {
    id: string;
    username: string;
    email: string;
    name: string;
    lastname: string;
    password: string;
    roles: Array<UserRoleClass>;
    userPackage: any;
    profilePicture: any;
}
