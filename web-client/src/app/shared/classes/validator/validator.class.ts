import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export class ValidatorClass {

    /**
     * @description This method is used to check if the field pattern is correct
     *
     * @param regex Regex used to check the pattern
     * @param error Errors if the validation is not correct
     */
    static patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            if (!control.value) {
                return null;
            }
            const valid = regex.test(control.value);
            return valid ? null : error;
        };
    }

    /**
     * @description This method validates whether the password and the password confirmation match.
     *
     * @param control This is the base class for FormControl, FormGroup, and FormArray
     */
    static passwordMatchValidator(control: AbstractControl) {
        const password: string = control.get('password').value;
        const confirmPassword: string = control.get('confirmPassword').value;

        if (confirmPassword === '' || confirmPassword === null) {
            control.get('confirmPassword').setErrors({required: true});
        } else if (password !== confirmPassword) {
            control.get('confirmPassword').setErrors({passswordMatch: true});
        }
    }
}
