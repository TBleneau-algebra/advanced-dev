/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of Nebular's module
 */
import {NbActionsModule, NbCardModule, NbIconModule, NbProgressBarModule} from '@nebular/theme';
/**
 * Import of of the library's dependencies
 */
import {CountUpModule} from 'countup.js-angular2';
/**
 * Import of application's components
 */
import {ActionComponent} from './action.component';
/**
 * Import of application's modules
 */
import {PipesModule} from '../../pipes/pipes.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NbIconModule,
        NbCardModule,
        NbActionsModule,
        NbProgressBarModule,
        CountUpModule,
        PipesModule
    ],
    declarations: [
        ActionComponent
    ],
    exports: [
        ActionComponent
    ],
})
export class ActionModule {
}
