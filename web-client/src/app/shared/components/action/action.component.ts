import {Component, Input, OnInit} from '@angular/core';
import {AlertService} from '../../services/alert/alert.service';
import {RequestAuthenticationService} from '../../services/request/request-authentication.service';
import {I18nTranslatableClass} from '../../services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../services/i18n/i18n-translation.service';

@Component({
    selector: 'app-action',
    templateUrl: './action.component.html'
})
export class ActionComponent extends I18nTranslatableClass implements OnInit {

    /**
     * @description Data model to be provided to the component
     */
    @Input() public model: any;

    /**
     * @description Constructor of ActionComponent component
     *
     * The constructor creates an instance of the ActionComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param alertService A reference to the alert service (AlertService) of the application
     * @param authenticationService A reference to the authentication service (RequestAuthenticationService) of the application
     * @param translationService A reference to the i18n translation service of the application
     */
    constructor(private alertService: AlertService, private authenticationService: RequestAuthenticationService,
                translationService: I18nTranslationService) {
        super(translationService);
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
    }

    /**
     * @description This method returns the current language of the application. Value available 'en' | 'fr'
     */
    lang(): string {
        return this.translationService.lang;
    }
}


