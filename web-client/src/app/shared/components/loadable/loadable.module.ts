/**
 * Import of Angular's modules
 */
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

/**
 * Import of application components
 */
import {LoadableContentComponent} from './loadable-content/loadable-content.component';
import {LoadableDescriptionComponent} from './loadable-description/loadable-description.component';
import {LoadableSpinnerComponent} from './loadable-spinner/loadable-spinner.component';
import {LoadableComponent} from './loadable.component';

/**
 * Import of application modules
 */
import {SpinnerModule} from './loadable-spinner/spinner/spinner.module';

@NgModule({
    imports: [
        CommonModule,
        SpinnerModule
    ],
    declarations: [
        LoadableContentComponent,
        LoadableDescriptionComponent,
        LoadableSpinnerComponent,
        LoadableComponent
    ],

    exports: [
        LoadableContentComponent,
        LoadableDescriptionComponent,
        LoadableSpinnerComponent,
        LoadableComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LoadableModule {
}
