import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {LoaderService} from '../../services/loader-spinner/loader.service';

/**
 * This component is the base component of the loader service.
 */
@Component({
    selector: 'app-loadable',
    templateUrl: './loadable.component.html',
    animations: [
        trigger('fadeInOut', [
            state('void', style({
                opacity: 0,
            })),
            transition('void => *', animate(300)),
            transition('* => void', animate(350)),
        ]),
    ],
})
export class LoadableComponent implements OnInit, OnDestroy {

    /**
     * @description loader spinner id
     */
    @Input() public loaderId: string;

    /**
     * @description state which defines if the loader is activated or not
     */
    @Input() public initialState: boolean;

    /**
     * @description state which defines if the loader is fixed or not
     */
    @Input() public fixed: boolean;

    /**
     * @description loader spinner animation state
     */
    public animationState: string;

    /**
     * @description Constructor of LoadableComponent component
     *
     * The constructor creates an instance of the LoadableComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param loaderService A reference to the loader service of the application
     */
    constructor(private loaderService: LoaderService) {
        this.fixed = true;
        this.loaderId = '';
        this.initialState = true;
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.animationState = 'in';

        this.loaderService.register(this.loaderId, this.initialState);
        this.loaderService.getObservable(this.loaderId).subscribe((status) => {
            this.initialState = status;
        });
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular delete the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        this.loaderService.unregister(this.loaderId);
    }
}
