import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AlertService} from '../../services/alert/alert.service';
import {RequestAuthenticationService} from '../../services/request/request-authentication.service';
import {I18nTranslatableClass} from '../../services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../services/i18n/i18n-translation.service';

@Component({
    selector: 'app-package',
    templateUrl: './package.component.html'
})
export class PackageComponent extends I18nTranslatableClass implements OnInit {

    /**
     * @description PackageComponent title
     */
    public title: string;

    /**
     * @description Defined if user consumption is to be displayed
     */
    @Input() public showProgress: boolean;

    /**
     * @description User consumption
     */
    @Input() public consumption: number;

    /**
     * @description Defined if the PackageComponent is selected
     */
    @Input() public selected: boolean;

    /**
     * @description Data model to be provided to the component
     */
    @Input() public package: any;

    /**
     * @description This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * In the ActionComponent, this EventEmitter allows the sending of a boolean to indicate that the package
     * has been selected.
     */
    @Output() public packageEmitter: EventEmitter<{ id: number, selected: boolean }>;

    /**
     * @description Constructor of PackageComponent component
     *
     * The constructor creates an instance of the PackageComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param alertService A reference to the alert service of the application
     * @param authenticationService A reference to the authentication service of the application
     * @param translationService A reference to the i18n translation service of the application
     */
    constructor(private alertService: AlertService, private authenticationService: RequestAuthenticationService,
                translationService: I18nTranslationService) {
        super(translationService);
        this.package = null;
        this.showProgress = false;
        this.packageEmitter = new EventEmitter<{ id: number, selected: boolean }>();
    }

    /**
     * @description This method emits an event when the selected value of the PackageComponent changes.
     */
    change(): void {
        this.packageEmitter.emit({id: this.package.id, selected: this.selected});
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.title = 'PACKAGE/' + this.package.name;
    }

    /**
     * @description This method makes it possible to define the rate of consumption of the offer by the user.
     */
    usage(): number {
        return this.consumption > this.package.dailyPhotos ? this.package.dailyPhotos : this.consumption;
    }
}


