/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of Nebular's module
 */
import {NbActionsModule, NbCardModule, NbIconModule, NbProgressBarModule} from '@nebular/theme';
/**
 * Import of of the library's dependencies
 */
import {CountUpModule} from 'countup.js-angular2';
/**
 * Import of application's components
 */
import {PackageComponent} from './package.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NbIconModule,
        NbCardModule,
        NbActionsModule,
        NbProgressBarModule,
        CountUpModule
    ],
    declarations: [
        PackageComponent
    ],
    exports: [
        PackageComponent
    ],
})
export class PackageModule {
}
