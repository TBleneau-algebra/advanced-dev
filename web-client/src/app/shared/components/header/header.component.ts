import {Component, Input, NgZone, OnInit} from '@angular/core';
import {NbSidebarService} from '@nebular/theme';
import {Router} from '@angular/router';
import {AlertService} from '../../services/alert/alert.service';
import {RequestAuthenticationService} from '../../services/request/request-authentication.service';
import {I18nTranslationService} from '../../services/i18n/i18n-translation.service';
import {I18nTranslatableClass} from '../../services/i18n/i18n-class/i18n-translatable.class';
import {LoaderSemaphoreClass} from '../../services/loader-spinner/loader-semaphore/loader-semaphore.class.';

@Component({
    selector: 'app-header',
    styleUrls: ['./header.component.scss'],
    templateUrl: './header.component.html',
})
export class HeaderComponent extends I18nTranslatableClass implements OnInit {

    /**
     * @description Application title
     */
    @Input() public title: string;

    /**
     * @description Application selected language
     */
    private _selectedLanguage: string;

    /**
     * @description Application languages 'English' | 'French'
     */
    private _languages: Array<any>;

    /**
     * @description Constructor of HeaderComponent component
     *
     * The constructor creates an instance of the HeaderComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param sidebarService A reference to the Nebular sidebar service
     * @param alertService A reference to the alert service of the application
     * @param authenticationService A reference to the authentication service of the application
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param translationService A reference to the i18n application service for translation
     * @param zone An injectable service for executing work inside or outside of the Angular zone
     */
    constructor(private sidebarService: NbSidebarService, private alertService: AlertService,
                private authenticationService: RequestAuthenticationService, private router: Router,
                translationService: I18nTranslationService, private zone: NgZone) {
        super(translationService);
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit() {
        this.languages = [
            {value: 'fr', name: 'HEADER/FRENCH', asset: 'flag_french.png'},
            {value: 'en', name: 'HEADER/ENGLISH', asset: 'flag_english.png'}
        ];
        this.selectedLanguage = this.translationService.lang;
    }

    /**
     * @description This method allows a guest user to access the application menu
     */
    guest(): boolean {
        const roles: string[] = JSON.parse(localStorage.getItem('USER_ROLE'));

        if (roles !== null) {
            return !!roles.find(item => item === 'guest');
        }
        return this.authenticationService.hasRole('guest');
    }

    /**
     * @description A method to log out the user via the RequestAuthenticationService service
     */
    logout(): void {
        this.alert().then(response => {
            if (response) {
                this.authenticationService.activeLoader(new LoaderSemaphoreClass(1));

                this.authenticationService.get('/user/logout', null, null).then(next => {
                    if (next.responseStatus === 200) {
                        this.authenticationService.setRoles();
                        localStorage.removeItem('USER_ROLE');

                        this.navigate('/authentication');
                    }
                });
            }
        });
    }

    /**
     * @description The method uses the Angular router and allows you to navigate the application's routes
     *
     * @param path Path to navigate to
     */
    navigate(path: string): void {
        this.router.navigate([path]).then(response => {
        });
    }

    /**
     * @description The method allows you to reduce or enlarge the navigation bar
     */
    toggle(): void {
        this.sidebarService.toggle(true, 'menu-sidebar');
    }

    /**
     * @description This method makes it possible to translate the label of the NbSelectComponent thanks to
     * the translation service of the application
     */
    currentLanguage(): string {
        if (this.languages.find(item => item.value === this.selectedLanguage)) {
            return this.translate(this.languages.find(item => item.value === this.selectedLanguage)['name']);
        } else {
            return this.translate(this.languages.find(item => item.value === this.translationService.lang)['name']);
        }
    }

    /**
     * @description This method allows you to change the language of the application 'French' | 'English'
     *
     * @param event language code selected 'en' | 'fr'
     */
    changeLanguages(event: string) {
        if (this.translationService.lang !== event) {
            this.translationService.changeLang(event);
            this.translationService.reloadTranslation()
                .then(() => {
                    this.zone.run(() => {
                    });
                });
        }
    }

    /**
     * @description This method ensures that the user is willing to disconnect from the platform.
     * It uses the AlertService service
     */
    private alert(): PromiseLike<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.alertService.confirm(this.translate('HEADER/LOGOUT_QUESTION'),
                this.translate('HEADER/YES'), this.translate('HEADER/NO'))
                .subscribe((confirm) => {
                    resolve(confirm);
                });
        });
    }

    /**
     * @description This method allows you to retrieve the application languages 'English' | 'French'
     */
    get languages(): Array<any> {
        return this._languages;
    }

    /**
     * @description This method allows you to assign the application languages 'English' | 'French'
     *
     * @param value Value of the application languages
     */
    set languages(value: Array<any>) {
        this._languages = value;
    }

    /**
     * @description This method allows you to retrieve the application selected language
     */
    get selectedLanguage(): string {
        return this._selectedLanguage;
    }

    /**
     * @description This method allows you to assign the application selected language
     *
     * @param value Value of the application selected language
     */
    set selectedLanguage(value: string) {
        this._selectedLanguage = value;
    }
}
