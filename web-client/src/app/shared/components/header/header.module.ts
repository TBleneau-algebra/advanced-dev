/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

/**
 * Import of Nebular's modules
 */
import {
    NbActionsModule,
    NbButtonModule,
    NbContextMenuModule,
    NbIconModule,
    NbSelectModule,
    NbToggleModule,
    NbUserModule
} from '@nebular/theme';

/**
 * Import of application's components
 */
import {HeaderComponent} from './header.component';

@NgModule({
    imports: [
        CommonModule,
        NbActionsModule,
        NbUserModule,
        NbIconModule,
        NbSelectModule,
        NbContextMenuModule,
        RouterModule,
        NbButtonModule,
        NbToggleModule
    ],
    exports: [
        HeaderComponent
    ],
    declarations: [
        HeaderComponent
    ]
})
export class HeaderModule {
}
