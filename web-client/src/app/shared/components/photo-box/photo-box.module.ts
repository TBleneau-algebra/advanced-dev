/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of Nebular's module
 */
import {NbActionsModule, NbCardModule, NbContextMenuModule, NbIconModule, NbMenuModule, NbUserModule} from '@nebular/theme';
/**
 * Import of application's components
 */
import {PhotoBoxComponent} from './photo-box.component';
/**
 * Import of application's modules
 */
import {PipesModule} from '../../pipes/pipes.module';


@NgModule({
    imports: [
        CommonModule,
        NbIconModule,
        NbActionsModule,
        NbCardModule,
        PipesModule,
        NbMenuModule,
        NbUserModule,
        NbContextMenuModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        PhotoBoxComponent
    ],
    exports: [
        PhotoBoxComponent
    ],
})
export class PhotoBoxModule {
}
