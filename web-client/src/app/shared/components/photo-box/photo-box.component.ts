import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AlertService} from '../../services/alert/alert.service';
import {RequestAuthenticationService} from '../../services/request/request-authentication.service';
import {I18nTranslatableClass} from '../../services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../services/i18n/i18n-translation.service';
import * as moment from 'moment';
import {NbMenuService} from '@nebular/theme';
import {filter} from 'rxjs/operators';
import {RequestService} from '../../services/request/request.service';

@Component({
    selector: 'app-photo-box',
    templateUrl: './photo-box.component.html'
})
export class PhotoBoxComponent extends I18nTranslatableClass implements OnInit, AfterViewInit {

    /**
     * @description Tag identifier of the PhotoBoxComponent component
     */
    private _tag: string;

    /**
     * @description NbContextMenu items
     */
    private items: any;

    /**
     * @description Editable content (title and description of the image)
     */
    private _editContent: { title: HTMLElement, description: HTMLElement };

    /**
     * @description edit mode. It's an option to allow a user to edit the information of his picture
     */
    @Input() public editable: boolean;

    /**
     * @description edit mode status (activated or not)
     */
    @Input() public onEdit: boolean;

    /**
     * @description Data model to be provided to the component
     */
    @Input() public box: any;

    /**
     * @description current user's id
     */
    @Input() public userId: number;

    /**
     * @description This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * In the PhotoBoxComponent, this EventEmitter allows the sending of an information to indicate that the user want
     * to execute an action 'edit' | 'download' | 'delete'
     */
    @Output() public eventEmitter: EventEmitter<{ event: string, data: any }>;

    /**
     * @description Constructor of PhotoBoxComponent component
     *
     * The constructor creates an instance of the PhotoBoxComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param alertService A reference to the alert service of the application
     * @param authenticationService A reference to the authentication service of the application
     * @param translationService A reference to the i18n translation service of the application
     * @param requestService A reference to the user service of the application
     * @param menuService A reference to the Nebular NbMenuService
     */
    constructor(private alertService: AlertService, private authenticationService: RequestAuthenticationService,
                translationService: I18nTranslationService, private requestService: RequestService,
                private menuService: NbMenuService) {
        super(translationService);
        this.box = null;

        this.items = [
            {title: this.translate('PHOTO_BOX/IMAGE'), code: 'image'},
            {title: this.translate('PHOTO_BOX/IMAGE_FILTER'), code: 'image-filter'}
        ];

        this.onEdit = false;
        this.editable = false;
        this.eventEmitter = new EventEmitter<{ event: string, data: any }>();
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.tag = Math.floor(1000 + Math.random() * 9000).toString();
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular has finished to initialize the view of a component.
     * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterViewInit(): void {
        this._editContent = {
            title: document.getElementById('editor-title-' + this.tag),
            description: document.getElementById('editor-description-' + this.tag)
        };

        this.menuService.onItemClick()
            .pipe(
                filter(({tag}) => tag === 'menu_' + this.tag),
            )
            .subscribe(value => this.onMenuClick(value));
    }

    /**
     * @description La méthode est utilisée lorsqu'un élément de clique est intercepté sur le menu contextuel de l'utilisateur
     *
     * @param event Evenement de clique sur le menu contextuel de l'utilisateur
     */
    onMenuClick(event: any): void {
        switch (event.item.code) {
            case 'image':
                this.eventEmitter.emit({event: 'download', data: {image: this.box.image, format: this.box.format}});
                break;
            case 'image-filter':
                (this.box.imageFilter) ?
                    this.eventEmitter.emit({event: 'download', data: {image: this.box.imageFilter, format: this.box.format}}) :
                    undefined;
                break;
        }
    }

    /**
     * @description The method allows to know if the user is logged in
     */
    logged(): boolean {
        return this.authenticationService.isAuthenticated();
    }

    /**
     * @description The method allows you to display a close-up image
     *
     * @param path image path to display
     */
    viewerImage(path: string): void {
        if (window.innerWidth >= 992) {
            this.alertService.image(path, window.innerWidth * 0.6, window.innerHeight * 0.75);
        }
    }

    /**
     * @description The method allows you to display a close-up image and its description
     */
    viewerDescription(): void {
        moment.locale(this.lang());

        const image: string = this.box.imageFilter ? this.box.imageFilter : this.box.image;
        const tags = this.tagsConcatenated();
        const date: string = (this.lang() === 'en') ? moment(this.box.createdAt, moment.ISO_8601).format('dddd, MMM Do YYYY - HH:mm:ss') :
            moment(this.box.createdAt, moment.ISO_8601).format('dddd Do MMM YYYY - HH:mm:ss');

        const html: string = '<div class="row swal-popup-desc h-100"><div class="col-lg-7">' +
            '<img src="' + image + '">' +
            '</div><div class="col-lg-5"><h2>' + this.box.title + '</h2>' +
            '<h3>' + this.box.user.name + ' ' + this.box.user.lastname + '</h3>' +
            '<div class="swal-popup-desc-text"><p>' + this.box.description + '</p></div>' +
            '<div class="swal-popup-desc-tags"><p>' + tags + '</p><span style="width: 100%; right: 0;">' + date + '</span></div>' +
            '</div></div>';

        if (window.innerWidth >= 992) {
            this.alertService.html(html);
        }
    }

    /**
     * @description This method allows you to delete an image.
     * It sends an event to the parent component so that it can process the request.
     */
    delete(): void {
        this.alertService.confirm(this.translate('PHOTO_BOX/DELETE_QUESTION'),
            this.translate('PHOTO_BOX/YES'), this.translate('PHOTO_BOX/NO'))
            .subscribe(value => {
                if (value) {
                    this.eventEmitter.emit({event: 'delete', data: this.box});
                }
            });
    }

    /**
     * @description The method allows changes to be made to the image in the PhotoBoxComponent component
     */
    change(): void {
        const title: string = this._editContent.title.innerText;
        const descripton: string = this._editContent.description.innerText;

        if (title && title !== '' && descripton && descripton !== '') {
            if (title !== this.box.title || descripton !== this.box.description) {
                this.box.title = title;
                this.box.description = descripton;

                this.eventEmitter.emit({event: 'edit', data: this.box});
            }
        } else {
            this._editContent.title.innerText = this.box.title;
            this._editContent.description.innerText = this.box.description;
        }
        this.onEdit = false;
    }

    /**
     * @description This method returns the current language of the application. Value available 'en' | 'fr'
     */
    lang(): string {
        return this.translationService.lang;
    }

    /**
     * @description The method concatenates the tags relative to an image
     */
    private tagsConcatenated(): string {
        let tags: string = '';

        if (this.box.tags) {
            this.box.tags.forEach((tag, index) => {
                tags += '#' + tag.name.toUpperCase();
                tags += (index < this.box.tags.length - 1) ? ', ' : '';
            });
        }
        return tags;
    }

    /**
     * @description The method allows you to retrieve the PhotoBoxComponent component
     */
    get tag(): string {
        return this._tag;
    }

    /**
     * @description The method allows you to assign the PhotoBoxComponent component
     *
     * @param value Value of the new tag identifier of the PhotoBoxComponent component
     */
    set tag(value: string) {
        this._tag = value;
    }
}


