/**
 * Import of Angular's mdoules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

/**
 * Import NgSelectModule
 */
import {NgSelectModule} from '@ng-select/ng-select';

/**
 * Import of the application's components
 */
import {ViewComponent} from './view.component';

@NgModule({
    declarations: [
        ViewComponent
    ],
    imports: [
        CommonModule,
        NgSelectModule
    ],
    exports: [
        ViewComponent
    ]
})
export class ViewModule {
}
