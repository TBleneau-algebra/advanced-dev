import {
    AfterContentInit,
    Component,
    ContentChildren,
    EventEmitter, forwardRef,
    Inject, Input,
    Optional,
    Output,
    QueryList,
    SkipSelf
} from '@angular/core';
import {FilterEmitterClass} from '../filter/models/filter-emitter.class';
import {FilterInputClass} from '../filter/models/filter-input.class';
import {FilterComponent} from '../filter/filter.component';
import {FilterInputEmitterClass} from '../filter/models/filter-input-emitter.class';
import {FilterClass} from '../filter/models/filter.class';

@Component({
    selector: 'app-view',
    template: '<ng-content></ng-content>',
})
export class ViewComponent implements AfterContentInit {

    /**
     * @description This variable represents the identification name of the component ViewComponent
     */
    @Input() public idName: string;

    /**
     * @description A reference to the parent component ViewComponent (if it exists)
     */
    private _viewParentComponent: ViewComponent;

    /**
     * @description This variable (false by default) allows an ViewComponent to subscribe or
     * no to a parent component of the same type (if this parent exists).
     */
    @Input() public viewSubscription: boolean;

    /**
     * @description This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * In the ViewComponent, this EventEmitter allows the sending of filters for each component
     * FilterComponent child. Components that subscribe to this EventEmitter can therefore receive the
     * filter change.
     */
    @Output() public viewFiltersEmitter: EventEmitter<FilterEmitterClass>;

    /**
     * @description A QueryList variable of the components of type FilterInputClass from the content DOM.
     *
     * Each time a child item is added, deleted or moved, the list is updated and changes are made
     * of observables in the list issue a new value.
     */
    @ContentChildren(FilterInputClass, {descendants: true}) inputsComponent !: QueryList<FilterInputClass>;

    /**
     * @description A QueryList variable of the components of type ViewComponent from the DOM
     * of content.
     *
     * Each time a child item is added, deleted or moved, the list is updated and changes are made
     * of observables in the list issue a new value.
     */
    @ContentChildren(ViewComponent, {descendants: true}) viewsChildrenComponent !: QueryList<ViewComponent>;

    /**
     * @description A QueryList variable of the components of type FilterComponent from the DOM
     * of content.
     *
     * Each time a child item is added, deleted or moved, the list is updated and changes are made
     * of observables in the list issue a new value.
     */
    @ContentChildren(FilterComponent, {descendants: true}) filtersComponent !: QueryList<FilterComponent>;

    /**
     * @description Constructor of the component ViewComponent
     *
     * The constructor creates an instance of the ViewComponent component and specifies the default values
     * of the component's input and output variables.
     *
     * @param viewParent Optional parent component. It should be a ViewComponent component
     */
    constructor(@Optional() @SkipSelf() @Inject(forwardRef(() => ViewComponent))
                private viewParent: ViewComponent) {

        this.viewFiltersEmitter = new EventEmitter<FilterEmitterClass>();
        this.viewParentComponent = (viewParent === null || viewParent === undefined) ? null : viewParent;
    }

    /**
     * @dscription: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called after Angular has completely initialized the content of the component.
     * Defining an ngAfterContentInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterContentInit(): void {
        this.onPurgeViewsChildren();
        this.onPurgeViewInput();
        this.onPurgeViewFilters();
        this.onSubscribe();
    }

    /**
     * @description The method is used to process the change in value of an FilterComponent component and
     * to retrieve all the filters to be sent to the components subscribed to the viewFiltersEmitter variable
     *
     * {@link viewFiltersEmitter}.
     */
    private onFiltersChange(emitter: string): void {
        this.viewFiltersEmitter.emit({emitter: emitter, filter: this.getFilters()});
    }

    /**
     * @description The method is used to process the change in value of an FilterInputClass component.
     *
     * @param item Value sent by a component of type FilterInputClass when a change is detected
     */
    private onValuesChange(item: FilterInputEmitterClass): void {
    }

    /**
     * @description This method is called during the life cycle of the ngAfterContentInit component.
     *
     * It allows you to manage the inputs to which the view has the right to subscribe. Indeed, a component
     * ViewComponent should only be able to subscribe to its own inputs.
     *
     * For example, it should not be able to subscribe to an input whose parent is a component
     * FilterComponent.
     */
    private onPurgeViewInput(): void {
        this.filtersComponent.forEach(filter => {
            filter.inputs.forEach(input => {
                this.inputsComponent.reset(this.inputsComponent.filter(item => input !== item));
            });
        });
    }

    /**
     * @description This method is called during the life cycle of the ngAfterContentInit component.
     *
     * It allows you to manage the filters to which the view has the right to subscribe. Indeed, a component
     * ViewComponent should only be able to subscribe to its own filters.
     *
     * For example, it should not be able to subscribe to a filter whose parent is a component of the type
     * FilterInputClass.
     */
    private onPurgeViewFilters(): void {
        this.filtersComponent.forEach(filter => {
            (filter.inputParentComponent) ? this.filtersComponent.reset(this.filtersComponent.filter(item =>
                filter !== item)) : undefined;
        });
    }

    /**
     * @description This method is called during the life cycle of the ngAfterContentInit component.
     *
     * It allows you to manage the filters to which the view has the right to subscribe. Indeed, a component
     * ViewComponent should only be able to subscribe to its own filters and inputs.
     *
     * This method iterates on child components of type ViewComponent and filters the
     * elements (filters, inputs) to which the parent component can subscribe.
     */
    private onPurgeViewsChildren(): void {
        if (this.viewsChildrenComponent) {

            this.viewsChildrenComponent.forEach(view => {
                (view !== this) ?
                    view.filtersComponent.forEach(filter => {
                        this.filtersComponent.reset(this.filtersComponent.filter(item => item !== filter));
                    }) : undefined;
            });

            this.viewsChildrenComponent.forEach(view => {
                (view !== this) ?
                    view.inputsComponent.forEach(input => {
                        this.inputsComponent.reset(this.inputsComponent.filter(item => item !== input));
                    }) : undefined;
            });
        }
    }

    /**
     * @description This method is called during the life cycle of the ngAfterContentInit component.
     *
     * It manages EventEmitter subscriptions for input, filter and (if available) parent view components.
     */
    private onSubscribe(): void {

        this.inputsComponent.forEach(input => input.inputEmitter
            .asObservable()
            .subscribe(item => this.onValuesChange(item)));

        this.filtersComponent.forEach(filter => filter.filtersEmitter
            .asObservable()
            .subscribe(value => {
                (value) ? this.onFiltersChange(value.emitter) : undefined;
            }));

        (this.viewParentComponent !== null && this.viewSubscription) ?
            this.viewParentComponent.viewFiltersEmitter
                .asObservable()
                .subscribe(value => {
                    this.onFiltersChange(value.emitter);
                }) : undefined;
    }

    /**
     * @description The method allows you to retrieve all the child filters of the ViewComponent component.
     *
     * If this component has a parent of the same type and that parent is a subscriber, then the component also retrieves the
     * filters of its parent
     */
    getFilters(): FilterClass {
        let filter: FilterClass = new FilterClass();

        if (this.viewParentComponent && this.viewSubscription) {
            filter = this.viewParentComponent.getFilters();
        }

        this.filtersComponent.forEach(item => {
            if (item.filter !== null) {
                filter.filters.concat(item.filter.filters);
            }
        });
        return filter;
    }

    /**
     * @description The method allows you to retrieve the parent component (if defined) of type ViewComponent.
     *
     * If no parent component of type ViewComponent is defined, the method will return null.
     */
    get viewParentComponent(): ViewComponent {
        return this._viewParentComponent;
    }

    /**
     * @description The method is used to assign the parent component of type ViewComponent.
     *
     * @param value Value of the parent component assigned
     */
    set viewParentComponent(value: ViewComponent) {
        this._viewParentComponent = value;
    }
}
