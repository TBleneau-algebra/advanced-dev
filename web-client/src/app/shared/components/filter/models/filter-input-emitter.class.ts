/**
 * @description The FilterInputEmitterClass class is an event sent or received when data is
 * used as filters
 */
export class FilterInputEmitterClass {

    /**
     * @description Allows to define, in the case of a select, if the selection is multiple or not
     */
    public multiple: boolean;

    /**
     * @description Data emitted by a component of type FilterInputClass
     */
    public data: any;

    /**
     * @description Constructor of FilterInputEmitterClass
     *
     * The constructor creates an instance of the FilterInputEmitterClass class and specifies the default values of
     * the variables of the component.
     *
     * @param multiple Allows to define, in the case of a select, if the selection is multiple or not
     * @param data Data emitted by a component of type FilterInputClass
     */
    constructor(multiple?: boolean, data?: any) {
        this.multiple = multiple || false;
        this.data = data || null;
    }
}
