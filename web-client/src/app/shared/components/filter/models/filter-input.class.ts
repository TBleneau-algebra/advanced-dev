import {EventEmitter, Output} from '@angular/core';
import {FilterInputEmitterClass} from './filter-input-emitter.class';
import {I18nTranslatableClass} from '../../../services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../../services/i18n/i18n-translation.service';

/**
 * @description The abstract class FilterInputClass allows you to define a class type for all our components
 * of the "input" type whose purpose is to play with the data values. These components can be used under
 * the shape of filters.
 *
 * In this sense, an FilterComponent will therefore be subscribed to the change of values of a component of the type
 * FilterInputClass.
 */
export abstract class FilterInputClass extends I18nTranslatableClass {

    /**
     * @description This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * In an FilterInputClass component, this EventEmitter allows you to send updated data.
     */
    @Output() public inputEmitter: EventEmitter<FilterInputEmitterClass>;

    /**
     * Constructor of the FilterInputClass class
     *
     * The constructor creates an instance of the FilterInputClass class and specifies the default values
     * of the component's input and output variables.
     *
     * @param translationService A reference to the i18n service of the application
     */
    protected constructor(translationService: I18nTranslationService) {
        super(translationService);
    }
}

