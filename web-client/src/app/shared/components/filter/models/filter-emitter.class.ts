import {FilterClass} from './filter.class';

/**
 * @descuription The FilterEmitterClass class is an event sent or received when data
 * are used as filters.
 */
export class FilterEmitterClass {

    /**
     * @description Name of the component emitting the filter
     */
    public emitter: string;

    /**
     * @description Filters to be provided from an Http request to the API
     */
    public filter: FilterClass;

    /**
     * @description Constructor of FilterEmitterClass
     *
     * The constructor creates an instance of the FilterEmitterClass class and specifies the default values of
     * the variables of the component.
     *
     * @param emitter Name of the component emitting the filter
     * @param filter Filters to be provided from an Http request to the API
     */
    constructor(emitter?: string, filter?: FilterClass) {
        this.emitter = emitter || '';
        this.filter = filter || new FilterClass();
    }
}
