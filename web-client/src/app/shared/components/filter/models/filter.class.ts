/**
 * @description The FilterClass class defines the search filters to be provided when
 * from an Http request to the API
 */
export class FilterClass {

    /**
     * @description Filters used in the http request
     */
    public filters: Array<{ name: string, value: any; }>;

    /**
     * @description Constructor of FilterClass
     *
     * The constructor creates an instance of the  FilterClass class and specifies the default values of
     * the variables of the component.
     *
     * @param value Value used as filter
     */
    constructor(value?: Array<{ name: string, value: any }>) {
        this.filters = (value) ? value : new Array<{ name: string, value: any }>();
    }
}
