/**
 * Import of Angular's mdoules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

/**
 * Import of the library's components
 */
import {FilterComponent} from './filter.component';

@NgModule({
    declarations: [
        FilterComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        FilterComponent
    ]
})
export class FilterModule {
}
