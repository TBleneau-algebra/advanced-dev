import {AfterContentInit, Component, ContentChildren, EventEmitter, Host, Input, Optional, Output, QueryList} from '@angular/core';
import {FilterEmitterClass} from './models/filter-emitter.class';
import {FilterInputEmitterClass} from './models/filter-input-emitter.class';
import {FilterInputClass} from './models/filter-input.class';
import {FilterClass} from './models/filter.class';

@Component({
    selector: 'app-filter',
    template: '<ng-content></ng-content>',
})
export class FilterComponent implements AfterContentInit {

    /**
     * @description A reference to the filter variable of the component
     */
    private _filter: FilterClass;

    /**
     * @description A reference to the parent component InputComponent (if it exists)
     */
    private _inputParentComponent: FilterInputClass;

    /**
     * @dscription This variable represents the identification name of the component FilterComponent
     */
    @Input() public idName: string;

    /**
     * @description This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * In the FilterComponent, this EventEmitter allows the sending of a boolean to indicate that the filter
     * has been updated.
     */
    @Output() public filtersEmitter: EventEmitter<FilterEmitterClass>;

    /**
     * @description A QueryList variable of the components of type FilterInputClass from the content DOM.
     *
     * Each time a child item is added, deleted or moved, the list is updated and changes are made
     * of observables in the list issue a new value.
     */
    @ContentChildren(FilterInputClass, {descendants: true}) public inputs !: QueryList<FilterInputClass>;

    /**
     * @description Constructor of the component FilterComponent
     *
     * The constructor creates an instance of the component FilterComponent and specifies the default values
     * of the component's input and output variables.
     *
     * @param inputParent A reference (if it exists) to a parent component of type FilterInputClass
     */
    constructor(@Optional() @Host() private inputParent: FilterInputClass) {
        this.filter = new FilterClass();
        this.filtersEmitter = new EventEmitter<FilterEmitterClass>();
        this.inputParentComponent = (inputParent === null || inputParent === undefined) ? null : inputParent;
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called after Angular has completely initialized the content of the component.
     * Defining an ngAfterContentInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterContentInit(): void {
        if (this.inputs.length > 0) {
            this.inputs.forEach(input => input.inputEmitter
                .asObservable()
                .subscribe(value => this.handleFilter(value)));
        }
    }

    /**
     * @description The method is called when a filter receives a data change event from a component
     * FilterInputClass.
     *
     * This method processes the data with the parameters entered during the component instantiation and sends
     * a change event following the filter update.
     */
    handleFilter(value?: FilterInputEmitterClass) {
        this.clearFilters();

        this.filter.filters.push(value.data);
        this.filtersEmitter.emit({emitter: 'filter', filter: this.filter});
    }

    /**
     * @description The method is called when you want to delete all search filters and orders
     * of search present in the object FilterClass
     */
    public clearFilters(): void {
        if (this.filter) {
            this.filter.filters.splice(0, this.filter.filters.length);
        }
    }

    /**
     * @description The method allows you to retrieve the filter of the FilterComponent component
     *
     * @return FilterClass a reference of the component filter
     */
    get filter(): FilterClass {
        return this._filter;
    }

    /**
     * @description The method is used to assign the filter of the FilterComponent
     */
    set filter(value: FilterClass) {
        this._filter = value;
    }

    /**
     * @description The method allows you to retrieve the parent component (if defined) of type FilterInputClass.
     *
     * If no parent component of type FilterInputClass is defined, the method will return null.
     */
    get inputParentComponent(): FilterInputClass {
        return this._inputParentComponent;
    }

    /**
     * @description The method is used to assign the parent component of type FilterInputClass.
     *
     * @param value Value of the parent component assigned
     */
    set inputParentComponent(value: FilterInputClass) {
        this._inputParentComponent = value;
    }
}
