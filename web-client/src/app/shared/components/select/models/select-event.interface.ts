/**
 * @description This interface defines the name of the event emitted by the ng-select component as well as the data
 * (if any) associated.
 */
export interface SelectEventInterface {

    /**
     * @description Name of the event emitted
     */
    name: string;

    /**
     * @description Data emitted
     */
    data?: any;
}
