/**
 * @description This interface defines the start and end index of the elements available in the select.
 * Can be used to load more items before the user scrolls down the list.
 */
export interface SelectScrollInterface {

    /**
     * @description The start index of the elements available in the select
     */
    start: number;

    /**
     * @description The end index of the elements available in the select
     */
    end: number;
}
