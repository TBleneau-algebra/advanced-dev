/**
 * @description This interface defines the format whose output (search) of the ng-select component represents a search.
 *
 * We find the search term as well as the data sorted according to the search term.
 */
export interface SelectSearchInterface {

    /**
     * @description Search term
     */
    term: string;

    /**
     * @description Data sorted according to the search term
     */
    items: Array<any>;
}
