/**
 * Import of Angular's mdoules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of of the library's dependencies
 */
import {NgSelectModule} from '@ng-select/ng-select';
/**
 * Import of the library's modules
 */
import {FilterModule} from '../filter/filter.module';
/**
 * Import of the library's components
 */
import {SelectComponent} from './select.component';
import {SelectTagComponent} from './select-tag/select-tag.component';
import {SelectFilterComponent} from './select-filter/select-filter.component';
import {SelectFormatComponent} from './select-format/select-format.component';


@NgModule({
    declarations: [
        SelectComponent,
        SelectTagComponent,
        SelectFilterComponent,
        SelectFormatComponent
    ],
    imports: [
        CommonModule,
        NgSelectModule,
        FormsModule,
        FilterModule,
        ReactiveFormsModule
    ],
    exports: [
        SelectComponent,
        SelectTagComponent,
        SelectFilterComponent,
        SelectFormatComponent
    ]
})
export class SelectModule {
}
