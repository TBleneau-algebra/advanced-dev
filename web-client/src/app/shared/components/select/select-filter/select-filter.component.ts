import {AfterViewInit, Component, forwardRef} from '@angular/core';
import {debounceTime} from 'rxjs/internal/operators';
import {SelectComponent} from '../select.component';
import {I18nTranslationService} from '../../../services/i18n/i18n-translation.service';
import {FilterInputClass} from '../../filter/models/filter-input.class';

@Component({
    selector: 'app-select-filter',
    templateUrl: './select-filter.component.html',
    providers: [{provide: FilterInputClass, useExisting: forwardRef(() => SelectFilterComponent)}],
})
export class SelectFilterComponent extends SelectComponent implements AfterViewInit {

    /**
     * @description Constructor of the component SelectFilterComponent
     *
     * The constructor creates an instance of the component SelectFilterComponent and specifies the default values
     * of the component's input and output variables.
     *
     * @param translationService A reference to the i18n translation service of the application
     */
    constructor(translationService: I18nTranslationService) {
        super(translationService);
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular has initialized the view of a component.
     * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterViewInit(): void {
        this.eventEmitter.asObservable().pipe(debounceTime(300)).subscribe(
            event => {
                if (event.name === 'search') {
                    this.doSearch();
                }
            });
    }

    /**
     * @description The method receives an event to indicate that the dropodown of the ng-select component is open
     *
     * It initializes the selection of the component when it is opened if no selection has been defined.
     */
    onOpen(): void {
        this.pageFrom = 0;
        this.loading = true;
        (this.items.length === 0) ? this.loadFilter() : this.loading = false;
    }

    /**
     * @description The method receives an event to indicate that it has reached the end of the component list
     * ng-select
     *
     * The method allows you to reload the following selection to propose it to the user.
     */
    onScrollToEnd(): void {
        this.pageFrom = this.items.length;
        this.loading = true;
        this.loadFilter();
    }

    /**
     * @description Resets "pageFrom", sets loading to true and triggers loading of people.
     */
    doSearch(): void {
        this.pageFrom = 0;
        this.loading = true;
        this.loadFilter();
    }

    /**
     * @description The method receives an event to indicate that the selection has been deleted by the user
     * following a click on the delete icon of the ng-select component
     *
     * It allows you to reset the selection of the ng-select component.
     */
    onClear(): void {
        if (this.selectComponent.searchTerm === null) {
            this.closeSelect();
            this.clearSelect();
            this.pageFrom = 0;
            this.loading = true;
            this.loadFilter();
        }
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that the selected element
     * has been updated (change in value).
     *
     * The method provides the selected element(s) in the'data' field of the issued object.
     *
     * @param data Data received when the value of the selected elements changes.
     */
    onChange(data: any): void {
        if (data) {
            this.inputEmitter.emit({multiple: this.multiple, data: {name: 'filter', value: data}});
        } else {
            this.inputEmitter.emit({multiple: this.multiple, data: {name: 'filter', value: {id: -1}}});
        }
    }

    /**
     * @description The method defines how the SelectFilterComponent should
     * behave when the component search is used or when it is simply used as a simple selector switch
     */
    private loadFilter(): void {
        this.request();
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method "list" of the FilterService service
     */
    private request(): void {
        this.items = [{id: 0, name: 'Gray Scale'}, {id: 1, name: 'Black and White'}, {id: 2, name: 'Sepia'},
            {id: 3, name: 'Emboss'}, {id: 4, name: 'Halftone'}, {id: 5, name: 'Edge'}, {id: 6, name: 'Invert'},
            {id: 7, name: 'Hand Drawing'}];
        this.loading = false;
    }
}
