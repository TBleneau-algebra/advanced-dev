import {AfterViewInit, Component, forwardRef} from '@angular/core';
import {debounceTime} from 'rxjs/internal/operators';
import {SelectComponent} from '../select.component';
import {I18nTranslationService} from '../../../services/i18n/i18n-translation.service';
import {FilterInputClass} from '../../filter/models/filter-input.class';
import {RequestParamsInterface} from '../../../services/request/params/request-params.interface';
import {RequestParamsListClass} from '../../../services/request/params/request-params-list.class';
import {RequestService} from '../../../services/request/request.service';

@Component({
    selector: 'app-select-tag',
    templateUrl: './select-tag.component.html',
    providers: [{provide: FilterInputClass, useExisting: forwardRef(() => SelectTagComponent)}],
})
export class SelectTagComponent extends SelectComponent implements AfterViewInit {

    /**
     * @description Total number of element returned by the API
     */
    private _elementTotal: number;

    /**
     * @description Total number of pages returned by the API
     */
    private _pageTotal: number;

    /**
     * @description Constructor of the component SelectTagComponent
     *
     * The constructor creates an instance of the component SelectTagComponent and specifies the default values
     * of the component's input and output variables.
     *
     * @param translationService A reference to the i18n translation service of the application
     * @param requestService A reference to the http request service of the application
     */
    constructor(translationService: I18nTranslationService, private requestService: RequestService) {
        super(translationService);

        this.loading = false;
        this.elementTotal = 0;
        this.pageTotal = 0;
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular has initialized the view of a component.
     * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterViewInit(): void {
        this.eventEmitter.asObservable().pipe(debounceTime(300)).subscribe(
            event => {
                if (event.name === 'search') {
                    this.doSearch();
                }
            });
    }

    /**
     * @description The method receives an event to indicate that the dropodown of the ng-select component is open
     *
     * It initializes the selection of the component when it is opened if no selection has been defined.
     */
    onOpen(): void {
        this.pageFrom = 0;
        this.loading = true;
        (this.items.length === 0) ? this.loadTag() : this.loading = false;
    }

    /**
     * @description The method receives an event to indicate that it has reached the end of the component list
     * ng-select
     *
     * The method allows you to reload the following selection to propose it to the user.
     */
    onScrollToEnd(): void {
        this.loadTag();
    }

    /**
     * @description Resets "pageFrom", sets loading to true and triggers loading of people.
     */
    doSearch(): void {
        this.pageFrom = 0;
        this.loadTag();
    }

    /**
     * @description The method receives an event to indicate that the selection has been deleted by the user
     * following a click on the delete icon of the ng-select component
     *
     * It allows you to reset the selection of the ng-select component.
     */
    onClear(): void {
        if (this.selectComponent.searchTerm === null) {
            this.closeSelect();
            this.clearSelect();
            this.elementTotal = 0;
            this.pageTotal = 0;
            this.pageFrom = 0;
            this.loadTag();
        }
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that the selected element
     * has been updated (change in value).
     *
     * The method provides the selected element(s) in the'data' field of the issued object.
     *
     * @param data Data received when the value of the selected elements changes.
     */
    onChange(data: any): void {
        this.inputEmitter.emit({multiple: this.multiple, data: {name: 'tags', value: data}});
    }

    /**
     * @description The method defines how the SelectFilterComponent should behave when the component search is used or
     * when it is simply used as a simple selector
     */
    private loadTag(): void {
        let params: RequestParamsInterface;
        const size: number = this.items.length;

        if (size !== 0 && (this.loading || size === this.elementTotal)) {
            return;
        }
        this.loading = true;

        this.pageFrom += (this.pageFrom < this.pageTotal) ? 1 : 0;
        params = new RequestParamsListClass(this.pageFrom.toString(), '20');

        this.requestService.get('/tag', null, params).then(response => {
            this.items = [...this.items, ...response.responseData.data];

            this.pageTotal = (response.responseHeaders.has('page.total')) ?
                parseInt(response.responseHeaders.get('page.total'), 10) : 0;

            this.elementTotal = (response.responseHeaders.has('element.total')) ?
                parseInt(response.responseHeaders.get('element.total'), 10) : 0;

            this.loading = false;
        }).catch(error => {
            this.loading = false;
        });
    }

    /**
     * @description The method allows you to retrieve the total number of element returned by the API
     */
    get elementTotal(): number {
        return this._elementTotal;
    }

    /**
     * @description The method allows you to assign the total number of element returned by the API
     *
     * @param value Value of the total number of element returned by the API
     */
    set elementTotal(value: number) {
        this._elementTotal = value;
    }

    /**
     * @description The method allows you to retrieve the total number of element returned by the API
     */
    get pageTotal(): number {
        return this._pageTotal;
    }

    /**
     * @description The method allows you to assign the total number of element returned by the API
     *
     * @param value Value of the total number of element returned by the API
     */
    set pageTotal(value: number) {
        this._pageTotal = value;
    }
}
