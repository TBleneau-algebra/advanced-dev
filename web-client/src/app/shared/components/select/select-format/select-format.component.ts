import {AfterViewInit, Component, forwardRef, OnInit} from '@angular/core';
import {SelectComponent} from '../select.component';
import {I18nTranslationService} from '../../../services/i18n/i18n-translation.service';
import {FilterInputClass} from '../../filter/models/filter-input.class';
import {debounceTime} from 'rxjs/operators';

@Component({
    selector: 'app-select-format',
    templateUrl: './select-format.component.html',
    providers: [{provide: FilterInputClass, useExisting: forwardRef(() => SelectFormatComponent)}],
})
export class SelectFormatComponent extends SelectComponent implements OnInit, AfterViewInit {

    /**
     * @description Constructor of the component SelectFormatComponent
     *
     * The constructor creates an instance of the component SelectFormatComponent and specifies the default values
     * of the component's input and output variables.
     *
     * @param translationService A reference to the i18n translation service of the application
     */
    constructor(translationService: I18nTranslationService) {
        super(translationService);
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.loadFormat();
        this.selected = this.items[0];
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular has initialized the view of a component.
     * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterViewInit(): void {
        this.eventEmitter.asObservable().pipe(debounceTime(300)).subscribe(
            event => {
                if (event.name === 'search') {
                    this.doSearch();
                }
            });
    }

    /**
     * @description The method receives an event to indicate that the dropodown of the ng-select component is open
     *
     * It initializes the selection of the component when it is opened if no selection has been defined.
     */
    onOpen(): void {
        this.pageFrom = 0;
        this.loading = true;
        if (this.items.length === 0) {
            this.loadFormat();
        }
        this.loading = false;
    }

    /**
     * @description The method receives an event to indicate that it has reached the end of the component list
     * ng-select
     *
     * The method allows you to reload the following selection to propose it to the user.
     */
    onScrollToEnd(): void {
        this.pageFrom = this.items.length;
        this.loading = true;
    }

    /**
     * @description The method receives an event to indicate that the selection has been deleted by the user
     * following a click on the delete icon of the ng-select component
     *
     * It allows you to reset the selection of the ng-select component.
     */
    onClear(): void {
        if (this.selectComponent.searchTerm === null) {
            this.closeSelect();
            this.clearSelect();
            this.pageFrom = 0;
            this.loading = true;
            this.loadFormat();
        }
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that the selected element
     * has been updated (change in value).
     *
     * The method provides the selected element(s) in the'data' field of the issued object.
     *
     * @param data Data received when the value of the selected elements changes.
     */
    onChange(data: any): void {
        if (data) {
            this.inputEmitter.emit({multiple: this.multiple, data: {name: 'format', value: data}});
        } else {
            this.inputEmitter.emit({multiple: this.multiple, data: {name: 'format', value: null}});
        }
    }

    /**
     * @description Resets "pageFrom", sets loading to true and triggers loading of people.
     */
    doSearch(): void {
        this.pageFrom = 0;
        this.loading = true;
        this.loadFormat();
    }

    /**
     * @description The method defines how the SelectFilterComponent should
     * behave when the component search is used or when it is simply used as a simple selector
     */
    private loadFormat(): void {
        this.items = [{id: 0, name: 'PNG', value: 'image/png'}, {id: 1, name: 'JPEG', value: 'image/jpeg'},
            {id: 2, name: 'WebP', value: 'image/webp'}];
        this.loading = false;
    }
}
