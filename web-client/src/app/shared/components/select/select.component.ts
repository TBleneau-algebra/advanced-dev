import {Component, ContentChildren, EventEmitter, Input, Output, QueryList, ViewChild} from '@angular/core';
import {NgSelectComponent} from '@ng-select/ng-select';
import {FilterComponent} from '../filter/filter.component';
import {SelectScrollInterface} from './models/select-scroll.interface';
import {SelectEventInterface} from './models/select-event.interface';
import {SelectSearchInterface} from './models/select-search.interface';
import {FilterInputEmitterClass} from '../filter/models/filter-input-emitter.class';
import {I18nTranslationService} from '../../services/i18n/i18n-translation.service';
import {FilterInputClass} from '../filter/models/filter-input.class';

@Component({
    selector: 'app-select',
    template: ''
})
export class SelectComponent extends FilterInputClass {

    /**
     * @description Start page
     */
    @Input() public pageFrom: number;

    /**
     * @description Size of one page returned by the API
     */
    @Input() public pageSize: number;

    /**
     * @description Allows manual control of dropdown opening and closing. True - won't close. False - won't open
     */
    @Input() public isOpen: boolean;

    /**
     * @description Set the loading state from the outside
     */
    @Input() public loading: boolean;

    /**
     * @description Object property to use for selected model. By default binds to whole object
     */
    @Input() public bindValue: string;

    /**
     * @description Object property to use for label. Default label
     */
    @Input() public bindLabel: string;

    /**
     * @description Items array
     */
    @Input() public items: Array<any>;

    /**
     * @description Allows to select multiple items
     */
    @Input() public multiple: boolean;

    /**
     * @description Set custom text when for loading items
     */
    @Input() public loadingText: string;

    /**
     * @description Placeholder text
     */
    @Input() public placeholder: string;

    /**
     * @description Allow to search for value. Default true
     */
    @Input() public searchable: boolean;

    /**
     * @description Set custom text when filter returns empty result
     */
    @Input() public notFoundText: string;

    /**
     * @description Whether to close the menu when a value is selected
     */
    @Input() public closeOnSelect: boolean;

    /**
     * @description Allows to hide selected items
     */
    @Input() public hideSelected: boolean;

    /**
     * @description Item(s) selected
     */
    @Input() public selected: any;

    /**
     * @description This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * In the SelectComponent, this EventEmitter allows the sending of an event 'scroll', 'open', 'close", 'search" ...
     */
    @Output() public eventEmitter: EventEmitter<SelectEventInterface>;

    /**
     * @description NgSelectComponent component in the HTML DOM
     */
    @ViewChild(NgSelectComponent, {static: false}) public selectComponent: NgSelectComponent;

    /**
     * @description List of FilterComponent components in the SelectComponent content
     */
    @ContentChildren(FilterComponent, {descendants: true}) filtersComponent !: QueryList<FilterComponent>;

    /**
     * @description Constructor of the SelectComponent
     *
     * The constructor creates an instance of the SelectComponent and specifies the default values
     * of the component's input and output variables.
     */
    constructor(translationService: I18nTranslationService) {
        super(translationService);

        this.items = [];
        this.pageFrom = 0;
        this.pageSize = 20;
        this.loading = false;
        this.inputEmitter = new EventEmitter<FilterInputEmitterClass>();
        this.eventEmitter = new EventEmitter<SelectEventInterface>();
    }

    /**
     * @description The method called during a scroll: it emits an event (to the subscriber component) that provides
     * the start and end indexes of the elements available in the ng-select component.
     *
     * @param data Data received during a scroll in the dropdown area of the select
     */
    onScroll(data: SelectScrollInterface): void {
        this.eventEmitter.next({name: 'scroll', data: data});
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that it has reached the end
     * of the list of the ng-select component
     */
    onScrollToEnd(): void {
        this.eventEmitter.next({name: 'scrollToEnd'});
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that the dropodown of the component
     * ng-select is open
     */
    onOpen(): void {
        this.eventEmitter.next({name: 'open'});
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that the dropodown of the component
     * ng-select is closed
     */
    onClose(): void {
        this.eventEmitter.next({name: 'close'});
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that an element has been
     * selected by the user.
     *
     * To do this, the multiple otpion {@link #multiple} must be set to'true'.
     * The method provides the selected element in the'data' field of the issued object.
     *
     * @param data Data received when adding a selected element in the selection area.
     */
    onAdd(data: any): void {
        this.eventEmitter.next({name: 'add', data: data});
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that an element of the selection
     * has been deleted by the user.
     *
     * To do this, the multiple otpion {@link #multiple} must be set to'true'.
     * The method provides the deleted element in the'data' field of the issued object.
     *
     * @param data Data received when deleting a selected element in the selection area.
     */
    onRemove(data: any): void {
        this.eventEmitter.next({name: 'remove', data: data});
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that the selection has been made
     * deleted by the user after clicking on the delete icon of the ng-select component
     */
    onClear(): void {
        this.eventEmitter.next({name: 'clear'});
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that the component
     * ng-select has the HTML attribute blur.
     *
     * The method provides a FocusEvent element in the'data' field of the issued object.
     *
     * @param data Data received when the focus is lost on the ng-select component.
     */
    onBlur(data: any): void {
        this.eventEmitter.next({name: 'blur', data: data as FocusEvent});
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that the component
     * ng-select has the HTML focus attribute
     *
     * @param data Data received when the focus is present on the ng-select component.
     */
    onFocus(data: any): void {
        this.eventEmitter.next({name: 'focus', data: data as FocusEvent});
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that the selected element
     * has been updated (change in value).
     *
     * The method provides the selected element in the'data' field of the issued object.
     *
     * @param data Data received when the value of the selected elements changes.
     */
    onChange(data: any): void {
        this.eventEmitter.next({name: 'change', data: data});
    }

    /**
     * @description The method sends an event (to the subscriber component) to indicate that the user is performing
     * a search in the ng-select component.
     *
     * The method provides the search term and the list of elements sorted according to the search term.
     *
     * @param data Data received when the user performs a search.
     */
    onSearch(data: SelectSearchInterface): void {
        this.eventEmitter.next({name: 'search', data: data});
    }

    /**
     * @description The method allows you to delete the elements loaded by the ng-select component.
     */
    clearSelect(): void {
        this.items = [];
    }

    /**
     * @description The method allows you to open the drop-down panel of the ng-select component.
     */
    openSelect(): void {
        this.selectComponent.open();
    }

    /**
     * @description The method is used to close the drop-down panel of the ng-select component.
     */
    closeSelect(): void {
        this.selectComponent.close();
    }

    /**
     * @description The method allows you to focus the drop-down panel of the ng-select component.
     */
    focusSelect(): void {
        this.selectComponent.focus();
    }

    /**
     * @description The method allows you to disable the ng-select component and its behaviors.
     *
     * @param value Value that defines whether the ng-select component is enabled or disabled.
     */
    disabledSelect(value: boolean): void {
        this.selectComponent.disabled = value;
    }
}
