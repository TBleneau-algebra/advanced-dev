/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's modules
 */
import {HeaderModule} from './header/header.module';
import {FilterModule} from './filter/filter.module';
import {ViewModule} from './view/view.module';
import {SelectModule} from './select/select.module';
import {ColumnModule} from './column/column.module';
import {PhotoBoxModule} from './photo-box/photo-box.module';
import {PackageModule} from './package/package.module';
import {ActionModule} from './action/action.module';
import {LoadableModule} from './loadable/loadable.module';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        ColumnModule,
        HeaderModule,
        FilterModule,
        ViewModule,
        SelectModule,
        PhotoBoxModule,
        PackageModule,
        ActionModule,
        LoadableModule
    ],
    exports: [
        ColumnModule,
        HeaderModule,
        FilterModule,
        ViewModule,
        SelectModule,
        PhotoBoxModule,
        PackageModule,
        ActionModule,
        LoadableModule
    ]
})

export class ComponentsModule {
}
