/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of Nebular's modules
 */
import {NbLayoutModule, NbSidebarModule} from '@nebular/theme';
/**
 * Import of application's components
 */
import {ColumnComponent} from './column.component';


@NgModule({
    imports: [
        CommonModule,
        NbLayoutModule,
        NbSidebarModule
    ],
    exports: [
        ColumnComponent
    ],
    declarations: [
        ColumnComponent
    ]
})
export class ColumnModule {
}
