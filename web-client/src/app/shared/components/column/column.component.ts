import {Component} from '@angular/core';

@Component({
    selector: 'app-one-column',
    styleUrls: ['./column.component.scss'],
    template: `
        <nb-layout windowMode>
            <nb-layout-header fixed>
                <ng-content select="app-header"></ng-content>
            </nb-layout-header>

            <nb-sidebar class="menu-sidebar" tag="menu-sidebar" responsive>
                <ng-content select="nb-menu"></ng-content>
            </nb-sidebar>

            <nb-layout-column>
                <ng-content select="router-outlet"></ng-content>
            </nb-layout-column>
        </nb-layout>
    `
})
export class ColumnComponent {

    /**
     * @description Constructor of the component ColumnComponent
     *
     * The constructor creates an instance of the component ColumnComponent and specifies the default values
     * of the component's input and output variables.
     */
    constructor() {
    }
}
