import {UserMenuData} from '../data/user-menu.data';
import {AdminMenuData} from '../data/admin-menu.data';
import {NbMenuItem} from '@nebular/theme';
import {GuestMenuData} from '../data/guest-menu.data';

/**
 * @description This function allows you to define which menu should load the application according to the user's rights on the application
 *
 * @param roles User's roles
 */
export function getMenu(roles: string[]): Array<NbMenuItem> {

    if (roles.find(item => item === 'moderator')) {
        return AdminMenuData;
    } else if (roles.find(item => item === 'user')) {
        return UserMenuData;
    }
    return GuestMenuData;
}
