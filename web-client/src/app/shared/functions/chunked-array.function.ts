/**
 * @description This function divides a data table into several data tables of a given size.
 * It is useful for example when you want to display elements responsively with bootstrap and ngFor
 *
 * @param data data table to chunk
 * @param size size of each data table after resizing
 * @param array array in which to put the data
 */
export function createChunkedArray<T>(data: Array<T>, size: number, array?: Array<Array<T>>): Array<Array<T>> {
    const tmp: Array<Array<T>> = [];

    for (let index = 0; index < data.length; index += size) {
        const chunk = data.slice(index, index + size);
        (array) ? array.push(chunk) : tmp.push(chunk);
    }
    return (array) ? array : tmp;
}

/**
 * @description This function returns the number of elements present in the chunked table
 *
 * @param data data chunked array
 */
export function sizeOfChunkedArray<T>(data: Array<Array<T>>): number {
    let x = 0;
    let y = 0;
    let size: number = 0;

    while (x < data.length) {
        y = 0;
        while (y < data[x].length) {
            size += 1;
            y += 1;
        }
        x += 1;
    }
    return size;
}


/**
 * @description This method allows you to delete the elements in an chunked array.
 *
 * @param array data chunked array
 * @param property comparative property
 * @param value comparative value
 * @param size size of each data table after resizing
 */
export function removeElementOfChunkedArray<T>(array: Array<Array<T>>, property: string, value: any, size: number) {
    let x = 0;
    let tmp: Array<T> = [];

    while (x < array.length) {
        array[x] = array[x].filter(elem => elem[property] !== value);
        tmp = tmp.concat(array[x]);
        x += 1;
    }
    return createChunkedArray(tmp, size);
}
