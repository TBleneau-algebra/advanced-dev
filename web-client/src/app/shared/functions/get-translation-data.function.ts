import {data as i18nFr} from '../services/i18n/i18n-data/i18n-fr.data';
import {data as i18nEn} from '../services/i18n/i18n-data/i18n-en.data';

/**
 * @description This method allows you to retrieve the translations of the application for each translated component or layout
 *
 * @param name name of the route for retrieving translations '/translations/fr' | '/translations/en'
 */
export function getTranslationData<T>(name: string): T {
    const sampleDatas: any = [
        {
            route: '/translations/fr',
            data: i18nFr,
        },
        {
            route: '/translations/en',
            data: i18nEn,
        }
    ];

    for (const item of sampleDatas) {
        if (item.route === name) {
            return item.data;
        }
    }
    return null;
}
