import {Pipe, PipeTransform} from '@angular/core';
import moment = require('moment');

@Pipe({
    name: 'datePipe'
})
export class DatePipe implements PipeTransform {

    /**
     * @description Constructor of DatePipe pipe
     *
     * The constructor creates an instance of the DatePipe pipe and specifies the default values
     * the input and output variables of the pipe.
     *
     */
    constructor() {
    }

    /**
     * @description The "transform" method of the PipeTransform interface allows a transformation to be performed.
     *
     * It is used to transform a date into a specific format.
     *
     * @param value Value of the date to be transformed
     * @param format Current date format (before transformation)
     * @param lang Language in which to transform the date
     * @param formatExcepted Format in which to transform the date
     */
    transform(value: string, format: string, lang: string, formatExcepted?: string): string {
        if (value && format) {

            if (format === 'ISO' && moment(value, moment.ISO_8601, true).isValid()) {
                if (formatExcepted) {
                    moment(value, moment.ISO_8601).format(formatExcepted);
                } else {
                    (lang) ? moment.locale(lang) : undefined;
                    return (lang === 'en') ?
                        moment(value, moment.ISO_8601).format('dddd, MMM Do YYYY') :
                        moment(value, moment.ISO_8601).format('dddd Do MMM YYYY');
                }
            } else {
                return moment(value, format).format(formatExcepted);
            }
        }
        return value;
    }
}
