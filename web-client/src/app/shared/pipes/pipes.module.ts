/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

/**
 * Import of application's pipes
 */
import {DatePipe} from './date.pipe';

@NgModule({
    imports: [
        CommonModule,
    ],
    exports: [
        CommonModule,
        DatePipe
    ],
    declarations: [
        DatePipe
    ]
})
export class PipesModule {
}
