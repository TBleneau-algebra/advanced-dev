import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-administration-option',
    templateUrl: './administration-option.component.html',
})
export class AdministrationOptionComponent implements OnInit, OnDestroy {

    /**
     * @description Title of the option
     */
    @Input() public title: string;

    /**
     * @description The option that must be performed when the user clicks on the component
     */
    @Input() public option: string;

    /**
     * @description Image source for the component view
     */
    @Input() public imgSrc: string;

    /**
     * @escription This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * In the AdministrationOptionComponent, this EventEmitter allows to send the option that has just been chosen
     */
    @Output() public optionEmitter: EventEmitter<string>;

    /**
     * @description Constructor of AdministrationOptionComponent component
     *
     * The constructor creates an instance of the AdministrationOptionComponent component and specifies the default values
     * the input and output variables of the component.
     */
    constructor() {
        this.title = '';
        this.optionEmitter = new EventEmitter();
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
    }

    /**
     * @description This method allows to send a status change, an action to the subscriber component
     *
     * @param option The option that has just been chosen
     */
    emit(option: string) {
        this.optionEmitter.emit(option);
    }
}
