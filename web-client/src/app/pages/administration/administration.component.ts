import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {LocalDataSource} from 'ng2-smart-table';
import {SettingsEn, SettingsFr} from '../../shared/data/administration.data';
import {AlertService} from '../../shared/services/alert/alert.service';
import {I18nTranslatableClass} from '../../shared/services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../shared/services/i18n/i18n-translation.service';
import {RequestService} from '../../shared/services/request/request.service';
import {AdministrationModalComponent} from './administration-modal/administration-modal.component';

@Component({
    selector: 'app-administration',
    templateUrl: './administration.component.html',
})
export class AdministrationComponent extends I18nTranslatableClass implements OnInit, OnDestroy {

    /**
     * @description A reference to the newly opened modal returned by the NgbModal.open() method
     */
    private _ngbModalRef: NgbModalRef;

    /**
     * @description Data retrieved via the API
     */
    private _data: any;

    /**
     * @description Configuration parameter of the Ng2SmartTable component
     */
    private _settings: any;

    /**
     * @description Class (object) to be provided to the Ng2SmartTable component to load local data
     */
    private _source: LocalDataSource;

    /**
     * @description Constructor of AdministrationComponent component
     *
     * The constructor creates an instance of the AdministrationComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param ngbModal A service for opening modal windows
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param translationService A reference to the i18n translation service of the application
     * @param requestService A reference to the http request service of the application
     * @param alertService A reference to the alert service of the application
     */
    constructor(private ngbModal: NgbModal, private router: Router, translationService: I18nTranslationService,
                private requestService: RequestService, private alertService: AlertService) {
        super(translationService);
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.initServerDataSource();
        this.settings = (this.translationService.lang === 'en') ? SettingsEn : SettingsFr;

        this.translationService.getTranslationObservable().subscribe(() => {
            this.settings = (this.translationService.lang === 'en') ? SettingsEn : SettingsFr;
        });
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * @description This method is called in the component initialization cycle to retrieve data via the API
     */
    initServerDataSource(): void {
        this._source = new LocalDataSource();

        this.requestService.get('/user', null, null)
            .then((response) => {
                (response.responseStatus !== 200) ? this.errorEvent(response.responseData) : this.successEvent(response.responseData.data);
            })
            .catch((error) => {
                this.errorEvent(error.responseError);
            });
    }

    /**
     * @description A method method that sends the error message received by the API
     *
     * @param data Data with errors messages sent by the API
     */
    errorEvent(data: any) {
        if (data && data.hasOwnProperty('errors')) {
            for (const item of (data.errors as Array<any>)) {
                this.alertService.error(this.translate('ERROR/' + item));
            }
        } else {
            this.alertService.error(this.translate('ERROR/API_CONNECTION'));
        }
    }

    /**
     * @description A method method that sends a success event
     *
     * @param data Response sent by the API
     */
    successEvent(data: Array<any>): void {
        data.forEach(item => {
            item['rolesJoined'] = (item['roles'] as Array<any>).map(elem => elem.name).join(' - ');
        });

        data.forEach(item => item['packageSelected'] = this.translate('PACKAGE/' + item.userPackage.pack.name));

        this._source.load(data).then(() => {
            this.data = data;
        });
    }

    /**
     * @description This method allows to confirm changes before they are applied to the table data source.
     *
     * @param event User click event on delete button
     */
    onDeleteConfirm(event): void {
        this.alertService.confirm(this.translate('ADMINISTRATION/DELETE_QUESTION'),
            this.translate('ADMINISTRATION/YES'), this.translate('ADMINISTRATION/NO'))
            .subscribe(value => {
                if (value) {
                    this.requestService.delete(null, '/user/' + event.data.id)
                        .then((response) => {
                            if (response.responseStatus !== 200) {
                                this.errorEvent(response.responseData);
                                event.confirm.reject();
                            } else {
                                event.confirm.resolve();
                                this.alertService.success(this.translate('SUCCESS/USER_DELETED'));
                            }
                        })
                        .catch((error) => {
                            this.errorEvent(error.responseError);
                            event.confirm.reject();
                        });
                }
            });
    }

    /**
     * @description This method allows to bind a click event performed by the user
     *
     * @param event User click event
     */
    onUserRowSelect(event: any): void {
        this.ngbModalRef = this.ngbModal.open(AdministrationModalComponent, {centered: true});

        this.ngbModalRef.componentInstance.data = event.data;
        this.ngbModalRef.componentInstance.modalEmitter
            .asObservable().subscribe(action => this.onActionSelect(event.data, action));
    }

    /**
     * @description This method allows to edit user's information
     *
     * @param event User click event
     */
    onEditConfirm(event: any): void {
        if (event.data.id === event.newData.id && JSON.stringify(event.data) !== JSON.stringify(event.newData)) {
            this.requestService.put(event.newData, '/user').then((response) => {
                if (response.responseStatus !== 200) {
                    this.errorEvent(response.responseData);
                    this.source.update(event.data, event.data);
                } else {
                    this.alertService.success(this.translate('SUCCESS/USER_UPDATED'));
                }
                event.confirm.resolve();
            })
                .catch((error) => {
                    this.errorEvent(error.responseError);
                    this.source.update(event.data, event.data);
                    event.confirm.resolve();
                });
        } else {
            this.source.update(event.data, event.data);
            event.confirm.resolve();
        }
    }

    /**
     * @description This method allows you to search in the data table
     *
     * @param query search parameter to be applied as a filter
     */
    onSearch(query: string = ''): void {
        this._source.setFilter([
            {
                field: 'id',
                search: query
            },
            {
                field: 'name',
                search: query
            },
            {
                field: 'username',
                search: query
            },
            {
                field: 'email',
                search: query
            }
        ], false);
    }

    /**
     * @description This method allows you to capture an event, an action issued by the modal AdministrationOptionComponent
     *
     * @param data Data of selected user
     * @param value Action performed by the user
     */
    onActionSelect(data: any, value: string): void {
        (this.ngbModalRef) ? this.ngbModalRef.close() : undefined;

        if (value !== 'close') {
            this.router.navigate(['/pages/settings/' + value, {id: data.id}], {
                state: {
                    data: data.name + ' ' + data.lastname
                }
            }).then(() => {
            });
        }
    }

    /**
     * @description The method allows you to retrieve the newly opened modal returned by the NgbModal.open() method
     */
    get ngbModalRef(): NgbModalRef {
        return this._ngbModalRef;
    }

    /**
     * @description The method allows you to assign the newly opened modal returned by the NgbModal.open() method
     *
     * @param value Value of the newly opened modal
     */
    set ngbModalRef(value: NgbModalRef) {
        this._ngbModalRef = value;
    }

    /**
     * @description The method allows you to retrieve the data gotten via the API
     */
    get data(): any {
        return this._data;
    }

    /**
     * @description The method allows you to assign the data gotten via the API
     *
     * @param value Value of the data gotten via the API
     */
    set data(value: any) {
        this._data = value;
    }

    /**
     * @description The method allows you to retrieve the configuration parameter of the Ng2SmartTable component
     */
    get settings(): any {
        return this._settings;
    }

    /**
     * @description The method allows you to assign the configuration parameter of the Ng2SmartTable component
     *
     * @param value Value of the configuration parameter of the Ng2SmartTable component
     */
    set settings(value: any) {
        this._settings = value;
    }

    /**
     * @description The method allows you to retrieve the class (object) to be provided to the Ng2SmartTable component to load local data
     */
    get source(): LocalDataSource {
        return this._source;
    }

    /**
     * @description The method allows you to assign the class (object) to be provided to the Ng2SmartTable component to load local data
     *
     * @param value Value of the class (object) to be provided to the Ng2SmartTable component
     */
    set source(value: LocalDataSource) {
        this._source = value;
    }

}
