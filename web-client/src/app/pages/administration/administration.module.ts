/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
/**
 * Import of ng-bootstrap's modules
 */
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
/**
 * Import of Nebular's modules
 */
import {NbButtonModule, NbCardModule, NbIconModule, NbInputModule} from '@nebular/theme';
/**
 * Import of Ng2SmartTable's module
 */
import {Ng2SmartTableModule} from 'ng2-smart-table';
/**
 * Import of application's components
 */
import {AdministrationComponent} from './administration.component';
import {AdministrationModalComponent} from './administration-modal/administration-modal.component';
import {AdministrationOptionComponent} from './administration-option/administration-option.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        NbCardModule,
        NbInputModule,
        NbButtonModule,
        NbIconModule,
        NgbModule,
        Ng2SmartTableModule
    ],
    declarations: [
        AdministrationComponent,
        AdministrationModalComponent,
        AdministrationOptionComponent
    ],
    exports: [
        AdministrationModalComponent
    ],
    bootstrap: [
        AdministrationModalComponent
    ],
    entryComponents: [AdministrationModalComponent],
    providers: [
        NgbActiveModal
    ]
})
export class AdministrationModule {
}
