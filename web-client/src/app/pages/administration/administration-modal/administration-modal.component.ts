import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {I18nTranslatableClass} from '../../../shared/services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../../shared/services/i18n/i18n-translation.service';
import {AdministrationOptionComponent} from '../administration-option/administration-option.component';

@Component({
    selector: 'app-administration-modal',
    templateUrl: './administration-modal.component.html',
})
export class AdministrationModalComponent extends I18nTranslatableClass implements OnInit, AfterViewInit, OnDestroy {

    /**
     * @description Data passed as a parameter when the AdministrationComponent opens the modal
     */
    @Input() public data: any;

    /**
     * @escription This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * In the AdministrationModalComponent, this EventEmitter allows to send the action that has just been performed
     */
    @Output() public modalEmitter: EventEmitter<string>;

    /**
     * @description A QueryList variable of the components of type AdministrationOptionComponent from the DOM
     * of content.
     *
     * Each time a child item is added, deleted or moved, the list is updated and changes are made
     * of observables in the list issue a new value.
     */
    @ViewChildren(AdministrationOptionComponent) optionComponent !: QueryList<AdministrationOptionComponent>;

    /**
     * @description Constructor of AdministrationModalComponent component
     *
     * The constructor creates an instance of the AdministrationModalComponent component and specifies the default values
     * the input and output variables of the component.
     */
    constructor(translationService: I18nTranslationService) {
        super(translationService);

        this.modalEmitter = new EventEmitter<string>();
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * @dscription: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called after Angular has completely initialized the view of the component.
     * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterViewInit(): void {
        this.optionComponent.forEach(option => option.optionEmitter
            .asObservable()
            .subscribe(action => {
                this.emit(action);
            })
        );
    }

    /**
     * @description This method allows to send a status change, an action to the subscriber component
     *
     * @param action The action that has just been performed
     */
    emit(action: string): void {
        this.modalEmitter.emit(action);
    }
}
