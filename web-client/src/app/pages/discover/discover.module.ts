/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of Nebular's modules
 */
import {NbButtonModule, NbCardModule, NbIconModule, NbInputModule} from '@nebular/theme';
/**
 * Import of application's modules
 */
import {ComponentsModule} from '../../shared/components/components.module';
/**
 * Import of application's components
 */
import {DiscoverComponent} from './discover.component';

@NgModule({
    imports: [
        CommonModule,
        NbCardModule,
        NbButtonModule,
        NbIconModule,
        NbInputModule,
        ComponentsModule,
        ReactiveFormsModule,
        FormsModule
    ],
    declarations: [
        DiscoverComponent
    ]
})
export class DiscoverModule {
}
