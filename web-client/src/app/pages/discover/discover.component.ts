import {AfterViewInit, Component, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {createChunkedArray, sizeOfChunkedArray} from '../../shared/functions/chunked-array.function';
import {I18nTranslatableClass} from '../../shared/services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../shared/services/i18n/i18n-translation.service';
import {RequestParamsInterface} from '../../shared/services/request/params/request-params.interface';
import {RequestParamsListClass} from '../../shared/services/request/params/request-params-list.class';
import {ScrollDispatcher} from '@angular/cdk/overlay';
import {SearchClass} from '../../shared/classes/models/search.class';
import {ViewComponent} from '../../shared/components/view/view.component';
import {dataURItoBlob} from '../../shared/functions/blob-function';
import {RequestService} from '../../shared/services/request/request.service';
import {RequestAuthenticationService} from '../../shared/services/request/request-authentication.service';
import {LoaderSemaphoreClass} from '../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';

@Component({
    selector: 'app-discover',
    templateUrl: './discover.component.html'
})
export class DiscoverComponent extends I18nTranslatableClass implements OnInit, AfterViewInit, OnDestroy {

    /**
     * @description boolean which allows to know if a request is in progress or not
     */
    private _loading: boolean;

    /**
     * @description Total number of element returned by the API
     */
    private _elementTotal: number;

    /**
     * @description Total number of pages returned by the API
     */
    private _pageTotal: number;

    /**
     * @description The page number to be loaded through the API web service
     */
    private _pageFrom: number;

    /**
     * @description Data table divided into several data tables of a given size
     */
    private _photosChunked: Array<Array<any>>;

    /**
     * @description Search bar for filtering the images to be loaded
     */
    private _searchBarParent: HTMLElement;

    /**
     * @description Search term for filtering the images to be loaded
     */
    private _search: SearchClass;

    /**
     * @description ViewComponent component in the HTML DOM
     */
    @ViewChild(ViewComponent, {static: false}) viewComponent: ViewComponent;

    /**
     * @description Constructor of DiscoverComponent component
     *
     * The constructor creates an instance of the DiscoverComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param translationService A reference to the i18n translation service of the application
     * @param requestService A reference to the user service of the application
     * @param scrollDispatcher The cdkScrollable directive and the ScrollDispatcher service together allow components to react
     * to scrolling in any of its ancestor scrolling container
     * @param requestAuthenticationService A reference to the authentication service of the application
     */
    constructor(translationService: I18nTranslationService, private requestService: RequestService,
                private scrollDispatcher: ScrollDispatcher, private requestAuthenticationService: RequestAuthenticationService) {
        super(translationService);

        this.pageFrom = 0;
        this.pageTotal = 0;
        this.elementTotal = 0;
        this.loading = false;
        this.search = new SearchClass();
        this.photosChunked = [];
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.searchBarParent = document.getElementById('discover-fixed');

        this.scrollDispatcher.scrolled().subscribe((event: any) => {
            if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight) {
                this.loadPhotos();
            }
        });

        this.loadPhotos();
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular has finished to initialize the view of a component.
     * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterViewInit(): void {
        this.onResize();

        this.viewComponent.filtersComponent.forEach(filter => {
            filter.filtersEmitter.asObservable().subscribe(emitter => {
                emitter.filter.filters.forEach(item => {
                    this.handleFilters(item);
                });
            });
        });
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * @description Decorator that declares a DOM event to listen for, and provides a handler method to run when that event occurs.
     * This DOM event to listen for is 'resize'
     *
     * @param event DOM event
     */
    @HostListener('window:resize', ['$event'])
    onResize(event?: Event): void {
        if (window.innerWidth < 992) {
            this.searchBarParent.style.position = 'relative';
            this.searchBarParent.style.paddingTop = '30px';
        } else {
            this.searchBarParent.removeAttribute('style');
        }
        this.searchBarParent.style.width = (this.searchBarParent.parentElement.clientWidth - 30) + 'px';
    }

    /**
     * @description This method allows you to interact with filter change events.
     *
     * @param item filter
     */
    handleFilters(item: any): void {
        (item.name === 'tags') ? this.createTag(item) : undefined;
    }

    /**
     * @description This method allows you to create the tags received by when subscribing to the SelectTagComponent
     *
     * @param item emitted values
     */
    createTag(item: any): void {
        this.search.tags = [];

        item.value.forEach(tag => {
            this.search.tags.push(tag.id);
        });

        if (item.value.length === 0 && this.search.prev.tags.length > 0) {
            this.loadPhotos();
        }
    }

    /**
     * @description This method is called when the user clicks on the search button to refresh the page with new images.
     */
    refresh(): void {
        if (this.search.term || this.search.size || this.search.tags.length > 0) {
            this.pageFrom = 0;
            this.pageTotal = 0;
            this.elementTotal = 0;
            this.photosChunked = [];
            this.loadPhotos();
        }
    }

    /**
     * @description This method is called when the search terms change.
     * If this search term becomes null, then the page is reloaded with new images
     *
     * @param event changed data
     * @param property element which changed
     */
    searchChange(property: string, event: any): void {
        if (event === null || event === '' && this.search.prev[property]) {
            this.pageFrom = 0;
            this.pageTotal = 0;
            this.elementTotal = 0;
            this.photosChunked = [];
            this.loadPhotos();
        }
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method Get of the NetworkService service
     */
    loadPhotos(): void {
        let params: RequestParamsInterface;
        const size: number = sizeOfChunkedArray(this.photosChunked);

        if (this.loading || size === this.elementTotal && size !== 0) {
            return;
        }
        this.loading = true;
        this.requestService.activeLoader(new LoaderSemaphoreClass(1));

        this.pageFrom += (this.pageFrom < this.pageTotal) ? 1 : 0;
        params = new RequestParamsListClass(this.pageFrom.toString(), '9');

        this.search.prev.tags = this.search.tags;
        this.search.prev.term = this.search.term;
        this.search.prev.size = this.search.size;

        if (this.search.term) {
            params.params.push({name: 'searchTerm', value: '%' + this.search.term + '%'});
        }
        if (this.search.size) {
            params.params.push({name: 'size', value: this.search.size});
        }

        if (this.search.tags.length > 0) {
            params.params.push({name: 'tag', value: this.search.tags.toString()});
        }

        this.requestService.get('/image', null, params).then(response => {
            if (response.responseStatus === 200) {
                createChunkedArray(response.responseData.data, 3, this.photosChunked);

                this.pageTotal = (response.responseHeaders.has('page.total')) ?
                    parseInt(response.responseHeaders.get('page.total'), 10) : 0;

                this.elementTotal = (response.responseHeaders.has('element.total')) ?
                    parseInt(response.responseHeaders.get('element.total'), 10) : 0;

                this.loading = false;
            }
        }).catch(error => {
            this.loading = false;
        });
    }

    /**
     * @description This method makes it possible to interpolate the events sent by the PhotoBoxComponent component and to carry out
     * the requested action.
     *
     * @param event handled event
     */
    handle(event: any): void {
        if (event.event === 'download') {
            return this.downloadPhoto(event.data);
        }
    }

    /**
     * @description This method allows you to download an image
     *
     * @param data image to download
     */
    downloadPhoto(data: any): void {
        const a = document.createElement('a');

        a.href = window.URL.createObjectURL(dataURItoBlob(data.image));
        a.setAttribute('download', '_image_' + Math.floor(1000 + Math.random() * 9000).toString() + data.format);
        a.click();
    }

    /**
     * @description This method allows you to retrieve the current user of the application
     */
    user(): number {
        if (this.requestAuthenticationService.user) {
            return parseInt(this.requestAuthenticationService.user.id, 10);
        }
        return undefined;
    }

    /**
     * @description The method allows you to retrieve the data table divided into several data tables of a given size
     */
    get photosChunked(): Array<Array<any>> {
        return this._photosChunked;
    }

    /**
     * @description The method allows you to assign the user ID found in the optional parameters of the URL
     *
     * @param value Value of the new data table divided into several data tables of a given size
     */
    set photosChunked(value: Array<Array<any>>) {
        this._photosChunked = value;
    }

    /**
     * @description The method allows you to retrieve the search bar for filtering the images to be loaded
     */
    get searchBarParent(): HTMLElement {
        return this._searchBarParent;
    }

    /**
     * @description The method allows you to assign the search bar for filtering the images to be loaded
     *
     * @param value Value of the new search bar for filtering the images to be loaded
     */
    set searchBarParent(value: HTMLElement) {
        this._searchBarParent = value;
    }

    /**
     * @description The method allows you to retrieve the boolean which allows to know if a request is in progress or not
     */
    get loading(): boolean {
        return this._loading;
    }

    /**
     * @description The method allows you to assign the boolean which allows to know if a request is in progress or not
     *
     * @param value Value of the boolean which allows to know if a request is in progress or not
     */
    set loading(value: boolean) {
        this._loading = value;
    }

    /**
     * @description The method allows you to retrieve the page number to be loaded through the API web service
     */
    get pageFrom(): number {
        return this._pageFrom;
    }

    /**
     * @description The method allows you to assign the page number to be loaded through the API web service
     *
     * @param value Value of the page number to be loaded through the API web service
     */
    set pageFrom(value: number) {
        this._pageFrom = value;
    }

    /**
     * @description The method allows you to retrieve the total number of element returned by the API
     */
    get elementTotal(): number {
        return this._elementTotal;
    }

    /**
     * @description The method allows you to assign the total number of element returned by the API
     *
     * @param value Value of the total number of element returned by the API
     */
    set elementTotal(value: number) {
        this._elementTotal = value;
    }

    /**
     * @description The method allows you to retrieve the total number of element returned by the API
     */
    get pageTotal(): number {
        return this._pageTotal;
    }

    /**
     * @description The method allows you to assign the total number of element returned by the API
     *
     * @param value Value of the total number of element returned by the API
     */
    set pageTotal(value: number) {
        this._pageTotal = value;
    }

    get search(): SearchClass {
        return this._search;
    }

    set search(value: SearchClass) {
        this._search = value;
    }
}
