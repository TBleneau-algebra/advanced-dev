import {Component, OnDestroy} from '@angular/core';
import {I18nTranslatableClass} from '../../shared/services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../shared/services/i18n/i18n-translation.service';

@Component({
    selector: 'app-not-found',
    templateUrl: './not-found.component.html',
})
export class NotFoundComponent extends I18nTranslatableClass implements OnDestroy {

    /**
     * @description Constructor of NotFoundComponent component
     *
     * The constructor creates an instance of the NotFoundComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param translationService A reference to the i18n translation service of the application
     */
    constructor(translationService: I18nTranslationService) {
        super(translationService);
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
}
