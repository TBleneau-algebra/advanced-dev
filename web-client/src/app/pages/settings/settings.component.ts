import {Component, OnDestroy} from '@angular/core';

@Component({
    selector: 'app-settings',
    template: '<router-outlet></router-outlet>',
})
export class SettingsComponent implements OnDestroy {

    /**
     * @description Constructor of SettingsComponent component
     *
     * The constructor creates an instance of the SettingsComponent component and specifies the default values
     * the input and output variables of the component.
     */
    constructor() {
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
    }
}
