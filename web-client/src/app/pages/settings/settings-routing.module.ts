/**
 * Import of Angular's modules
 */
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
/**
 * Import of application's components
 */
import {SettingsComponent} from './settings.component';
import {NotFoundComponent} from '../not-found/not-found.component';
import {SettingProfileComponent} from './setting-profile/setting-profile.component';
import {SettingPhotoComponent} from './setting-photo/setting-photo.component';

const routes: Routes = [{
    path: '',
    component: SettingsComponent,
    children: [
        {
            path: '',
            redirectTo: 'profile',
            pathMatch: 'full',
        },
        {
            path: 'profile',
            component: SettingProfileComponent
        },
        {
            path: 'photos',
            component: SettingPhotoComponent
        },
        {
            path: '**',
            component: NotFoundComponent,
        },
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SettingsRoutingModule {
}
