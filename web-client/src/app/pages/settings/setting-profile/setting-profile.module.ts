/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of application's components
 */
import {SettingProfileComponent} from './setting-profile.component';
/**
 * Import of Nebular's modules
 */
import {NbButtonModule, NbCardModule, NbIconModule, NbInputModule} from '@nebular/theme';
/**
 * Import of application's modules
 */
import {ViewModule} from '../../../shared/components/view/view.module';
import {ComponentsModule} from '../../../shared/components/components.module';


@NgModule({
    imports: [
        CommonModule,
        ComponentsModule,
        FormsModule,
        ReactiveFormsModule,
        NbCardModule,
        NbInputModule,
        NbButtonModule,
        NbIconModule,
        ViewModule
    ],
    declarations: [
        SettingProfileComponent
    ]
})
export class SettingProfileModule {
}
