import {AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RequestAuthenticationService} from '../../../shared/services/request/request-authentication.service';
import {UserClass} from '../../../shared/classes/models/user.class';
import {AlertService} from '../../../shared/services/alert/alert.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ValidatorClass} from '../../../shared/classes/validator/validator.class';
import {PackageComponent} from '../../../shared/components/package/package.component';
import {Subscription} from 'rxjs';
import {NgxImageCompressService} from 'ngx-image-compress';
import {I18nTranslatableClass} from '../../../shared/services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../../shared/services/i18n/i18n-translation.service';
import {LoaderSemaphoreClass} from '../../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';

@Component({
    selector: 'app-setting-profile',
    templateUrl: './setting-profile.component.html',
})
export class SettingProfileComponent extends I18nTranslatableClass implements OnInit, OnDestroy, AfterViewInit {

    /**
     * @description A Subscriber to PackageComponent Event Controllers
     */
    private subscriptionPackage: Subscription;

    /**
     * @description Available offers of the application
     */
    private _packages: Array<any>;

    /**
     * @description User backup
     */
    private _userChanged: boolean;

    /**
     * @description User model used to save information
     */
    private _user: { info: UserClass, passwordConfirmation: string };

    /**
     * @description User ID found in the optional parameters of the URL. Used mainly by an administrator to load profile of a user
     */
    private _urlID: string;

    /**
     * @description Tracks the value and validity state of a group of FormControl instances
     */
    private _formGroup: FormGroup;

    /**
     * @description Tracks the value and validity state of a group of FormControl instances
     */
    private _formPassword: FormGroup;

    /**
     * @description A QueryList variable of the components of type ActionComponent from the DOM
     * of content.
     *
     * Each time a child item is added, deleted or moved, the list is updated and changes are made
     * of observables in the list issue a new value.
     */
    @ViewChildren(PackageComponent) packageComponent !: QueryList<PackageComponent>;

    /**
     * @description Constructor of SettingProfileComponent component
     *
     * The constructor creates an instance of the SettingProfileComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param activatedRoute The route path and parameters are available through an injected router service called the ActivatedRoute
     * @param authenticationService A reference to the authentication service of the application
     * @param alertService A reference to the alert service of the application
     * @param translationService A reference to the i18n translation service of the application
     * @param imageCompress Angular utility for compressing images to a satisfying size
     */
    constructor(private formBuilder: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute,
                private authenticationService: RequestAuthenticationService, private alertService: AlertService,
                private imageCompress: NgxImageCompressService, translationService: I18nTranslationService) {
        super(translationService);
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.user = {info: new UserClass(), passwordConfirmation: null};

        this.formsInit();
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular has finished to initialize the view of a component.
     * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterViewInit(): void {
        this.urlID = this.activatedRoute.snapshot.paramMap.get('id');

        if (this.urlID && this.authenticationService.hasRole('moderator')) {
            this.loadProfile(this.urlID);
        } else {
            this.reloadPath();
        }

        this.subscriptionPackage = this.packageComponent.changes.subscribe(() => {
            this.packageComponent.forEach(item => item.packageEmitter.asObservable().subscribe(emitter => {
                this.changePackage(emitter);
            }));
        });

    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        this.subscriptionPackage.unsubscribe();
    }

    /**
     * @description The method reloads the route when a user ID does not exist or the user does not have the appropriate rights
     * to access the resource.
     */
    reloadPath(): void {
        this.router.navigate(['.', {}], {
            relativeTo: this.activatedRoute,
            queryParams: {id: null},
            queryParamsHandling: 'merge',
        });
        this.loadProfile();
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the NetworkService service
     *
     * @param id user id found in the optional parameters of the URL
     */
    loadProfile(id?: string): void {
        const path: string = (id) ? id : 'me';

        this.authenticationService.get('/user/' + path).then(response => {
            if (response.responseStatus !== 200) {
                this.errorEvent(response.responseData);
                (id) ? this.reloadPath() : undefined;
            } else {
                this.user.info = response.responseData.data;
                this.authenticationService.user = (id) ? this.authenticationService.user :
                    JSON.parse(JSON.stringify(response.responseData.data));

                this.userChanged = JSON.stringify(this.user.info) !== JSON.stringify(this.authenticationService.user);
                this.loadPackages();
            }
        }).catch((error) => {
            this.errorEvent(error.responseError);
            (id) ? this.reloadPath() : undefined;
        });
    }


    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the NetworkService service
     */
    loadPackages(): void {
        this.authenticationService.get('/package').then(response => {
            if (response.responseStatus !== 200) {
                this.errorEvent(response.responseData);
            } else {
                if (response.responseData.data.find(item => item.id === this.user.info.userPackage.pack.id)) {
                    response.responseData.data.find(item => item.id === this.user.info.userPackage.pack.id)['selected'] = true;
                }
                this.packages = response.responseData.data;
            }
        }).catch((error) => {
            this.errorEvent(error.responseError);
        });
    }

    /**
     * @description The method is used to update the user profile
     */
    updateProfile(): void {
        this.authenticationService.activeLoader(new LoaderSemaphoreClass(1));

        this.authenticationService.put(this.user.info, '/user').then((response) => {
            if (response.responseStatus !== 200) {
                this.errorEvent(response.responseData);
            } else {
                this.successEvent();
            }
            this.user.info = response.responseData.data;
            this.authenticationService.user = (this.urlID) ? this.authenticationService.user :
                JSON.parse(JSON.stringify(response.responseData.data));
        })
            .catch((error) => {
                this.errorEvent(error.responseError);

                this.authenticationService.user = (this.urlID) ? this.authenticationService.user :
                    JSON.parse(JSON.stringify(error.responseError.data));
            });
    }

    /**
     * @description The method allows the user's password to be updated
     */
    updatePassword(): void {
        this.authenticationService.activeLoader(new LoaderSemaphoreClass(1));

        this.authenticationService.put(this.user.info, '/user/password').then((response) => {
            if (response.responseStatus !== 200) {
                this.errorEvent(response.responseData);
            } else {
                this.successEvent();
            }
            this.user.info = response.responseData.data;
            this.authenticationService.user = (this.urlID) ? this.authenticationService.user :
                JSON.parse(JSON.stringify(response.responseData.data));

            this.user.passwordConfirmation = null;
        })
            .catch((error) => {
                this.errorEvent(error.responseError);
                
                this.authenticationService.user = (this.urlID) ? this.authenticationService.user :
                    JSON.parse(JSON.stringify(error.responseError.data));
                this.user.passwordConfirmation = null;
            });
    }

    /**
     * @description The method allows the user's package to be updated
     */
    updatePackage(pack: any): void {
        const packages: Array<any> = JSON.parse(JSON.stringify(this.packages));
        const path: string = (this.urlID) ? (this.user.info.id).toString() : 'me';

        this.authenticationService.activeLoader(new LoaderSemaphoreClass(1));
        this.authenticationService.put(pack, '/user/' + path + '/package/' + pack.id).then((response) => {
            if (response.responseStatus !== 200) {
                this.errorEvent(response.responseData);

                packages.forEach(item => item.selected = (item.selected) ? item.selected : false);
                this.packages = packages;
            } else {
                this.successEvent();

                packages.forEach(item => item.selected = (pack.id === item.id));
                this.packages = packages;
            }
        })
            .catch((error) => {
                this.errorEvent(error.responseError);

                packages.forEach(item => item.selected = (item.selected) ? item.selected : false);
                this.packages = packages;
            });
    }

    /**
     * @description This method receives a change event from a PackageComponent and asks the user if they are sure
     * they want to change their offer.
     *
     * @param emitter Emitted event
     */
    changePackage(emitter: any): void {
        const packages: Array<any> = JSON.parse(JSON.stringify(this.packages));

        this.alertService.confirm(this.translate('SETTINGS_PROFILE/PACKAGE_QUESTION'),
            this.translate('SETTINGS_PROFILE/YES'),
            this.translate('SETTINGS_PROFILE/NO')).subscribe(value => {
            if (value) {
                this.updatePackage(this.packages.find(item => item.id === emitter.id));
            } else {
                packages.forEach(item => {
                    item.selected = (item.selected) ? item.selected : false;
                });
                this.packages = packages;
            }
        });
    }

    /**
     * @description The method allows the user's profil picture to be updated
     */
    changePicture() {
        this.imageCompress.uploadFile().then(({image, orientation}) => {
            this.imageCompress.compressFile(image, orientation, 50, 50).then(
                result => {
                    this.user.info.profilePicture = result;
                }
            );
        });
    }

    /**
     * @description The method allows the user's profile picture to be cleared
     */
    clearPicture(): void {
        this.user.info.profilePicture = null;
    }

    /**
     * @description This method verifies (before updating information) that the user information is different to avoid
     * making too many calls to the API.
     */
    changed(): void {
        this.userChanged = JSON.stringify(this.user.info) !== JSON.stringify(this.authenticationService.user);
    }

    /**
     * @description This method returns the value of the focus of an HTML element
     *
     * @param value HTMLElement element
     */
    isFocused(value: HTMLElement): boolean {
        return document.activeElement === value;
    }

    /**
     * @description The method is used to initialize the state of the values of a group of FormControl instances
     */
    private formsInit(): void {
        const regExp = new RegExp('^((?:\\w|[\\-_ ](?![\\-_ ])|[\\u00C0\\u00C1\\u00C2\\u00C3\\u00C4\\u00C5\\u00C6\\u00C7\\u00C8\\' +
            'u00C9\\u00CA\\u00CB\\u00CC\\u00CD\\u00CE\\u00CF\\u00D0\\u00D1\\u00D2\\u00D3\\u00D4\\u00D5\\u00D6\\u00D8\\u00D9\\u00DA\\u00DB' +
            '\\u00DC\\u00DD\\u00DF\\u00E0\\u00E1\\u00E2\\u00E3\\u00E4\\u00E5\\u00E6\\u00E7\\u00E8\\u00E9\\u00EA\\u00EB\\u00EC\\u00ED\\u00' +
            'EE\\u00EF\\u00F0\\u00F1\\u00F2\\u00F3\\u00F4\\u00F5\\u00F6\\u00F9\\u00FA\\u00FB\\u00FC\\u00FD\\u00FF\\u0153])+)$', 'i');

        this.formGroup = this.formBuilder.group(
            {
                name: [null, Validators.compose([
                    Validators.required,
                    Validators.pattern(regExp)
                ])],
                lastname: [null, Validators.compose([
                    Validators.required,
                    Validators.pattern(regExp)
                ])],
                email: [null, Validators.compose([
                    Validators.required,
                    Validators.email
                ])]
            }
        );

        this.formPassword = this.formBuilder.group(
            {
                password: [null, Validators.compose([
                    Validators.required,
                    ValidatorClass.patternValidator(/\d/, {hasNumber: true}),
                    ValidatorClass.patternValidator(/[A-Z]/, {hasCapitalCase: true}),
                    ValidatorClass.patternValidator(/[a-z]/, {hasSmallCase: true}),
                    ValidatorClass.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/, {hasSpecialCharacters: true}),
                    Validators.minLength(8),
                    Validators.maxLength(64)
                ])],
                confirmPassword: [null, Validators.compose([
                    Validators.required])]
            }, {
                validators: [
                    ValidatorClass.passwordMatchValidator
                ]
            }
        );

    }

    /**
     * @description A method method that sends the error message received by the API
     *
     * @param data Data with errors messages sent by the API
     */
    private errorEvent(data: any) {
        if (data && data.hasOwnProperty('errors')) {
            for (const item of (data.errors as Array<any>)) {
                this.alertService.error(this.translate('ERROR/' + item));
            }
        } else {
            this.alertService.error(this.translate('ERROR/API_CONNECTION'));
        }
    }

    /**
     * @description A method method that sends a success event
     */
    private successEvent(): void {
        this.alertService.success(this.translate('SUCCESS/USER_UPDATED'));
    }

    /**
     * @description The method allows you to retrieve the user ID found in the optional parameters of the URL
     */
    get urlID(): string {
        return this._urlID;
    }

    /**
     * @description The method allows you to assign the user ID found in the optional parameters of the URL
     *
     * @param value Value of the user ID found in the optional parameters of the URL
     */
    set urlID(value: string) {
        this._urlID = value;
    }

    /**
     * @description The method allows you to retrieve the user model in the LoginComponent
     */
    get user(): { info: UserClass, passwordConfirmation: string } {
        return this._user;
    }

    /**
     * @description The method allows you to assign the user model in the LoginComponent
     *
     * @param value Value of the user model in the LoginComponent
     */
    set user(value: { info: UserClass, passwordConfirmation: string }) {
        this._user = value;
    }

    /**
     * @description The method allows you to retrieve the variable which tracks the value and validity state of a group of
     * FormControl instances
     */
    get formGroup(): FormGroup {
        return this._formGroup;
    }

    /**
     * @description The method allows you to assign the variable which tracks the value and validity state of a group of
     * FormControl instances
     *
     * @param value Value of the variable which tracks the value and validity state of a group of FormControl instances
     */
    set formGroup(value: FormGroup) {
        this._formGroup = value;
    }

    /**
     * @description The method allows you to retrieve the variable which tracks the value and validity state of a group of
     * FormControl instances
     */
    get formPassword(): FormGroup {
        return this._formPassword;
    }

    /**
     * @description The method allows you to assign the variable which tracks the value and validity state of a group of
     * FormControl instances
     *
     * @param value Value of the variable which tracks the value and validity state of a group of FormControl instances
     */
    set formPassword(value: FormGroup) {
        this._formPassword = value;
    }

    /**
     * @description The method allows you to retrieve the available offers of the application
     */
    get packages(): Array<any> {
        return this._packages;
    }

    /**
     * @description The method allows you to assign the available offers of the application
     *
     * @param value Value of the available offers of the application
     */
    set packages(value: Array<any>) {
        this._packages = value;
    }

    /**
     * @description The method allows you to retrieve the user backup
     */
    get userChanged(): boolean {
        return this._userChanged;
    }

    /**
     * @description The method allows you to assign the user backup
     *
     * @param value Value of the user backup
     */
    set userChanged(value: boolean) {
        this._userChanged = value;
    }
}
