import {ScrollDispatcher} from '@angular/cdk/overlay';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {isNumeric} from 'rxjs/internal-compatibility';
import {dataURItoBlob} from '../../../shared/functions/blob-function';
import {createChunkedArray, removeElementOfChunkedArray, sizeOfChunkedArray} from '../../../shared/functions/chunked-array.function';
import {AlertService} from '../../../shared/services/alert/alert.service';
import {I18nTranslatableClass} from '../../../shared/services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../../shared/services/i18n/i18n-translation.service';
import {LoaderSemaphoreClass} from '../../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';
import {RequestParamsListClass} from '../../../shared/services/request/params/request-params-list.class';
import {RequestParamsInterface} from '../../../shared/services/request/params/request-params.interface';
import {RequestAuthenticationService} from '../../../shared/services/request/request-authentication.service';
import {RequestService} from '../../../shared/services/request/request.service';

@Component({
    selector: 'app-setting-photo',
    templateUrl: './setting-photo.component.html',
})
export class SettingPhotoComponent extends I18nTranslatableClass implements OnInit, OnDestroy {

    /**
     * @description boolean which allows to know if a request is in progress or not
     */
    private _loading: boolean;

    /**
     * @description Total number of element returned by the API
     */
    private _elementTotal: number;

    /**
     * @description Total number of pages returned by the API
     */
    private _pageTotal: number;

    /**
     * @description The page number to be loaded through the API web service
     */
    private _pageFrom: number;

    /**
     * @description User ID found in the optional parameters of the URL. Used mainly by an administrator to load photos of a user
     */
    private _urlID: string;

    /**
     * @description Data table divided into several data tables of a given size
     */
    private _photosChunked: Array<any>;

    /**
     * @description Constructor of SettingPhotoComponent component
     *
     * The constructor creates an instance of the SettingPhotoComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param activatedRoute The route path and parameters are available through an injected router service called the ActivatedRoute
     * @param authenticationService A reference to the authentication service of the application
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param translationService A reference to the i18n translation service of the application
     * @param requestService A reference to the http request service of the application
     * @param alertService A reference to the alert service of the application
     * @param scrollDispatcher The cdkScrollable directive and the ScrollDispatcher service together allow components to react
     * to scrolling in any of its ancestor scrolling container
     */
    constructor(private activatedRoute: ActivatedRoute, private authenticationService: RequestAuthenticationService,
                private router: Router, translationService: I18nTranslationService, private requestService: RequestService,
                private alertService: AlertService, private scrollDispatcher: ScrollDispatcher) {
        super(translationService);

        this.urlID = null;
        this.pageFrom = 0;
        this.pageTotal = 0;
        this.elementTotal = 0;
        this.loading = false;
        this.photosChunked = [];
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.urlID = this.activatedRoute.snapshot.paramMap.get('id');
        this.urlID = (isNumeric(this.urlID)) ? this.urlID : null;

        if (this.urlID && !this.authenticationService.hasRole('moderator')) {
            this.urlID = null;

            this.router.navigate(['.', {}], {
                relativeTo: this.activatedRoute,
                queryParams: {id: null},
                queryParamsHandling: 'merge',
            }).then(() => {
            });
        }

        if (this.urlID && this.authenticationService.hasRole('moderator')) {
            this.loadPhotos(this.urlID);
        } else {
            this.loadPhotos();
        }

        this.scrollDispatcher.scrolled().subscribe((event: any) => {
            if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight) {
                this.loadPhotos();
            }
        });
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method GET of the RequestClass service
     *
     * @param id Optionnal, user id found in the optional parameters of the URL
     */
    loadPhotos(id?: string): void {
        let params: RequestParamsInterface;
        const path: string = (id) ? '/user/' + id + '/image' : '/user/me/image';
        const size: number = sizeOfChunkedArray(this.photosChunked);

        if (this.loading || size === this.elementTotal && size !== 0) {
            return;
        }
        this.loading = true;

        this.pageFrom += (this.pageFrom < this.pageTotal) ? 1 : 0;
        params = new RequestParamsListClass(this.pageFrom.toString(), '12');

        this.requestService.activeLoader(new LoaderSemaphoreClass(1));
        this.requestService.get(path, null, params).then(response => {
            if (response.responseStatus === 200) {
                createChunkedArray(response.responseData.data, 4, this.photosChunked);

                this.pageTotal = (response.responseHeaders.has('page.total')) ?
                    parseInt(response.responseHeaders.get('page.total'), 10) : 0;

                this.elementTotal = (response.responseHeaders.has('element.total')) ?
                    parseInt(response.responseHeaders.get('element.total'), 10) : 0;
            } else {
                this.errorEvent(response.responseData);
            }
            this.loading = false;
        }).catch(error => {
            this.errorEvent(error.responseError);
            this.loading = false;
        });
    }

    /**
     * @description This method allows you to delete an image
     *
     * @param data image to delete
     */
    deletePhoto(data: any): void {
        const id: number = data.id;
        const path: string = (this.urlID) ? '/user/' + this.urlID + '/image/' : '/user/me/image/';

        this.requestService.activeLoader(new LoaderSemaphoreClass(1));
        this.requestService.delete(null, path + data.id, null).then(response => {
            if (response.responseStatus === 200) {
                this.photosChunked = removeElementOfChunkedArray(this.photosChunked, 'id', id, 4);
                this.elementTotal -= 1;
                this.successEvent('PHOTO_DELETED');
            } else {
                this.errorEvent(response.responseData);
            }
        }).catch(error => {
            this.errorEvent(error.responseError);
        });
    }

    /**
     * @description This method allows you to download an image
     *
     * @param data image to download
     */
    downloadPhoto(data: any): void {
        const a = document.createElement('a');

        a.href = window.URL.createObjectURL(dataURItoBlob(data.image));
        a.setAttribute('download', '_image_' + Math.floor(1000 + Math.random() * 9000).toString() + data.format);
        a.click();
    }

    /**
     * @description This method allows you to edit an image
     *
     * @param data image to edit
     */
    editPhoto(data: any): void {
        const path: string = (this.urlID) ? '/user/' + this.urlID + '/image' : '/user/me/image';

        this.requestService.activeLoader(new LoaderSemaphoreClass(1));
        this.requestService.put(data, path, null).then(response => {
            if (response.responseStatus === 200) {
                this.successEvent('PHOTO_UPDATED');
            } else {
                this.errorEvent(response.responseData);
            }
        }).catch(error => {
            this.errorEvent(error.responseError);
        });
    }

    /**
     * @description This method makes it possible to interpolate the events sent by the PhotoBoxComponent component and to carry out
     * the requested action.
     *
     * @param event handled event
     */
    handle(event: any): void {
        switch (event.event) {
            case 'delete':
                return this.deletePhoto(event.data);
            case 'download':
                return this.downloadPhoto(event.data);
            case 'edit':
                return this.editPhoto(event.data);
        }
    }

    /**
     * @description This method allows you to retrieve the current user of the application
     */
    user(): number {
        return parseInt(this.authenticationService.user.id, 10);
    }

    /**
     * @description This method allows you to retrieve the current user managed by the administrator of the application
     */
    id(): number {
        return parseInt(this.urlID, 10);
    }

    /**
     * @description A method method that sends the error message received by the API
     *
     * @param data Data with errors messages sent by the API
     */
    private errorEvent(data: any) {
        if (data && data.hasOwnProperty('errors')) {
            for (const item of (data.errors as Array<any>)) {
                this.alertService.error(this.translate('ERROR/' + item));
            }
        } else {
            this.alertService.error(this.translate('ERROR/API_CONNECTION'));
        }
    }

    /**
     * @description A method method that sends a success event
     */
    private successEvent(message: string): void {
        this.alertService.success(this.translate('SUCCESS/' + message));
    }

    /**
     * @description The method allows you to retrieve the user ID found in the optional parameters of the URL
     */
    get urlID(): string {
        return this._urlID;
    }

    /**
     * @description The method allows you to assign the user ID found in the optional parameters of the URL
     *
     * @param value Value of the user ID found in the optional parameters of the URL
     */
    set urlID(value: string) {
        this._urlID = value;
    }

    /**
     * @description The method allows you to retrieve the data table divided into several data tables of a given size
     */
    get photosChunked(): Array<any> {
        return this._photosChunked;
    }

    /**
     * @description The method allows you to assign the user ID found in the optional parameters of the URL
     *
     * @param value Value of the new data table divided into several data tables of a given size
     */
    set photosChunked(value: Array<any>) {
        this._photosChunked = value;
    }

    /**
     * @description The method allows you to retrieve the boolean which allows to know if a request is in progress or not
     */
    get loading(): boolean {
        return this._loading;
    }

    /**
     * @description The method allows you to assign the boolean which allows to know if a request is in progress or not
     *
     * @param value Value of the boolean which allows to know if a request is in progress or not
     */
    set loading(value: boolean) {
        this._loading = value;
    }

    /**
     * @description The method allows you to retrieve the page number to be loaded through the API web service
     */
    get pageFrom(): number {
        return this._pageFrom;
    }

    /**
     * @description The method allows you to assign the page number to be loaded through the API web service
     *
     * @param value Value of the page number to be loaded through the API web service
     */
    set pageFrom(value: number) {
        this._pageFrom = value;
    }

    /**
     * @description The method allows you to retrieve the total number of element returned by the API
     */
    get elementTotal(): number {
        return this._elementTotal;
    }

    /**
     * @description The method allows you to assign the total number of element returned by the API
     *
     * @param value Value of the total number of element returned by the API
     */
    set elementTotal(value: number) {
        this._elementTotal = value;
    }

    /**
     * @description The method allows you to retrieve the total number of element returned by the API
     */
    get pageTotal(): number {
        return this._pageTotal;
    }

    /**
     * @description The method allows you to assign the total number of element returned by the API
     *
     * @param value Value of the total number of element returned by the API
     */
    set pageTotal(value: number) {
        this._pageTotal = value;
    }
}
