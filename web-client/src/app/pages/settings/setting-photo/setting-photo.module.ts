/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of Nebular's modules
 */
import {NbCardModule} from '@nebular/theme';
/**
 * Import of application's modules
 */
import {ViewModule} from '../../../shared/components/view/view.module';
import {ComponentsModule} from '../../../shared/components/components.module';
/**
 * Import of application's components
 */
import {SettingPhotoComponent} from './setting-photo.component';


@NgModule({
    imports: [
        CommonModule,
        NbCardModule,
        ViewModule,
        ComponentsModule
    ],
    declarations: [
        SettingPhotoComponent
    ]
})
export class SettingPhotoModule {
}
