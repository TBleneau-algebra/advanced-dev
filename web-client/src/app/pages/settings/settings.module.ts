/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
/**
 * Import of application's modules
 */
import {SettingsRoutingModule} from './settings-routing.module';
import {SettingProfileModule} from './setting-profile/setting-profile.module';
import {SettingPhotoModule} from './setting-photo/setting-photo.module';
/**
 * Import of application's components
 */
import {SettingsComponent} from './settings.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        SettingsRoutingModule,
        SettingProfileModule,
        SettingPhotoModule
    ],
    declarations: [
        SettingsComponent
    ]
})
export class SettingsModule {
}
