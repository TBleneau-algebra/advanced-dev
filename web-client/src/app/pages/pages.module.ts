/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';

/**
 * Import of application's modules
 */
import {ThemeModule} from '../shared/theme/theme.module';
import {PagesRoutingModule} from './pages-routing.module';
import {NotFoundModule} from './not-found/not-found.module';
import {DiscoverModule} from './discover/discover.module';
import {AdministrationModule} from './administration/administration.module';
import {SettingsModule} from './settings/settings.module';
import {UploadPhotoModule} from './upload-photo/upload-photo.module';
import {ComponentsModule} from '../shared/components/components.module';

/**
 * Import of application's components
 */
import {PagesComponent} from './pages.component';


@NgModule({
    imports: [
        PagesRoutingModule,
        ThemeModule,
        ComponentsModule,
        NotFoundModule,
        DiscoverModule,
        AdministrationModule,
        UploadPhotoModule,
        SettingsModule
    ],
    declarations: [
        PagesComponent,
    ],
})
export class PagesModule {
}
