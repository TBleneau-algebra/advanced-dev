import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {NbMenuItem} from '@nebular/theme';
import {getMenu} from '../shared/functions/menu.function';
import {I18nTranslatableClass} from '../shared/services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../shared/services/i18n/i18n-translation.service';
import {LoaderService} from '../shared/services/loader-spinner/loader.service';
import {RequestAuthenticationService} from '../shared/services/request/request-authentication.service';

@Component({
    selector: 'app-pages',
    template: `
        <app-one-column>
            <app-header [title]="translate('APP/TITLE')"></app-header>
            <nb-menu [items]="menuItems" [autoCollapse]="false">
            </nb-menu>
            <router-outlet></router-outlet>
        </app-one-column>`
})
export class PagesComponent extends I18nTranslatableClass implements OnInit, OnDestroy, AfterViewInit {

    /**
     * @description Items in the component menu NbMenuComponent
     *
     * A map is used to ensure the uniqueness of the items by title.
     * The elements are sorted in {@link menuItems}.
     */
    private _menuItems: Map<string, NbMenuItem> = new Map<string, NbMenuItem>();

    /**
     * @description Constructor of PagesComponent component
     *
     * The constructor creates an instance of the PagesComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param authenticationService A reference to the authentication service of the application
     * @param translationService A reference to the i18n translation service of the application
     * @param loaderService A reference to the loader service of the application
     */
    constructor(private authenticationService: RequestAuthenticationService, translationService: I18nTranslationService,
                private loaderService: LoaderService) {
        super(translationService);
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.loadMenu(this.authenticationService.roles);
        this.loadProfile();
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular has finished to initialize the view of a component.
     * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterViewInit(): void {
        this.translationService.getTranslationObservable().subscribe(() => {
            this.translateMenu(this.menuItems);
        });
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * @description The method initializes the application by retrieving the user's information
     */
    loadProfile(): void {
        if (!this.authenticationService.hasRole('guest')) {
            const id: string = (this.authenticationService.user) ? this.authenticationService.user.id : 'me';

            this.authenticationService.get('/user/' + id).then(response => {
                this.authenticationService.user = response.responseData.data;
                this.authenticationService.setRoles(response.responseData.data.roles);

                this.loadMenu(this.authenticationService.roles);

            }).catch(() => {
                this.authenticationService.setRoles();
                this.loadMenu(['guest']);
            });
        }
    }

    /**
     * @description The method initializes the application's menu with the user's roles
     */
    loadMenu(roles: Array<string>): void {
        this.menuItems = this.translateMenu(getMenu(roles));
    }

    /**
     * @description This method allows the menu to be translated according to the language chosen by the user.
     *
     * @param menu Menu to translate
     */
    translateMenu(menu: Array<NbMenuItem>): Array<NbMenuItem> {
        menu.forEach(item => {
            if (item.children && item.children.length > 0) {
                this.translateMenu(item.children);
            }
            item.title = this.translate(item.data);
        });
        return menu;
    }

    /**
     * @description The method allows you to retrieve the sorted items from the NbMenuComponent menu.
     */
    get menuItems(): Array<NbMenuItem> {
        return Array.from(this._menuItems.values());
    }

    /**
     * @description The method is used to assign the menu items of the NbMenuComponent
     * Empty the item map and fill it with the input table using the item title as the key.
     *
     * @param value Value of the NbMenuComponent menu items
     */
    set menuItems(value: Array<NbMenuItem>) {
        this._menuItems.clear();
        value.forEach(item => this._menuItems.set(item.title, item));
    }
}
