/**
 * Import of Angular's modules
 */
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

/**
 * Import of application's components
 */
import {PagesComponent} from './pages.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {DiscoverComponent} from './discover/discover.component';
import {AdministrationComponent} from './administration/administration.component';
import {UploadPhotoComponent} from './upload-photo/upload-photo.component';
import {UserGuardService} from '../shared/services/guards/user-guard.service';
import {AdminGuardService} from '../shared/services/guards/admin-guard.service';

const routes: Routes = [{
    path: '',
    component: PagesComponent,
    children: [
        {
            path: '',
            redirectTo: 'discover',
            pathMatch: 'full',
        },
        {
            path: 'discover',
            component: DiscoverComponent
        },
        {
            path: 'upload',
            canActivate: [UserGuardService],
            component: UploadPhotoComponent
        },
        {
            path: 'settings',
            canActivate: [UserGuardService],
            loadChildren: () => import('./settings/settings.module')
                .then(m => m.SettingsModule),
        },
        {
            path: 'administration',
            canActivate: [AdminGuardService],
            component: AdministrationComponent
        },
        {
            path: '**',
            component: NotFoundComponent,
        },
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PagesRoutingModule {
}
