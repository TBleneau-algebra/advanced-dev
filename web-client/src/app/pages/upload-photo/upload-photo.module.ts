/**
 * Import of Angular's modules
 */
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of application's modules
 */
import {ComponentsModule} from '../../shared/components/components.module';
/**
 * Import of application's components
 */
import {UploadPhotoComponent} from './upload-photo.component';
/**
 * Import of Nebular's modules
 */
import {NbButtonModule, NbCardModule, NbCheckboxModule, NbIconModule, NbInputModule} from '@nebular/theme';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NbCardModule,
        NbIconModule,
        NbInputModule,
        NbCheckboxModule,
        ComponentsModule,
        NbButtonModule
    ],
    declarations: [
        UploadPhotoComponent,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UploadPhotoModule {
}
