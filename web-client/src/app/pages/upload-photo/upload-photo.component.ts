import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgxImageCompressService} from 'ngx-image-compress';
import {PictureClass} from '../../shared/classes/models/picture.class';
import {ViewComponent} from '../../shared/components/view/view.component';
import {AlertService} from '../../shared/services/alert/alert.service';
import {I18nTranslatableClass} from '../../shared/services/i18n/i18n-class/i18n-translatable.class';
import {I18nTranslationService} from '../../shared/services/i18n/i18n-translation.service';
import {ImageFilterService} from '../../shared/services/image-filter/image-filter.service';
import {LoaderSemaphoreClass} from '../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';
import {RequestService} from '../../shared/services/request/request.service';

declare var editor: any;

@Component({
    selector: 'app-upload-photo',
    templateUrl: './upload-photo.component.html',
})
export class UploadPhotoComponent extends I18nTranslatableClass implements OnInit, AfterViewInit, OnDestroy {

    /**
     * @description Preview of the image loaded by the user
     */
    private _preview: any;

    /**
     * @description HTML canvas element
     */
    private _canvas: HTMLCanvasElement;

    /**
     * @description model to send to the API
     */
    private _model: PictureClass;

    /**
     * @description boolean allows to keep the proportions of the image
     */
    private _ratio: boolean;

    /**
     * @description image coordinates
     */
    private _coordinates: { width: number, height: number };

    /**
     * @description ViewComponent component in the HTML DOM
     */
    @ViewChild(ViewComponent, {static: false}) viewComponent: ViewComponent;

    /**
     * @description Constructor of UploadPhotoComponent component
     *
     * The constructor creates an instance of the UploadPhotoComponent component and specifies the default values of
     * the input and output variables of the component.
     *
     * @param imageCompress Angular utility for compressing images to a satisfying size
     * @param translationService A reference to the i18n translation service of the application
     * @param imageFilterService A reference to the image filter service of the application
     * @param alertService A reference to the alert service of the application
     * @param requestService A reference to the user service of the application
     */
    constructor(private imageCompress: NgxImageCompressService, translationService: I18nTranslationService,
                private imageFilterService: ImageFilterService, private alertService: AlertService,
                private requestService: RequestService) {
        super(translationService);
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.init();
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular has finished to initialize the view of a component.
     * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterViewInit(): void {
        this.canvas = document.getElementById('canvas') as HTMLCanvasElement;

        this.viewComponent.filtersComponent.forEach(filter => {
            filter.filtersEmitter.asObservable().subscribe(emitter => {
                emitter.filter.filters.forEach(item => {
                    this.handleFilters(item);
                });
            });
        });
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * @description This method initializes the variables of the component
     */
    init(): void {
        this.ratio = true;
        this.coordinates = {width: 0, height: 0};

        this.model = new PictureClass();
        this.model.image = new editor.marvinImage();
        this.model.imageBackup = new editor.marvinImage();
        this.model.imageFilter = new editor.marvinImage();
    }

    /**
     * @description This method allows you to interact with filter change events.
     *
     * @param item filter
     */
    handleFilters(item: any): void {
        switch (item.name) {
            case 'filter':
                this.model.filter = (item.value.id === -1) ? null : item.value;
                this.applyOrDraw(item.value.id);
                break;
            case 'tags':
                this.model.tags = [...item.value];
                break;
            case 'format':
                this.model.format = (item.value) ? item.value.value.split('/')[1] : 'png';
                break;
        }
    }

    /**
     * @description This method allows you to apply a filter or remove a filter
     *
     * @param id filter id
     */
    applyOrDraw(id: number): void {
        if (id === -1) {
            this.preview = this.imageFilterService.draw(id, this.model.image, this.canvas);
        } else {
            this.model.imageFilter = this.imageFilterService.apply(id, editor, this.model.imageBackup, this.canvas);
            this.preview = this.imageFilterService.draw(id, this.model.imageFilter, this.canvas);
        }
        this.imageFilterService.current = id;
    }

    /**
     * @description The method allows the user to load an image
     */
    change() {
        this.imageCompress.uploadFile().then(({image, orientation}) => {
            this.load(image);
        });
    }

    /**
     * @description This method loads a user-selected image and displays it in a preview variable.
     *
     * @param result image loading result
     */
    load(result: any) {
        (this.model.imageBackup.imageData) ? this.clear() : undefined;
        this.model.image.load(result, () => {
        });

        this.model.imageBackup.load(result, () => {

            this.coordinates = {width: this.model.imageBackup.width, height: this.model.imageBackup.height};

            this.canvas.height = this.model.imageBackup.height;
            this.canvas.width = this.model.imageBackup.width;

            this.applyOrDraw(this.imageFilterService.current);
        });
    }

    /**
     * @description This method allows you to resize the image
     *
     * @param value new dimension
     * @param property Property to be resized
     */
    resize(value: number, property: string): void {
        if (this.ratio) {
            if (value && this.model.image.hasOwnProperty('imageData')) {
                if (property === 'width') {
                    this.coordinates.height = (this.model.image.imageData.width === 0) ? value :
                        Math.round((this.model.image.imageData.height * value) / this.model.image.imageData.width);
                } else {
                    this.coordinates.width = (this.model.image.imageData.height === 0) ? value :
                        Math.round((this.model.image.imageData.width * value) / this.model.image.imageData.height);
                }
            } else {
                this.coordinates = {width: 0, height: 0};
            }
        }
    }

    /**
     * @description The method allows the preview of the image loaded by the user to be cleared
     */
    clear(): void {
        this.preview = null;
        this.canvas.getContext('2d').clearRect(0, 0, this.canvas.width, this.canvas.height);

        delete this.model;
        delete this.coordinates;
        this.init();
    }

    /**
     * @description This method saves the image that the user wants to upload
     */
    save(): void {
        this.alertService.confirm(this.translate('UPLOAD_PHOTO/SAVE_QUESTION'),
            this.translate('UPLOAD_PHOTO/YES'), this.translate('UPLOAD_PHOTO/NO'))
            .subscribe(value => {
                if (value) {
                    this.model.width = this.coordinates.width;
                    this.model.height = this.coordinates.height;
                    this.model.process(editor);

                    if (this.model.filter) {
                        this.model.imageFilter = this.imageFilterService.apply(this.model.filter.id, editor, this.model.image,
                            this.canvas);
                    } else {
                        delete this.model.imageFilter;
                    }
                    delete this.model.imageBackup;

                    this.request();
                }
            });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method post of the NetworkService service
     */
    request(): void {
        this.requestService.activeLoader(new LoaderSemaphoreClass(1));
        this.imageFilterService.serialize(this.model, 'image', this.canvas);
        this.imageFilterService.serialize(this.model, 'imageFilter', this.canvas);

        this.requestService.post(this.model, '/user/me/image', null).then(response => {
            (response.responseStatus === 200) ? this.successEvent() : this.errorEvent(response.responseData);
            this.clear();
            this.init();
        }).catch(error => {
            this.errorEvent(error.responseError);
            this.clear();
            this.init();
        });

    }

    /**
     * @description A method method that sends the error message received by the API
     *
     * @param data Data with errors messages sent by the API
     */
    errorEvent(data: any) {
        if (data.hasOwnProperty('errors')) {
            for (const item of (data.errors as Array<any>)) {
                this.alertService.error(this.translate('ERROR/' + item));
            }
        } else {
            this.alertService.error(this.translate('ERROR/API_CONNECTION'));
        }
    }

    /**
     * @description A method method that sends a success event
     */
    successEvent(): void {
        this.alertService.success(this.translate('SUCCESS/PHOTO_UPLOADED'));
    }

    /**
     * @description The method allows you to retrieve the HTML canvas element
     */
    get canvas(): HTMLCanvasElement {
        return this._canvas;
    }

    /**
     * @description The method allows you to assign the preview of the HTML canvas element
     *
     * @param value Value of the new HTML canvas element
     */
    set canvas(value: HTMLCanvasElement) {
        this._canvas = value;
    }

    /**
     * @description The method allows you to retrieve the preview of the image loaded by the user
     */
    get preview(): any {
        return this._preview;
    }

    /**
     * @description The method allows you to assign the preview of the image loaded by the user
     *
     * @param value Value of the new preview of the image loaded by the user
     */
    set preview(value: any) {
        this._preview = value;
    }

    /**
     * @description The method allows you to retrieve the model to send to the API
     */
    get model(): PictureClass {
        return this._model;
    }

    /**
     * @description The method allows you to assign the model to send to the API
     *
     * @param value Value of the new model to send to the API
     */
    set model(value: PictureClass) {
        this._model = value;
    }

    /**
     * @description The method allows you to retrieve the boolean allows to keep the proportions of the image
     */
    get ratio(): boolean {
        return this._ratio;
    }

    /**
     * @description The method allows you to assign the boolean allows to keep the proportions of the image
     *
     * @param value Value of the boolean allows to keep the proportions of the image
     */
    set ratio(value: boolean) {
        this._ratio = value;
    }

    /**
     * @description The method allows you to retrieve the image coordinates
     */
    get coordinates(): { width: number; height: number } {
        return this._coordinates;
    }

    /**
     * @description The method allows you to assign the image coordinates
     *
     * @param value Value of the new image coordinates
     */
    set coordinates(value: { width: number; height: number }) {
        this._coordinates = value;
    }
}
