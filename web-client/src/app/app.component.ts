import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-root',
    template: `
        <app-loadable loaderId="GLOBAL" [initialState]="false" [fixed]="true">
            <app-loadable-spinner></app-loadable-spinner>
            <app-loadable-content>
                <router-outlet></router-outlet>
            </app-loadable-content>
        </app-loadable>`
})
export class AppComponent implements OnInit {

    /**
     * @description Constructor of AppComponent component
     *
     * The constructor creates an instance of the AppComponent component and specifies the default values of
     * the input and output variables of the component.
     */
    constructor() {
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
    }
}
