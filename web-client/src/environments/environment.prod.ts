export const environment = {
    production: true,
    development: false,

    serverApiBase: 'http://localhost:7070/api/v1',
};
