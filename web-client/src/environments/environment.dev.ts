export const environment = {
    production: false,
    development: true,

    serverApiBase: 'http://localhost:7171/api/v1',
};
