# Advanced Application Development Based on Development Templates Project

### Application WEB (Client / User Interface) project

If you would like more information about the web application, follow this link :
[README.md](./web-client/README.md)

### API project

If you would like more information about the API, follow this link :
[README.md](./web-api/README.md)

### MySQL Database information

If you would like more information about the MySQL database, follow this link :
[README.md](./sql/README.md)

### Grafana configuration

If you would like more information about the Grafana configuration, follow this link :
[README.md](./grafana/README.md)

### Prometheus configuration

If you would like more information about the Prometheus configuration, follow this link :
[README.md](./prometheus/README.md)

### Application deployment using Docker

In order to deploy the project in Docker containers, simply follow some steps listed below. 

If, unfortunately, you must have "administrator" rights to perform these commands, I invite you to add your user to the docker group of your machine.

```sh
      sudo usermod -a -G docker <USER_NAME>
```

The first thing you will need to do is to create the network in which your containers will communicate. 
In this project, the network used is called <b>aadbdt-network</b>

```sh
      docker create network aadbdt-network
```

1) Afterwards the database will have to be set up. To do this, simply follow the instructions in the [sql](./sql/README.md) folder.

2) Once the database is mounted, you just need to build the application. To do this, you will first need to build the sources of each application and create the docker images.  
   To build your docker containers, execute the following command :

    ```sh
          docker-compose -f docker-compose.dev.yml build
    ```
   or 
    ```sh
          docker-compose -f docker-compose.prod.yml build
    ```

3) Once construction has been completed, you can launch your docker containers

     ```sh
           docker-compose -f docker-compose.dev.yml up
     ```
    or 
     ```sh
           docker-compose -f docker-compose.prod.yml up
     ```

4) To check that the docker containers are launched correctly, you can run the following command:

    ```sh
          docker ps
    ```

5) To check the running containers, you can run the following command:

    ```sh
          docker-compose -f docker-compose.dev.yml stop
    ```
   or 
    ```sh
          docker-compose -f docker-compose.prod.yml stop
    ```

6) To delete the stopped containers, you can run the following command:

    ```sh
          docker container prune
    ```

7) To delete the dangling images, you can run the following command:

    ```sh
          docker image prune
    ```

8) To delete a specific image, simply list the images and delete the image using its name or identifier:

    ```sh
          docker image ls
          docker rmi <IMAGE_NAME / IMAGE_ID>
    ```

