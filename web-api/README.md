# API

## Install Java SDK 8

Requirement : The first step to build or run this project is to install [Java JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html). 

## Build the API's JAR file

JAR files are packaged with the ZIP file format, so you can use them for tasks such as lossless data compression, archiving, decompression, and archive unpacking.

```sh
      ./gradlew clean && ./gradlew build --refresh-dependencies --no-build-cache
```

If you need to skip units tests, you should run the following command :  

```sh
      ./gradlew clean && ./gradlew build -x test --refresh-dependencies --no-build-cache
```

## Git commit format

The first line of the commit message must contains the tags depending on the commit's feature.
The tags hierarchy must be ascendant, that means from the most general to the most precise.

Example 1 :

```sh 
      git commit -m "[WEB_API][CONTROLLER][REST][LOGIN] - Creation of the authentication rest controller"
```

Example 2 : 

```sh
      git commit -m "[WEB_API][REPOSITORY][JPA][USER] - Implementation and creation of UserRepository which extends JPARepository properties"
```

## Code comment format

To comment the code in Java, we use the official Javadoc.
Javadoc uses a tag system that you can find at these addresses : 

- [Javadoc Tags](https://www.tutorialspoint.com/java/java_documentation.htm)

For example :

```sh 
      /**
       * <h1>Hello, World!</h1>
       * The HelloWorld program implements an application that
       * simply displays "Hello World!" to the standard output.
       * <p>
       * Giving proper comments in your program makes it more
       * user friendly and it is assumed as a high quality code.
       * 
       *
       * @author  xxxxxx yyyyyy
       * @version 1.0
       * @since   2014-03-31 
       */
      public class HelloWorld {
      }
```
```sh 
      /**
       * This method is used to add two integers. This is
       * a the simplest form of a class method, just to
       * show the usage of various javadoc Tags.
       *
       * @param numA This is the first paramter to addNum method
       * @param numB  This is the second parameter to addNum method
       * @return int This returns sum of numA and numB.
       */
      public int addNum(int numA, int numB) {
         return numA + numB;
      }
```

All of your code must be commented.
If the code is not commented correctly, the merge request and your task will not be validated.

## Merge request on the dev branch

If you want to merge a branch into dev branch, first make sure you push all your commits on the branch itself. 
Next, run the following command : 

```sh
      git push origin dev
```

This command will prohibit you to push and display a link to make a merge request on your GitLab account.
Follow this link and complete the merge request. This will notify the maintainers of the repository to evaluate your merge request.
He will then accept or refuse your merge request.

## Docker deployment 

Requirement : In the [environment](./config/environment) folder, create files <b>aadbdt-dev.env</b> and <b>aadbdt-prod.env</b> to specify environment variables for
the docker containers and specify the following variables :

See [sql/README.md](../sql/README.md)  
Note that the authentication information to the MySQL server must be the same for the API to connect to it.

```sh
      MYSQL_HOST=localhost              # Corresponds to the MySQL host (localhost is the default value)
      MYSQL_PORT=3306                   # Corresponds to the MySQL host port (3306 is the default value)
      MYSQL_DATABASE=aadbdt_db_dev      # Corresponds to the MySQL database for the development project (default value)
      MYSQL_USER=xxxxxxxxx              # Corresponds to the MySQL user who has access to the declared database 
      MYSQL_PASSWORD=xxxxxxxxx          # Corresponds to the MySQL user password
      SERVER_PORT=xxxx                  # Tomcat server port
      JWT_SIGNING_KEY=SMeKKF2QT4fwpM    # That's an exemple, not a true value
```
