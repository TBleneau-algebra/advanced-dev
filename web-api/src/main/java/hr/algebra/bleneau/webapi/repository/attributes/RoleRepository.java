package hr.algebra.bleneau.webapi.repository.attributes;


import hr.algebra.bleneau.webapi.entity.attributes.role.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
    RoleEntity findByName(String name);

    boolean existsByName(String name);
}
