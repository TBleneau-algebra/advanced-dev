package hr.algebra.bleneau.webapi.utils;

import hr.algebra.bleneau.webapi.constant.TokenConstant;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SecurityJWTokenAuthorizationFilter extends BasicAuthenticationFilter {

    SecurityJWTokenAuthorizationFilter(AuthenticationManager authManager) {
        super(authManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {

        if (SecurityCookieService.getValue(req, TokenConstant.TOKEN_NAME) == null) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        if (SecurityCookieService.getValue(request, TokenConstant.TOKEN_NAME) != null) {

            String user = SecurityJWTokenService.getSubject(request);

            if (user != null) {
                return new UsernamePasswordAuthenticationToken(
                        SecurityJWTokenService.getSubject(request),
                        null,
                        SecurityJWTokenService.getRoles(request));
            }
        }
        return null;
    }
}
