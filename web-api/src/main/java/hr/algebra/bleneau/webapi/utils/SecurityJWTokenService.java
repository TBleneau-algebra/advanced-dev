package hr.algebra.bleneau.webapi.utils;

import com.auth0.jwt.JWT;
import hr.algebra.bleneau.webapi.constant.TokenConstant;
import hr.algebra.bleneau.webapi.entity.attributes.role.RoleEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.core.env.Environment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

@Service
public class SecurityJWTokenService implements Serializable {

    private static Environment environment;

    private static String JWTokenSigningKey;

    public SecurityJWTokenService(Environment environment) {
        SecurityJWTokenService.environment = environment;
        SecurityJWTokenService.JWTokenSigningKey = SecurityJWTokenService.environment.getProperty("JWT_SIGNING_KEY");
    }

    public static String createToken(UserEntity userEntity) {
        Set<String> authorities = new HashSet<>();

        for (RoleEntity roleEntity : userEntity.getRoles()) {
            authorities.add(roleEntity.getName());
        }

        if (userEntity.getUsername() != null)
            return JWT.create()
                    .withSubject(userEntity.getUsername())
                    .withClaim(TokenConstant.TOKEN_ID_KEY, userEntity.getId())
                    .withClaim(TokenConstant.TOKEN_AUTHORITIES_KEY, String.join(",", authorities))
                    .withExpiresAt(new Date(System.currentTimeMillis() + (TokenConstant.TOKEN_EXPIRATION_TIME * 1000)))
                    .sign(HMAC512(SecurityJWTokenService.JWTokenSigningKey.getBytes()));
        return null;
    }

    public static String getSubject(HttpServletRequest httpServletRequest) {
        String token = SecurityCookieService.getValue(httpServletRequest, TokenConstant.TOKEN_NAME);

        if (token == null)
            return null;

        return JWT.require(HMAC512(SecurityJWTokenService.JWTokenSigningKey.getBytes())).build().verify(token).getSubject();
    }

    public static String getId(HttpServletRequest httpServletRequest) {
        String token = SecurityCookieService.getValue(httpServletRequest, TokenConstant.TOKEN_NAME);

        if (token == null)
            return null;

        Claims claims = Jwts.parser()
                .setSigningKey(TokenConstant.TOKEN_NAME.getBytes(StandardCharsets.UTF_8)).parseClaimsJws(token).getBody();

        return SecurityJWTokenService.parseClaim(claims, TokenConstant.TOKEN_ID_KEY);
    }

    public static List<GrantedAuthority> getRoles(HttpServletRequest httpServletRequest) {
        String token = SecurityCookieService.getValue(httpServletRequest, TokenConstant.TOKEN_NAME);

        if (token == null)
            return null;

        Claims claims = Jwts.parser()
                .setSigningKey(SecurityJWTokenService.JWTokenSigningKey.getBytes(StandardCharsets.UTF_8)).parseClaimsJws(token).getBody();

        return AuthorityUtils.commaSeparatedStringToAuthorityList(SecurityJWTokenService.parseClaim(claims,
                TokenConstant.TOKEN_AUTHORITIES_KEY));
    }

    public static Boolean isTokenValidated(HttpServletRequest httpServletRequest) {
        String token = SecurityCookieService.getValue(httpServletRequest, TokenConstant.TOKEN_NAME);

        if (token == null)
            return false;

        Claims claims = Jwts.parser()
                .setSigningKey(SecurityJWTokenService.JWTokenSigningKey.getBytes(StandardCharsets.UTF_8)).parseClaimsJws(token).getBody();

        return !claims.getExpiration().before(new Date());
    }


    private static String parseClaim(Claims claims, String key) {
        for (Map.Entry<String, Object> entry : claims.entrySet())
            if (entry.getKey().equals(key))
                return entry.getValue().toString();
        return null;
    }
}
