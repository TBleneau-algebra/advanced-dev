package hr.algebra.bleneau.webapi.aspects.user;

import hr.algebra.bleneau.webapi.entity.response.ResponseBodyEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;

@Aspect
public class IsValid {

    @Pointcut("execution(* hr.algebra.bleneau.webapi.controller.user.UserController.register(..))" +
            "||" +
            "execution(* hr.algebra.bleneau.webapi.controller.user.UserController.update(..))" +
            "||" +
            "execution(* hr.algebra.bleneau.webapi.controller.user.UserController.updatePassword(..))")
    private void doCheck() {
    }

    @Around("doCheck()")
    public Object isValid(ProceedingJoinPoint pjp) throws Throwable {
        if (((Errors) pjp.getArgs()[2]).hasErrors()) {
            ResponseBodyEntity<UserEntity> responseBodyEntity = new ResponseBodyEntity<>();

            responseBodyEntity.fromBindingErrors((Errors) pjp.getArgs()[2]);
            responseBodyEntity.setCode(HttpStatus.BAD_REQUEST.value());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBodyEntity);
        }
        return pjp.proceed();
    }
}
