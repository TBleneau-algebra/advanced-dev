package hr.algebra.bleneau.webapi.controller.attributes;


import hr.algebra.bleneau.webapi.constant.MessageConstant;
import hr.algebra.bleneau.webapi.entity.attributes.business.PackageEntity;
import hr.algebra.bleneau.webapi.entity.response.ResponseBodyEntity;
import hr.algebra.bleneau.webapi.repository.attributes.PackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/api/v1")
public class PackageController {

    private final PackageRepository packageRepository;

    @Autowired
    public PackageController(PackageRepository packageRepository) {
        this.packageRepository = packageRepository;
    }

    @RequestMapping(value = "/package", method = GET, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<List<PackageEntity>>> getAllPackage() {
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBodyEntity<>(packageRepository.findAll(), HttpStatus.OK));
    }

    @RequestMapping(value = "/package/{id}", method = GET, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<PackageEntity>> getPackageById(@PathVariable(value = "id") Long id) {
        ResponseBodyEntity<PackageEntity> responseBodyEntity = new ResponseBodyEntity<>();

        if (!this.packageRepository.existsById(id)) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.PACKAGE_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        responseBodyEntity.setCode(HttpStatus.OK.value());
        responseBodyEntity.setData(this.packageRepository.findById(id).get());
        return ResponseEntity.status(HttpStatus.OK).body(responseBodyEntity);
    }
}

