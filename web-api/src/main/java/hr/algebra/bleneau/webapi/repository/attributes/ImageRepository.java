package hr.algebra.bleneau.webapi.repository.attributes;

import hr.algebra.bleneau.webapi.entity.attributes.image.ImageEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ImageRepository extends JpaRepository<ImageEntity, Long> {

    Page<ImageEntity> findAllByUserOrderByCreatedAtDesc(UserEntity userEntity, Pageable pageable);

    List<ImageEntity> findAllByUserOrderByCreatedAtDesc(UserEntity userEntity);

    Page<ImageEntity> findAllByOrderByCreatedAtDesc(Pageable pageable);

    Optional<ImageEntity> findByUserAndId(UserEntity userEntity, Long Id);

    @Query("SELECT image from ImageEntity image " +
            "INNER JOIN image.tags tags " +
            "WHERE tags.id IN ?1 " +
            "ORDER BY image.createdAt DESC")
    Page<ImageEntity> fetchAllImages(List<Long> tags, Pageable pageable);

    @Query("SELECT image from ImageEntity image " +
            "INNER JOIN image.user user " +
            "WHERE image.title LIKE ?1 " +
            "OR image.description LIKE ?1 OR user.username LIKE ?1 " +
            "ORDER BY image.createdAt DESC")
    Page<ImageEntity> fetchAllImages(String serchTerm, Pageable pageable);

    @Query("SELECT image from ImageEntity image " +
            "WHERE image.width = ?1 " +
            "OR image.height = ?1 " +
            "ORDER BY image.createdAt DESC")
    Page<ImageEntity> fetchAllImages(Integer size, Pageable pageable);

    @Query("SELECT image from ImageEntity image " +
            "INNER JOIN image.user user " +
            "INNER JOIN image.tags tags " +
            "WHERE image.title LIKE ?1 " +
            "OR image.description LIKE ?1 OR user.username LIKE ?1 " +
            "OR tags.id IN ?2 " +
            "ORDER BY image.createdAt DESC")
    Page<ImageEntity> fetchAllImages(String searchTerm, List<Long> tags, Pageable pageable);

    @Query("SELECT image from ImageEntity image " +
            "INNER JOIN image.tags tags " +
            "WHERE image.width = ?1 " +
            "OR image.height = ?1 " +
            "OR tags.id IN ?2 " +
            "ORDER BY image.createdAt DESC")
    Page<ImageEntity> fetchAllImages(Integer size, List<Long> tags, Pageable pageable);

    @Query("SELECT image from ImageEntity image " +
            "INNER JOIN image.user user " +
            "WHERE image.title LIKE ?1 " +
            "OR image.description LIKE ?1 OR user.username LIKE ?1 " +
            "OR image.width = ?2 OR image.height = ?2 " +
            "ORDER BY image.createdAt DESC")
    Page<ImageEntity> fetchAllImages(String searchTerm, Integer size, Pageable pageable);

    @Query("SELECT image from ImageEntity image " +
            "INNER JOIN image.user user " +
            "INNER JOIN image.tags tags " +
            "WHERE image.title LIKE ?1 " +
            "OR image.description LIKE ?1 OR user.username LIKE ?1 " +
            "OR image.width = ?2 OR image.height = ?2 " +
            "OR tags.id IN ?3 " +
            "ORDER BY image.createdAt DESC")
    Page<ImageEntity> fetchAllImages(String searchTerm, Integer size, List<Long> tags, Pageable pageable);
}
