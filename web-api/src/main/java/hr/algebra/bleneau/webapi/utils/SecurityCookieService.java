package hr.algebra.bleneau.webapi.utils;

import org.springframework.stereotype.Service;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class SecurityCookieService {

    /**
     * This method allows you to
     *
     * @param name
     * @param value
     * @param secure
     * @param httpOnly
     * @param maxAge
     * @param domain
     */
    public static Cookie create(String name, String value, Boolean secure, Boolean httpOnly, Integer maxAge, String domain) {
        Cookie cookie = new Cookie(name, value);

        cookie.setSecure(secure);
        cookie.setHttpOnly(httpOnly);
        cookie.setMaxAge(maxAge);
        cookie.setDomain(domain);
        cookie.setPath("/");
        return cookie;
    }


    /**
     *
     * @param httpServletResponse
     * @param name
     */
    public static void clear(HttpServletResponse httpServletResponse, String name) {
        Cookie cookie = new Cookie(name, null);

        cookie.setPath("/");
        cookie.setHttpOnly(true);
        cookie.setMaxAge(0);
        httpServletResponse.addCookie(cookie);
    }

    /**
     *
     * @param httpServletRequest
     * @param name
     */
    public static String getValue(HttpServletRequest httpServletRequest, String name) {
        Cookie cookie = WebUtils.getCookie(httpServletRequest, name);

        return cookie != null ? cookie.getValue() : null;
    }
}
