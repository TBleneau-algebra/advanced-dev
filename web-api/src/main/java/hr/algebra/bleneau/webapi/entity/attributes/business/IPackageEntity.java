package hr.algebra.bleneau.webapi.entity.attributes.business;

import hr.algebra.bleneau.webapi.entity.attributes.image.ImageEntity;

import java.util.List;

public interface IPackageEntity {
    boolean isLimitReached(List<ImageEntity> imageEntities);
}
