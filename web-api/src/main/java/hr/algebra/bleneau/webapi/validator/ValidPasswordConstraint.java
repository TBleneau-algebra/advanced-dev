package hr.algebra.bleneau.webapi.validator;

import org.passay.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

/**
 * This class defines the logic to validate a given constraint {@code ValidPassword}
 * for a given object type {@code String}.
 */
public class ValidPasswordConstraint implements ConstraintValidator<ValidPassword, String> {

    /**
     * Initializes the validator in preparation for calls. The constraint annotation for a given constraint declaration
     * is passed. This method is guaranteed to be called before any use of this instance for validation.
     *
     * @see javax.validation.ConstraintValidator
     * @param arg0 annotation instance for a given constraint declaration
     */
    @Override
    public void initialize(ValidPassword arg0) {
    }

    /**
     * Implements the validation logic. This method can be accessed concurrently, thread-safety must be ensured
     * by the implementation.
     *
     * @see javax.validation.ConstraintValidator
     *
     * @param password password value to validate
     * @param context context in which the constraint is evaluated
     */
    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        PasswordValidator validator = new PasswordValidator(Arrays.asList(
                new LengthRule(8, 64),
                new CharacterRule(EnglishCharacterData.UpperCase, 1),
                new CharacterRule(EnglishCharacterData.LowerCase, 1),
                new CharacterRule(EnglishCharacterData.Digit, 1),
                new CharacterRule(EnglishCharacterData.Special, 1),
                new WhitespaceRule()

        ));

        RuleResult result = validator.validate(new PasswordData(password));
        if (result.isValid()) {
            return true;
        }

        List<String> messages = validator.getMessages(result);
        String messageTemplate = String.join(",", messages);

        context.buildConstraintViolationWithTemplate(messageTemplate)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();

        return false;
    }
}
