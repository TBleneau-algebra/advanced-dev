package hr.algebra.bleneau.webapi.repository.attributes;

import hr.algebra.bleneau.webapi.entity.attributes.image.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<TagEntity, Long> {
}
