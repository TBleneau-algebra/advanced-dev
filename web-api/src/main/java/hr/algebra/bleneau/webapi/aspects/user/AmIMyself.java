package hr.algebra.bleneau.webapi.aspects.user;

import hr.algebra.bleneau.webapi.entity.response.ResponseBodyEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.repository.user.UserRepository;
import hr.algebra.bleneau.webapi.utils.SecurityJWTokenService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

@Aspect
public class AmIMyself {

    private final UserRepository userRepository;

    AmIMyself(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Pointcut("execution(* hr.algebra.bleneau.webapi.controller.user.UserController.update(..))" +
            "||" +
            "execution(* hr.algebra.bleneau.webapi.controller.user.UserController.updatePassword(..))")
    private void doCheck() {
    }

    @Around("doCheck()")
    public Object doCheck(ProceedingJoinPoint pjp) throws Throwable {
        HttpServletRequest httpServletRequest = (HttpServletRequest) pjp.getArgs()[0];
        UserEntity userEntity = (UserEntity) pjp.getArgs()[1];

        if (!userEntity.getUsername().equals(SecurityJWTokenService.getSubject(httpServletRequest)) &&
                this.userRepository.findByUsername(SecurityJWTokenService.getSubject(httpServletRequest)).hasRole("ROLE_ADMIN")) {
            return pjp.proceed();
        } else if (!userEntity.getUsername().equals(SecurityJWTokenService.getSubject(httpServletRequest))) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(new ResponseBodyEntity<>(HttpStatus.UNAUTHORIZED));
        }

        return pjp.proceed();
    }

}
