package hr.algebra.bleneau.webapi.entity.attributes.business;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hr.algebra.bleneau.webapi.entity.attributes.image.ImageEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ProfessionalPackageEntity extends PackageEntity {

    ProfessionalPackageEntity() {
        this.setId(2L);
        this.setPrice(69);
        this.setDailyPhotos(100);
        this.setUnlimited(false);
        this.setDailyUploadSize(40);
        this.setName("PROFESSIONAL_PACKAGE");
    }

    @JsonIgnore
    @Override
    public boolean isLimitReached(List<ImageEntity> imageEntities) {
        return isDailyLimitReached(imageEntities) || isUploadSizeLimitReached(imageEntities);
    }

    @JsonIgnore
    private boolean isDailyLimitReached(List<ImageEntity> imageEntities) {
        Set<ImageEntity> images = imageEntities.stream().filter(image -> image.getCreatedAt()
                .isAfter(LocalDateTime.now().minusHours(24))).collect(Collectors.toSet());

        return images.size() >= this.getDailyPhotos();
    }

    @JsonIgnore
    private boolean isUploadSizeLimitReached(List<ImageEntity> imageEntities) {
        Set<ImageEntity> images = imageEntities.stream().filter(image -> image.getCreatedAt()
                .isAfter(LocalDateTime.now().minusHours(24))).collect(Collectors.toSet());
        long sum = images.stream().mapToLong(entity -> entity.getImage().getBytes().length).sum();

        return sum > this.getDailyUploadSize() * 1000000;
    }

}
