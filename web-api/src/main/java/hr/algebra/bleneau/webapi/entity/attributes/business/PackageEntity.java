package hr.algebra.bleneau.webapi.entity.attributes.business;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hr.algebra.bleneau.webapi.entity.attributes.image.ImageEntity;
import hr.algebra.bleneau.webapi.entity.user.UserPackageEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "packages")
@EntityListeners(AuditingEntityListener.class)
public class PackageEntity implements IPackageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    private String name;

    @Column(name = "daily_photos")
    private int dailyPhotos;

    @NotNull
    @Column(name = "daily_upload_size")
    private int dailyUploadSize;

    @NotNull
    @Column(name = "price")
    private int price;

    @NotNull
    @Column(name = "unlimited")
    private boolean unlimited;

    @JsonIgnore
    @OneToMany(mappedBy = "pack", cascade = CascadeType.ALL)
    private Set<UserPackageEntity> usersPackages = new HashSet<>();

    @JsonIgnore
    @Column(name = "created_at", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @JsonIgnore
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt;

    @JsonIgnore
    public boolean isLimitReached(List<ImageEntity> imageEntities) {
        return false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDailyPhotos() {
        return dailyPhotos;
    }

    public void setDailyPhotos(int dailyPhotos) {
        this.dailyPhotos = dailyPhotos;
    }

    public int getDailyUploadSize() {
        return dailyUploadSize;
    }

    public void setDailyUploadSize(int dailyUploadSize) {
        this.dailyUploadSize = dailyUploadSize;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isUnlimited() {
        return unlimited;
    }

    public void setUnlimited(boolean unlimited) {
        this.unlimited = unlimited;
    }

    public Set<UserPackageEntity> getUsersPackages() {
        return usersPackages;
    }

    public void setUsersPackages(Set<UserPackageEntity> usersPackages) {
        this.usersPackages = usersPackages;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
