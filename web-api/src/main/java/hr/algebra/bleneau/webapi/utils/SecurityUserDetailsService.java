package hr.algebra.bleneau.webapi.utils;

import hr.algebra.bleneau.webapi.entity.attributes.role.RoleEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.repository.user.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecurityUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public SecurityUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(username);

        if (userEntity == null)
            throw new UsernameNotFoundException(username);

        return new User(userEntity.getUsername(), userEntity.getPassword(), SecurityUserDetailsService.getAuthorities(userEntity));
    }

    private static List<? extends GrantedAuthority> getAuthorities(UserEntity user) {
        String[] userRoles = user.getRoles().stream().map(RoleEntity::getName).toArray(String[]::new);

        return AuthorityUtils.createAuthorityList(userRoles);
    }
}
