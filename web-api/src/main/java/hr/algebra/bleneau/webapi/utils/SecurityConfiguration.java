package hr.algebra.bleneau.webapi.utils;

import hr.algebra.bleneau.webapi.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

/**
 * This class defines the security configuration of the application.
 * The implementation allows customization by overriding methods of the WebSecurityConfigurerAdapter class.
 *
 * @see org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
 * @see EnableWebSecurity
 */
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * Repository which allows the creation of the user data access layer (SELECT, UPDATE requests...) faster
     */
    private final UserRepository userRepository;

    /**
     * Core interface which loads user-specific data
     */
    private final UserDetailsService userDetailsService;

    /**
     * Custom login url defines in the application.properties
     */
    @Value("${aadbdt.login.url}")
    private String loginUrl;

    /**
     * @param securityUserDetailsService Custom class which implement the core interface which loads user-specific data
     * @param userRepository             Repository which allows the creation of the user data access layer (SELECT,
     *                                   UPDATE requests...) faster
     */
    public SecurityConfiguration(SecurityUserDetailsService securityUserDetailsService, UserRepository userRepository) {
        this.userDetailsService = securityUserDetailsService;
        this.userRepository = userRepository;
    }

    /**
     * Bean which implement the PasswordEncoder interface that uses the BCrypt strong hashing function
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * This method configure the token for an authentication request
     *
     * @param authenticationManagerBuilder Allows for easily building in memory authentication, LDAP authentication,
     *                                     JDBC based authentication, adding UserDetailsService
     */
    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    /**
     * This method configure the {@link HttpSecurity}. It allows configuring web based security for specific http
     * requests
     *
     * @param http web based security for specific http requests
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().configurationSource(this.corsConfigurationSource()).and().csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/user/logged").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/user/register").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/user/login").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/package").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/image").permitAll()
                .antMatchers("/actuator/**").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .addFilter(authenticationFilter())
                .addFilter(new SecurityJWTokenAuthorizationFilter(authenticationManager()))
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    /**
     * Ths method creates a an authentication form submission. It uses the SecurityJWTokenAuthenticationFilter class
     * to authenticate a user and assign a JW Token to this user in a Cookie
     */
    private SecurityJWTokenAuthenticationFilter authenticationFilter() throws Exception {
        SecurityJWTokenAuthenticationFilter filter = new SecurityJWTokenAuthenticationFilter(userRepository, authenticationManager());

        filter.setFilterProcessesUrl(loginUrl);
        return filter;
    }

    /**
     * This method provides a {@link CorsConfiguration} instance based on the provided request
     */
    private CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration configuration = new CorsConfiguration();

        configuration.applyPermitDefaultValues().setAllowedOrigins(Collections.singletonList("*"));
        configuration.applyPermitDefaultValues().setAllowCredentials(true);
        configuration.applyPermitDefaultValues().setExposedHeaders(Arrays.asList("page.total", "element.total"));
        configuration.applyPermitDefaultValues().setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }
}
