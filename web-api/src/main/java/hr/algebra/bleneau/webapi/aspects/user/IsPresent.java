package hr.algebra.bleneau.webapi.aspects.user;

import hr.algebra.bleneau.webapi.constant.MessageConstant;
import hr.algebra.bleneau.webapi.entity.response.ResponseBodyEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.repository.user.UserRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Aspect
public class IsPresent {

    private final UserRepository userRepository;

    IsPresent(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Pointcut("execution(* hr.algebra.bleneau.webapi.controller.user.UserController.update(..))" +
            "||" +
            "execution(* hr.algebra.bleneau.webapi.controller.user.UserController.updatePassword(..))")
    private void doCheck() {
    }

    @Around("doCheck()")
    public Object isPresent(ProceedingJoinPoint pjp) throws Throwable {
        if (!this.userRepository.findById(((UserEntity) pjp.getArgs()[1]).getId()).isPresent()) {
            ResponseBodyEntity<UserEntity> responseBodyEntity = new ResponseBodyEntity<>();

            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }
        return pjp.proceed();
    }
}
