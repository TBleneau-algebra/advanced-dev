package hr.algebra.bleneau.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * This class corresponds to the Application class.
 * The static method main allows you to launch your Spring Boot Application.
 * <p>
 * - @EnableAspectJAutoProxy Enables support for handling components marked with AspectJ's {@code @Aspect} annotation
 * <p>
 * The annotation @SpringBootApplication is a convenience annotation that adds all of the following :
 * <p>
 * - @Configuration tags the class as a source of bean definitions for the application context.
 * <p>
 * - @EnableAutoConfiguration tells Spring Boot to start adding beans based on classpath settings, other beans,
 * and various property settings
 * <p>
 * - @ComponentScan tells Spring to look for other components, configurations, and services in the
 * hr.algebra.bleneau.webapi package, letting it find the controllers.
 */
@EnableScheduling
@EnableAspectJAutoProxy
@SpringBootApplication
@ImportResource("classpath:beans-config.xml")
public class WebApiApplication {

    /**
     * This method defines the application launch point
     *
     * @param args arguments passed to the programme when it was launched
     */
    public static void main(String[] args) {
        SpringApplication.run(WebApiApplication.class, args);
    }

}
