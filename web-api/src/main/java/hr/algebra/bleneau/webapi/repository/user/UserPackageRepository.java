package hr.algebra.bleneau.webapi.repository.user;


import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.entity.user.UserPackageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPackageRepository extends JpaRepository<UserPackageEntity, Long> {
    UserPackageEntity findByUser(UserEntity userEntity);
}
