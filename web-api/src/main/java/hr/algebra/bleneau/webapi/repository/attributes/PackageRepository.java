package hr.algebra.bleneau.webapi.repository.attributes;

import hr.algebra.bleneau.webapi.entity.attributes.business.PackageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PackageRepository extends JpaRepository<PackageEntity, Long> {
}
