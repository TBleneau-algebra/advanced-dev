package hr.algebra.bleneau.webapi.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.algebra.bleneau.webapi.constant.MessageConstant;
import hr.algebra.bleneau.webapi.constant.TokenConstant;
import hr.algebra.bleneau.webapi.entity.attributes.role.RoleEntity;
import hr.algebra.bleneau.webapi.entity.response.ResponseBodyEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.repository.user.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SecurityJWTokenAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    /**
     *
     */
    private AuthenticationManager authenticationManager;

    /**
     *
     */
    private final UserRepository userRepository;


    /**
     *
     * @param userRepository
     * @param authenticationManager
     */
    SecurityJWTokenAuthenticationFilter(UserRepository userRepository, AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
    }

    /**
     *
     * @param req
     * @param res
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException {

        try {
            UserEntity userEntity = new ObjectMapper().readValue(req.getInputStream(), UserEntity.class);
            UserEntity userExist = this.userRepository.findByUsername(userEntity.getUsername());
            List<GrantedAuthority> roles = new ArrayList<>();

            if (userExist != null) {
                Set<String> authorities = new HashSet<>();

                for (RoleEntity roleEntity : userExist.getRoles()) {
                    authorities.add(roleEntity.getName());
                }

                roles = AuthorityUtils.commaSeparatedStringToAuthorityList(String.join(",", authorities));
            }
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userEntity.getUsername(), userEntity.getPassword(), roles));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *
     * @param httpServletRequest
     * @param httpServletResponse
     * @param chain
     * @param auth
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain chain, Authentication auth) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ResponseBodyEntity<UserEntity> responseBodyEntity = new ResponseBodyEntity<>(HttpStatus.OK);

        UserEntity userEntity = userRepository.findByUsername(((User) auth.getPrincipal()).getUsername());

        Cookie JWTCookie = SecurityCookieService.create(TokenConstant.TOKEN_NAME, SecurityJWTokenService.createToken(userEntity), false, true, TokenConstant.TOKEN_EXPIRATION_TIME, "localhost");
        Cookie AuthCookie = SecurityCookieService.create(TokenConstant.TOKEN_AUTHENTICATED_NAME, String.valueOf(true), false, false, JWTCookie.getMaxAge(), "localhost");

        responseBodyEntity.setData(userEntity);

        httpServletResponse.addCookie(JWTCookie);
        httpServletResponse.addCookie(AuthCookie);
        httpServletResponse.setContentType("application/json");
        httpServletResponse.getOutputStream().println(objectMapper.writeValueAsString(responseBodyEntity));
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
    }

    /**
     *
     * @param request
     * @param response
     * @param failed
     */
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        ObjectMapper objectMapper = new ObjectMapper();
        ResponseBodyEntity<UserEntity> responseBodyEntity = new ResponseBodyEntity<>(HttpStatus.NOT_FOUND);

        responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());

        response.setContentType("application/json");
        response.getOutputStream().println(objectMapper.writeValueAsString(responseBodyEntity));
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }
}
