package hr.algebra.bleneau.webapi.repository.user;


import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByUsername(String username);

    UserEntity findByEmail(String username);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);
}
