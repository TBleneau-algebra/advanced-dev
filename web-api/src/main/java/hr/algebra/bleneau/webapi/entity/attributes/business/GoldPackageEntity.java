package hr.algebra.bleneau.webapi.entity.attributes.business;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hr.algebra.bleneau.webapi.entity.attributes.image.ImageEntity;

import java.util.List;

public class GoldPackageEntity extends PackageEntity {

    GoldPackageEntity() {
        this.setId(3L);
        this.setPrice(120);
        this.setDailyPhotos(0);
        this.setUnlimited(true);
        this.setDailyUploadSize(0);
        this.setName("GOLD_PACKAGE");
    }

    @JsonIgnore
    @Override
    public boolean isLimitReached(List<ImageEntity> imageEntities) {
        return false;
    }
}
