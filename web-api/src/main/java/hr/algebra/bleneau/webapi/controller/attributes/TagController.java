package hr.algebra.bleneau.webapi.controller.attributes;


import hr.algebra.bleneau.webapi.entity.attributes.image.TagEntity;
import hr.algebra.bleneau.webapi.entity.response.ResponseBodyEntity;
import hr.algebra.bleneau.webapi.repository.attributes.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/api/v1")
public class TagController {

    private final TagRepository tagRepository;

    @Autowired
    public TagController(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @RequestMapping(value = "/tag", method = GET, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<List<TagEntity>>> getAll(@RequestHeader MultiValueMap<String, String> headers) {
        Page<TagEntity> tagEntities;
        ResponseBodyEntity<List<TagEntity>> responseBodyEntity = new ResponseBodyEntity<>();
        MultiValueMap<String, String> headersResponse = new LinkedMultiValueMap<>();

        tagEntities = this.tagRepository.findAll(createPagination(headers));

        headersResponse.add("page.total", Integer.toString(tagEntities.getTotalPages()));
        headersResponse.add("element.total", Long.toString(tagEntities.getTotalElements()));

        responseBodyEntity.setCode(HttpStatus.OK.value());
        responseBodyEntity.setMessage(HttpStatus.OK.getReasonPhrase());
        responseBodyEntity.setData(tagEntities.getContent());
        return new ResponseEntity<>(responseBodyEntity, headersResponse, HttpStatus.OK);
    }

    private Pageable createPagination(MultiValueMap<String, String> headers) {
        Pageable pageable = PageRequest.of(0, 20, Sort.by("createdAt").descending());

        if (headers.containsKey("page.size") && headers.containsKey("page.from")) {
            pageable = PageRequest.of(Integer.parseInt(Objects.requireNonNull(headers.getFirst("page.from"))),
                    Integer.parseInt(Objects.requireNonNull(headers.getFirst("page.size"))),
                    Sort.by("id").ascending());
        }
        return pageable;
    }
}
