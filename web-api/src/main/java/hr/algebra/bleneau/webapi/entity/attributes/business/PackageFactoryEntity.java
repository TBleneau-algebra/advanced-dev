package hr.algebra.bleneau.webapi.entity.attributes.business;

public abstract class PackageFactoryEntity {

    static public PackageEntity create(int id) {
        switch (id) {
            case 2:
                return new ProfessionalPackageEntity();
            case 3:
                return new GoldPackageEntity();
            default:
                return new FreePackageEntity();
        }
    }
}
