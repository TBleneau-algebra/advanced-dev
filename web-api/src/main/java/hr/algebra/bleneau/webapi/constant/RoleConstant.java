package hr.algebra.bleneau.webapi.constant;

public enum RoleConstant {
    ROLE_USER,
    ROLE_ADMIN
}
