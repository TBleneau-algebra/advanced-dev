package hr.algebra.bleneau.webapi.entity.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

public class ResponseBodyEntity<T> {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<String> errors = new ArrayList<>();

    private int code;

    @Nullable
    private String message;

    @Nullable
    private T data;

    public ResponseBodyEntity() {
    }

    public ResponseBodyEntity(HttpStatus status) {
        this.message = status.getReasonPhrase();
        this.code = status.value();
    }

    public ResponseBodyEntity(T data) {
        this.data = data;
    }

    public ResponseBodyEntity(T data, HttpStatus status) {
        this.data = data;
        this.message = status.getReasonPhrase();
        this.code = status.value();
    }

    public void fromBindingErrors(Errors errors) {
        for (ObjectError objectError : errors.getAllErrors()) {
            this.errors.add(objectError.getDefaultMessage());
        }
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
