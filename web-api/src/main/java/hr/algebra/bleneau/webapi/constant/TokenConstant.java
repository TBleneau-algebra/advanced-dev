package hr.algebra.bleneau.webapi.constant;

public class TokenConstant {
    public static final String TOKEN_ID_KEY = "id";
    public static final String TOKEN_AUTHORITIES_KEY = "roles";
    public static final int TOKEN_EXPIRATION_TIME = 7200;
    public static final String TOKEN_NAME = "JWToken";
    public static final String TOKEN_AUTHENTICATED_NAME = "AUTHENTICATED";
}
