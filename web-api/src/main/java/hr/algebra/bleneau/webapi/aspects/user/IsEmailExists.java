package hr.algebra.bleneau.webapi.aspects.user;

import hr.algebra.bleneau.webapi.constant.MessageConstant;
import hr.algebra.bleneau.webapi.entity.response.ResponseBodyEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.repository.user.UserRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

@Aspect
public class IsEmailExists {

    private final UserRepository userRepository;

    IsEmailExists(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Pointcut("execution(* hr.algebra.bleneau.webapi.controller.user.UserController.register(..))" +
            "||" +
            "execution(* hr.algebra.bleneau.webapi.controller.user.UserController.update(..))")
    private void doCheck() {
    }

    @Around("doCheck()")
    public Object isUserEmailExists(ProceedingJoinPoint pjp) throws Throwable {

        if (((UserEntity) pjp.getArgs()[1]).getId() != null) {
            UserEntity userEntity = this.userRepository.findByEmail(((UserEntity) pjp.getArgs()[1]).getEmail());

            if (userEntity != null && userEntity.getEmail().equals(((UserEntity) pjp.getArgs()[1]).getEmail()) &&
                    !userEntity.getId().equals(((UserEntity) pjp.getArgs()[1]).getId())) {
                ResponseBodyEntity<UserEntity> responseBodyEntity = new ResponseBodyEntity<>();

                responseBodyEntity.setCode(HttpStatus.CONFLICT.value());
                responseBodyEntity.getErrors().add(MessageConstant.EMAIL_ALREADY_USED.name());
                return ResponseEntity.status(HttpStatus.CONFLICT).body(responseBodyEntity);
            }
        } else {
            if (this.userRepository.existsByEmail(((UserEntity) pjp.getArgs()[1]).getEmail())) {
                ResponseBodyEntity<UserEntity> responseBodyEntity = new ResponseBodyEntity<>();

                responseBodyEntity.setCode(HttpStatus.CONFLICT.value());
                responseBodyEntity.getErrors().add(MessageConstant.EMAIL_ALREADY_USED.name());
                return ResponseEntity.status(HttpStatus.CONFLICT).body(responseBodyEntity);
            }
        }
        return pjp.proceed();
    }
}
