package hr.algebra.bleneau.webapi.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * This interface allows us to define a custom Beans that we can use to check the validity of the user password.
 *
 * - @Documented A simple marker annotations which tell whether to add Annotation in Javadocs or not.
 *
 * - @Constraint Marks an annotation as being a Bean Validation Constraint. The element validatedBy specifies
 *   the classes implementing the constraint
 *
 * - @Target Is where our annotations can be used. If you don't specify this, the annotation can be placed anywhere.
 *
 * - @Retention Defines for how long the annotation should be kept.
 */
@Documented
@Constraint(validatedBy = ValidPasswordConstraint.class)
@Target({FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface ValidPassword {

    /**
     * Relative to the @Constraint annotation. Used to set a default error message.
     *
     * @see javax.validation.Constraint
     */
    String message() default "Password is invalid. Password should not be less than 8 characters. " +
            "Password should not be greater than 64 characters. " +
            "Password should contain at least 1 special character, 1 uppercase, 1 lowercase.";

    /**
     * Relative to the @Constraint annotation. Used to customize the targeted groups.
     *
     * @see javax.validation.Constraint
     */
    Class<?>[] groups() default {};

    /**
     * Relative to the @Constraint annotation. Used for extensibility purposes.
     *
     * @see javax.validation.Constraint
     */
    Class<? extends Payload>[] payload() default {};
}
