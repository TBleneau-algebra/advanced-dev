package hr.algebra.bleneau.webapi.controller.user;


import hr.algebra.bleneau.webapi.constant.MessageConstant;
import hr.algebra.bleneau.webapi.constant.RoleConstant;
import hr.algebra.bleneau.webapi.constant.TokenConstant;
import hr.algebra.bleneau.webapi.entity.attributes.business.PackageFactoryEntity;
import hr.algebra.bleneau.webapi.entity.response.ResponseBodyEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.entity.user.UserPackageEntity;
import hr.algebra.bleneau.webapi.repository.attributes.RoleRepository;
import hr.algebra.bleneau.webapi.repository.user.UserRepository;
import hr.algebra.bleneau.webapi.utils.SecurityCookieService;
import hr.algebra.bleneau.webapi.utils.SecurityJWTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/api/v1")
public class UserController {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final RoleRepository roleRepository;

    @Autowired
    public UserController(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    @RequestMapping(method = POST, path = "/user/register", produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<UserEntity>> register(HttpServletRequest httpServletRequest,
                                                                   @Valid @RequestBody UserEntity userEntity,
                                                                   Errors errors) {

        UserPackageEntity userPackageEntity = new UserPackageEntity(userEntity);
        userPackageEntity.setPack(PackageFactoryEntity.create(1));

        userEntity.getRoles().add(this.roleRepository.findByName(RoleConstant.ROLE_USER.name()));

        userEntity.setUserPackage(userPackageEntity);
        userEntity.setPassword(this.passwordEncoder.encode(userEntity.getPassword()));

        this.userRepository.save(userEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseBodyEntity<>(userEntity, HttpStatus.CREATED));
    }

    @RequestMapping(value = "/user/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

        if (httpServletRequest.getSession(false) != null)
            httpServletRequest.getSession(false).invalidate();

        SecurityCookieService.clear(httpServletResponse, TokenConstant.TOKEN_NAME);
        SecurityCookieService.clear(httpServletResponse, TokenConstant.TOKEN_AUTHENTICATED_NAME);
        httpServletResponse.setStatus(200);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/user", method = GET, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<List<UserEntity>>> getAll() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new ResponseBodyEntity<>(userRepository.findAll(), HttpStatus.OK));
    }

    @RequestMapping(value = "/user/me", method = GET, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<UserEntity>> get(HttpServletRequest httpServletRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new ResponseBodyEntity<>(userRepository.findByUsername(SecurityJWTokenService.getSubject(httpServletRequest)), HttpStatus.OK));
    }

    @RequestMapping(value = "/user/{id}", method = GET, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<UserEntity>> get(@PathVariable(value = "id") Long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new ResponseBodyEntity<>(userRepository.findById(id).get(), HttpStatus.OK));
    }

    @RequestMapping(value = "/user", method = PUT, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<UserEntity>> update(HttpServletRequest httpServletRequest,
                                                                 @RequestBody UserEntity userEntity,
                                                                 Errors errors) {
        Optional<UserEntity> optionalUserEntity = this.userRepository.findById(userEntity.getId());

        optionalUserEntity.get().setEmail(userEntity.getEmail());
        optionalUserEntity.get().setUsername(userEntity.getUsername());
        optionalUserEntity.get().setLastname(userEntity.getLastname());
        optionalUserEntity.get().setName(userEntity.getName());
        optionalUserEntity.get().setProfilePicture(userEntity.getProfilePicture());

        this.userRepository.save(optionalUserEntity.get());

        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBodyEntity<>(optionalUserEntity.get(), HttpStatus.OK));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/user/{id}", method = DELETE, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<UserEntity>> delete(@PathVariable(value = "id") Long id) {
        ResponseBodyEntity<UserEntity> responseBodyEntity = new ResponseBodyEntity<>();
        Optional<UserEntity> userEntity = this.userRepository.findById(id);

        if (!userEntity.isPresent()) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        if (userEntity.get().hasRole(RoleConstant.ROLE_ADMIN.name())) {
            responseBodyEntity.setCode(HttpStatus.UNAUTHORIZED.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_DELETE_ADMIN.name());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(responseBodyEntity);
        }

        this.userRepository.delete(userEntity.get());

        responseBodyEntity.setCode(HttpStatus.OK.value());
        return ResponseEntity.status(HttpStatus.OK).body(responseBodyEntity);
    }

    @RequestMapping(value = "/user/password", method = PUT, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<UserEntity>> updatePassword(HttpServletRequest httpServletRequest,
                                                                         @RequestBody UserEntity userEntity,
                                                                         Errors errors) {

        Optional<UserEntity> optionalUserEntity = this.userRepository.findById(userEntity.getId());

        optionalUserEntity.get().setPassword(this.passwordEncoder.encode(userEntity.getPassword()));
        this.userRepository.save(optionalUserEntity.get());

        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBodyEntity<>(optionalUserEntity.get(), HttpStatus.OK));
    }
}
