package hr.algebra.bleneau.webapi.controller.user;

import hr.algebra.bleneau.webapi.constant.MessageConstant;
import hr.algebra.bleneau.webapi.constant.RoleConstant;
import hr.algebra.bleneau.webapi.entity.attributes.business.PackageFactoryEntity;
import hr.algebra.bleneau.webapi.entity.response.ResponseBodyEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.entity.user.UserPackageEntity;
import hr.algebra.bleneau.webapi.repository.user.UserPackageRepository;
import hr.algebra.bleneau.webapi.repository.user.UserRepository;
import hr.algebra.bleneau.webapi.utils.SecurityJWTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping("/api/v1")
public class UserPackageController {

    private final UserRepository userRepository;
    private final UserPackageRepository userPackageRepository;

    @Autowired
    public UserPackageController(UserRepository userRepository, UserPackageRepository userPackageRepository) {
        this.userRepository = userRepository;
        this.userPackageRepository = userPackageRepository;
    }

    @RequestMapping(value = "/user/me/package", method = GET, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<UserPackageEntity>> get(HttpServletRequest httpServletRequest) {
        UserPackageEntity userPackageEntity = this.userPackageRepository.findByUser(this.userRepository.findByUsername(SecurityJWTokenService.getSubject(httpServletRequest)));

        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBodyEntity<>(userPackageEntity, HttpStatus.OK));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/user/{id}/package", method = GET, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<UserPackageEntity>> get(@PathVariable(value = "id") Long id) {
        UserPackageEntity userPackageEntity = this.userPackageRepository.findByUser(this.userRepository.findById(id).get());

        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBodyEntity<>(userPackageEntity, HttpStatus.OK));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/user/{id}/package/{packageId}", method = PUT, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<UserEntity>> updateUserPackageById(HttpServletRequest httpServletRequest,
                                                                                @PathVariable(value = "id") Long id,
                                                                                @PathVariable(value = "packageId") Long packageId) {
        ResponseBodyEntity<UserEntity> responseBodyEntity = new ResponseBodyEntity<>();

        if (packageId == null) {
            responseBodyEntity.getErrors().add(MessageConstant.PACKAGE_NOT_FOUND.name());
            responseBodyEntity.setCode(HttpStatus.BAD_REQUEST.value());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBodyEntity);
        }

        Optional<UserEntity> optionalUserEntity = this.userRepository.findById(id);

        if (!optionalUserEntity.isPresent()) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        if (optionalUserEntity.get().hasRole(RoleConstant.ROLE_ADMIN.name()) && !optionalUserEntity.get().getUsername().equals(
                SecurityJWTokenService.getSubject(httpServletRequest))) {
            responseBodyEntity.setData(optionalUserEntity.get());
            responseBodyEntity.setCode(HttpStatus.UNAUTHORIZED.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_UPDATE_ADMIN.name());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(responseBodyEntity);
        }

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime packageCreated = optionalUserEntity.get().getUserPackage().getCreatedAt();
        LocalDateTime packageUpdated = optionalUserEntity.get().getUserPackage().getUpdatedAt();

        if (!packageCreated.equals(packageUpdated) && now.isAfter(packageUpdated)
                && now.isBefore(packageUpdated.plusDays(1))) {
            responseBodyEntity.setCode(HttpStatus.UNAUTHORIZED.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_UPDATE_PACKAGE.name());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(responseBodyEntity);
        }

        optionalUserEntity.get().getUserPackage().setPack(PackageFactoryEntity.create(packageId.intValue()));
        optionalUserEntity.get().getUserPackage().setUpdatedAt(now);

        this.userRepository.save(optionalUserEntity.get());

        responseBodyEntity.setCode(HttpStatus.OK.value());
        responseBodyEntity.setData(optionalUserEntity.get());
        return ResponseEntity.status(HttpStatus.OK).body(responseBodyEntity);
    }

    @RequestMapping(value = "/user/me/package/{id}", method = PUT, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<UserEntity>> updateUserMePackage(HttpServletRequest httpServletRequest,
                                                                              @PathVariable(value = "id") Long id) {
        ResponseBodyEntity<UserEntity> responseBodyEntity = new ResponseBodyEntity<>();

        if (id == null) {
            responseBodyEntity.getErrors().add(MessageConstant.PACKAGE_NOT_FOUND.name());
            responseBodyEntity.setCode(HttpStatus.BAD_REQUEST.value());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBodyEntity);
        }

        UserEntity userExist = this.userRepository.findByUsername(SecurityJWTokenService.getSubject(httpServletRequest));

        if (userExist == null) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime packageCreated = userExist.getUserPackage().getCreatedAt();
        LocalDateTime packageUpdated = userExist.getUserPackage().getUpdatedAt();

        if (!packageCreated.equals(packageUpdated) && now.isAfter(packageUpdated)
                && now.isBefore(packageUpdated.plusDays(1))) {
            responseBodyEntity.setCode(HttpStatus.UNAUTHORIZED.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_UPDATE_PACKAGE.name());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(responseBodyEntity);
        }

        userExist.getUserPackage().setPack(PackageFactoryEntity.create(id.intValue()));
        userExist.getUserPackage().setUpdatedAt(now);

        this.userRepository.save(userExist);

        responseBodyEntity.setCode(HttpStatus.OK.value());
        responseBodyEntity.setData(userExist);
        return ResponseEntity.status(HttpStatus.OK).body(responseBodyEntity);
    }
}
