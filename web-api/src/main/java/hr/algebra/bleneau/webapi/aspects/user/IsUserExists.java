package hr.algebra.bleneau.webapi.aspects.user;

import hr.algebra.bleneau.webapi.constant.MessageConstant;
import hr.algebra.bleneau.webapi.entity.response.ResponseBodyEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.repository.user.UserRepository;
import hr.algebra.bleneau.webapi.utils.SecurityJWTokenService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

@Aspect
public class IsUserExists {

    private final UserRepository userRepository;

    IsUserExists(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Pointcut("execution(* hr.algebra.bleneau.webapi.controller.user.UserController.get(..))" +
            "||" +
            "execution(* hr.algebra.bleneau.webapi.controller.user.UserPackageController.get(..))")
    private void doCheck() {
    }

    @Around("doCheck()")
    public Object isUserExists(ProceedingJoinPoint pjp) throws Throwable {

        if (pjp.getArgs()[0] instanceof Long) {
            if (!this.userRepository.existsById(((Long) pjp.getArgs()[0]))) {
                ResponseBodyEntity<UserEntity> responseBodyEntity = new ResponseBodyEntity<>();

                responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
                responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
            }
        } else {
            if (!this.userRepository.existsByUsername(SecurityJWTokenService.getSubject((HttpServletRequest) pjp.getArgs()[0]))) {
                ResponseBodyEntity<UserEntity> responseBodyEntity = new ResponseBodyEntity<>();

                responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
                responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
            }
        }

        return pjp.proceed();
    }
}
