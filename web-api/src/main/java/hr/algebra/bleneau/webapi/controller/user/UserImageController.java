package hr.algebra.bleneau.webapi.controller.user;

import hr.algebra.bleneau.webapi.constant.MessageConstant;
import hr.algebra.bleneau.webapi.entity.attributes.image.ImageEntity;
import hr.algebra.bleneau.webapi.entity.response.ResponseBodyEntity;
import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.repository.attributes.ImageRepository;
import hr.algebra.bleneau.webapi.repository.user.UserRepository;
import hr.algebra.bleneau.webapi.utils.SecurityJWTokenService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/api/v1")
public class UserImageController {

    private final UserRepository userRepository;
    private final ImageRepository imageRepository;

    @Autowired
    public UserImageController(UserRepository userRepository, ImageRepository imageRepository) {
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
    }

    @RequestMapping(value = "/image", method = GET, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<List<ImageEntity>>> getImage(@Param(value = "searchTerm") String searchTerm,
                                                                          @Param(value = "size") String size,
                                                                          @RequestParam(required = false, name = "tag") List<String> tags,
                                                                          @RequestHeader MultiValueMap<String, String> headers) {
        Page<ImageEntity> imageEntityPage;
        List<Long> tagsId = new ArrayList<>();
        ResponseBodyEntity<List<ImageEntity>> responseBodyEntity = new ResponseBodyEntity<>();

        if (tags != null) {
            for (String tmp : tags) {
                if (!StringUtils.isNumeric(tmp)) {
                    responseBodyEntity.setData(new ArrayList<>());
                    return new ResponseEntity<>(responseBodyEntity, HttpStatus.NOT_FOUND);
                }
                tagsId.add(Long.valueOf(tmp));
            }
        }

        if (StringUtils.isNumeric(size)) {
            Integer sizeTerm = Integer.parseInt(size);

            if (searchTerm != null) {
                imageEntityPage = (tagsId.isEmpty()) ?
                        this.imageRepository.fetchAllImages(searchTerm, sizeTerm, createPagination(headers)) :
                        this.imageRepository.fetchAllImages(searchTerm, sizeTerm, tagsId, createPagination(headers));
            } else {
                imageEntityPage = (tagsId.isEmpty()) ?
                        this.imageRepository.fetchAllImages(sizeTerm, createPagination(headers)) :
                        this.imageRepository.fetchAllImages(sizeTerm, tagsId, createPagination(headers));
            }
        } else {
            if (searchTerm != null) {
                imageEntityPage = (tagsId.isEmpty()) ?
                        this.imageRepository.fetchAllImages(searchTerm, createPagination(headers)) :
                        this.imageRepository.fetchAllImages(searchTerm, tagsId, createPagination(headers));
                ;
            } else {
                imageEntityPage = (tagsId.isEmpty()) ?
                        this.imageRepository.findAllByOrderByCreatedAtDesc(createPagination(headers)) :
                        this.imageRepository.fetchAllImages(tagsId, createPagination(headers));
            }
        }

        MultiValueMap<String, String> headersResponse = new LinkedMultiValueMap<>();

        headersResponse.add("page.total", Integer.toString(imageEntityPage.getTotalPages()));
        headersResponse.add("element.total", Long.toString(imageEntityPage.getTotalElements()));

        responseBodyEntity.setCode(HttpStatus.OK.value());
        responseBodyEntity.setMessage(HttpStatus.OK.getReasonPhrase());
        responseBodyEntity.setData(imageEntityPage.getContent());
        return new ResponseEntity<>(responseBodyEntity, headersResponse, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/me/image", method = GET, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<List<ImageEntity>>> getUserImage(@RequestHeader MultiValueMap<String, String> headers,
                                                                              HttpServletRequest httpServletRequest) {
        ResponseBodyEntity<List<ImageEntity>> responseBodyEntity = new ResponseBodyEntity<>();

        if (!this.userRepository.existsByUsername(SecurityJWTokenService.getSubject(httpServletRequest))) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        Page<ImageEntity> imageEntityPage = this.imageRepository.findAllByUserOrderByCreatedAtDesc(this.userRepository.findByUsername(SecurityJWTokenService.getSubject(httpServletRequest)),
                createPagination(headers));
        MultiValueMap<String, String> headersResponse = new LinkedMultiValueMap<>();

        headersResponse.add("page.total", Integer.toString(imageEntityPage.getTotalPages()));
        headersResponse.add("element.total", Long.toString(imageEntityPage.getTotalElements()));

        responseBodyEntity.setCode(HttpStatus.OK.value());
        responseBodyEntity.setMessage(HttpStatus.OK.getReasonPhrase());
        responseBodyEntity.setData(imageEntityPage.getContent());
        return new ResponseEntity<>(responseBodyEntity, headersResponse, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/user/{id}/image", method = GET, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<List<ImageEntity>>> getUserImage(@RequestHeader MultiValueMap<String, String> headers,
                                                                              @PathVariable(value = "id") Long id) {
        ResponseBodyEntity<List<ImageEntity>> responseBodyEntity = new ResponseBodyEntity<>();

        if (!this.userRepository.existsById(id)) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        Page<ImageEntity> imageEntityPage = this.imageRepository.findAllByUserOrderByCreatedAtDesc(this.userRepository.findById(id).get(), createPagination(headers));
        MultiValueMap<String, String> headersResponse = new LinkedMultiValueMap<>();

        headersResponse.add("page.total", Integer.toString(imageEntityPage.getTotalPages()));
        headersResponse.add("element.total", Long.toString(imageEntityPage.getTotalElements()));

        responseBodyEntity.setCode(HttpStatus.OK.value());
        responseBodyEntity.setMessage(HttpStatus.OK.getReasonPhrase());
        responseBodyEntity.setData(imageEntityPage.getContent());
        return new ResponseEntity<>(responseBodyEntity, headersResponse, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/me/image", method = POST, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<ImageEntity>> postUserImage(HttpServletRequest httpServletRequest,
                                                                         @RequestBody ImageEntity imageEntity, Errors errors) {
        ResponseBodyEntity<ImageEntity> responseBodyEntity = new ResponseBodyEntity<>();

        if (errors.hasErrors()) {
            responseBodyEntity.fromBindingErrors(errors);
            responseBodyEntity.setCode(HttpStatus.BAD_REQUEST.value());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBodyEntity);
        }

        if (!this.userRepository.existsByUsername(SecurityJWTokenService.getSubject(httpServletRequest))) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        UserEntity userEntity = this.userRepository.findByUsername(SecurityJWTokenService.getSubject(httpServletRequest));
        List<ImageEntity> imageEntities = this.imageRepository.findAllByUserOrderByCreatedAtDesc(userEntity);

        if (userEntity.getUserPackage().getPack().isLimitReached(imageEntities)) {
            responseBodyEntity.setCode(HttpStatus.BAD_REQUEST.value());
            responseBodyEntity.getErrors().add(MessageConstant.IMAGE_DAILY_UPLOAD_SIZE_REACHED.name());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBodyEntity);
        }

        if (userEntity.getUserPackage().getPack().isLimitReached(imageEntities)) {
            responseBodyEntity.setCode(HttpStatus.BAD_REQUEST.value());
            responseBodyEntity.getErrors().add(MessageConstant.IMAGE_DAILY_PHOTOS_REACHED.name());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBodyEntity);
        }

        imageEntity.setUser(userEntity);

        userEntity.getUserPackage().setConsumption(imageEntities.size() + 1);

        this.imageRepository.save(imageEntity);
        this.userRepository.save(userEntity);

        responseBodyEntity.setCode(HttpStatus.OK.value());
        responseBodyEntity.setMessage(HttpStatus.OK.getReasonPhrase());
        responseBodyEntity.setData(imageEntity);
        return new ResponseEntity<>(responseBodyEntity, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/user/{id}/image/{imageId}", method = DELETE, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<ImageEntity>> deleteImage(HttpServletRequest httpServletRequest,
                                                                       @PathVariable(value = "id") Long id,
                                                                       @PathVariable(value = "imageId") Long imageId) {
        ResponseBodyEntity<ImageEntity> responseBodyEntity = new ResponseBodyEntity<>();

        if (!this.userRepository.existsById(id)) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        this.imageRepository.deleteById(imageId);

        responseBodyEntity.setCode(HttpStatus.OK.value());
        return ResponseEntity.status(HttpStatus.OK).body(responseBodyEntity);
    }

    @RequestMapping(value = "/user/me/image/{id}", method = DELETE, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<ImageEntity>> deleteImage(HttpServletRequest httpServletRequest,
                                                                       @PathVariable(value = "id") Long id) {
        ResponseBodyEntity<ImageEntity> responseBodyEntity = new ResponseBodyEntity<>();

        if (!this.userRepository.existsByUsername(SecurityJWTokenService.getSubject(httpServletRequest))) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        this.imageRepository.deleteById(id);

        responseBodyEntity.setCode(HttpStatus.OK.value());
        return ResponseEntity.status(HttpStatus.OK).body(responseBodyEntity);
    }


    @RequestMapping(value = "/user/me/image", method = PUT, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<ImageEntity>> updateUserImage(HttpServletRequest httpServletRequest,
                                                                           @RequestBody ImageEntity imageEntity, Errors errors) {
        ResponseBodyEntity<ImageEntity> responseBodyEntity = new ResponseBodyEntity<>();

        if (errors.hasErrors()) {
            responseBodyEntity.fromBindingErrors(errors);
            responseBodyEntity.setCode(HttpStatus.BAD_REQUEST.value());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBodyEntity);
        }

        UserEntity userEntity = this.userRepository.findByUsername(SecurityJWTokenService.getSubject(httpServletRequest));

        if (userEntity == null) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        Optional<ImageEntity> imageExist = this.imageRepository.findByUserAndId(userEntity, imageEntity.getId());

        if (!imageExist.isPresent()) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.IMAGE_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        imageExist.get().setDescription(imageEntity.getDescription());
        imageExist.get().setTitle(imageEntity.getTitle());

        this.imageRepository.save(imageExist.get());

        responseBodyEntity.setCode(HttpStatus.OK.value());
        responseBodyEntity.setMessage(HttpStatus.OK.getReasonPhrase());
        responseBodyEntity.setData(imageExist.get());
        return new ResponseEntity<>(responseBodyEntity, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/user/{id}/image", method = PUT, produces = "application/json")
    public ResponseEntity<ResponseBodyEntity<ImageEntity>> updateImage(HttpServletRequest httpServletRequest,
                                                                       @PathVariable(value = "id") Long id,
                                                                       @RequestBody ImageEntity imageEntity,
                                                                       Errors errors) {
        ResponseBodyEntity<ImageEntity> responseBodyEntity = new ResponseBodyEntity<>();

        if (errors.hasErrors()) {
            responseBodyEntity.fromBindingErrors(errors);
            responseBodyEntity.setCode(HttpStatus.BAD_REQUEST.value());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBodyEntity);
        }

        Optional<UserEntity> userEntity = this.userRepository.findById(id);

        if (!userEntity.isPresent()) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.USER_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        Optional<ImageEntity> imageExist = this.imageRepository.findByUserAndId(userEntity.get(), imageEntity.getId());

        if (!imageExist.isPresent()) {
            responseBodyEntity.setCode(HttpStatus.NOT_FOUND.value());
            responseBodyEntity.getErrors().add(MessageConstant.IMAGE_NOT_FOUND.name());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBodyEntity);
        }

        imageExist.get().setDescription(imageEntity.getDescription());
        imageExist.get().setTitle(imageEntity.getTitle());

        this.imageRepository.save(imageExist.get());

        responseBodyEntity.setCode(HttpStatus.OK.value());
        responseBodyEntity.setMessage(HttpStatus.OK.getReasonPhrase());
        responseBodyEntity.setData(imageExist.get());
        return new ResponseEntity<>(responseBodyEntity, HttpStatus.OK);

    }

    private Pageable createPagination(MultiValueMap<String, String> headers) {
        Pageable pageable = PageRequest.of(0, 9, Sort.by("createdAt").descending());

        if (headers.containsKey("page.size") && headers.containsKey("page.from")) {
            pageable = PageRequest.of(Integer.parseInt(Objects.requireNonNull(headers.getFirst("page.from"))),
                    Integer.parseInt(Objects.requireNonNull(headers.getFirst("page.size"))),
                    Sort.by("createdAt").descending());
        }
        return pageable;
    }
}
