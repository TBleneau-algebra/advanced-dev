INSERT IGNORE INTO `users` (`id`, `username`, `email`, `name`, `lastname`, `password`, `profile_picture`, `created_at`, `updated_at`) VALUES (1, 'Admin', 'admin@aadbdt.com', 'Admin', 'Admin', '$2a$10$MW7Moz1KMr1Te2UkfcpaSufSCv0lmat4ptXGjIPSobFis6AOrNcBO', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT IGNORE INTO `roles` (`id`, `name`) VALUES (1, 'ROLE_USER');

INSERT IGNORE INTO `roles` (`id`, `name`) VALUES (2, 'ROLE_ADMIN');

INSERT IGNORE INTO `users_roles` (`user_id`, `role_id`) VALUES (1, 2);

INSERT IGNORE INTO `packages` (`id`, `name`, `daily_photos`, `daily_upload_size`, `price`, `unlimited`) VALUES (1, 'FREE_PACKAGE', 10, 4, 0, false);

INSERT IGNORE INTO `packages` (`id`, `name`, `daily_photos`, `daily_upload_size`, `price`, `unlimited`) VALUES (2, 'PROFESSIONAL_PACKAGE', 100, 40, 69, false);

INSERT IGNORE INTO `packages` (`id`, `name`, `daily_photos`, `daily_upload_size`, `price`, `unlimited`) VALUES (3, 'GOLD_PACKAGE', 0, 0, 120, true);

INSERT IGNORE INTO `users_packages` (`id`, `user_id`, `package_id`, `created_at`, `updated_at`) VALUES (1, 1, 3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT IGNORE INTO `tags` (`id`, `name`)
VALUES (1, 'Fitness'),
       (2, 'Workout'),
       (3, 'Sunset'),
       (4, 'Fit'),
       (5, 'Pretty'),
       (6, 'Trump'),
       (7, 'Coronavirus'),
       (8, 'French'),
       (9, 'Hrvatska'),
       (10, 'Croatia'),
       (11, 'Coffee'),
       (12, 'PhotoShop'),
       (13, 'Ugly'),
       (14, 'Beer'),
       (15, 'Green'),
       (16, 'Shoes'),
       (17, 'Victoria Garden'),
       (18, 'Nudes'),
       (19, 'Food'),
       (20, 'FoodPorn'),
       (21, 'Eat'),
       (22, 'Holidays'),
       (23, 'Exams'),
       (24, 'Java'),
       (25, 'Fat'),
       (26, 'Burger'),
       (27, 'Sport'),
       (28, 'Games'),
       (29, 'Football'),
       (30, 'Travel'),
       (31, 'Animal'),
       (32, 'Technology'),
       (33, 'Sony');
