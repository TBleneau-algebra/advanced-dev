package hr.algebra.bleneau.webapi.controller.attributes;

import hr.algebra.bleneau.webapi.utils.EmbeddedLoginIT;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PackageControllerTests extends EmbeddedLoginIT {

    @BeforeEach()
    public void setup() throws Exception {
        this.login();
    }

    @Test
    public void testGetAll() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/package", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(
                        "{\"code\":200,\"message\":\"OK\",\"data\":" +
                                "[{\"id\":1,\"name\":\"FREE_PACKAGE\",\"dailyPhotos\":10,\"dailyUploadSize\":4,\"price\":0,\"unlimited\":false}," +
                                "{\"id\":2,\"name\":\"PROFESSIONAL_PACKAGE\",\"dailyPhotos\":100,\"dailyUploadSize\":40,\"price\":69,\"unlimited\":false}," +
                                "{\"id\":3,\"name\":\"GOLD_PACKAGE\",\"dailyPhotos\":0,\"dailyUploadSize\":0,\"price\":120,\"unlimited\":true}]}"));

    }

    @Test
    public void testGetByIdFirst() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/package/1")
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(
                        "{\"code\":200,\"message\":null,\"data\":" +
                                "{\"id\":1,\"name\":\"FREE_PACKAGE\",\"dailyPhotos\":10,\"dailyUploadSize\":4,\"price\":0,\"unlimited\":false}}"));
    }

    @Test
    public void testGetByIdSecond() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/package/2")
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(
                        "{\"code\":200,\"message\":null,\"data\":" +
                                "{\"id\":2,\"name\":\"PROFESSIONAL_PACKAGE\",\"dailyPhotos\":100,\"dailyUploadSize\":40,\"price\":69,\"unlimited\":false}}"));
    }

    @Test
    public void testGetByIdThird() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/package/3")
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(
                        "{\"code\":200,\"message\":null,\"data\":" +
                                "{\"id\":3,\"name\":\"GOLD_PACKAGE\",\"dailyPhotos\":0,\"dailyUploadSize\":0,\"price\":120,\"unlimited\":true}}"));
    }
}
