package hr.algebra.bleneau.webapi.controller.user;

import hr.algebra.bleneau.webapi.entity.user.UserPackageEntity;
import hr.algebra.bleneau.webapi.utils.EmbeddedLoginIT;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserPackageControllerTests extends EmbeddedLoginIT {

    public static UserPackageEntity userPackageEntity;

    @BeforeEach()
    public void setup() throws Exception {
        this.login();
    }

    @Test
    @Order(1)
    public void testGetPackageUserById() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/user/1/package")
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(2)
    public void testGetPackageUserMe() throws Exception {
        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/user/me/package")
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON));

        resultActions.andExpect(status().isOk()).andDo(this.assignUserPackage(resultActions));
    }


    @Test
    @Order(3)
    public void testPutPackageUserMe() throws Exception {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime packageCreated = UserPackageControllerTests.userPackageEntity.getCreatedAt();
        LocalDateTime packageUpdated = UserPackageControllerTests.userPackageEntity.getUpdatedAt();

        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders
                .put("/api/v1/user/me/package/1")
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON));

        if (!packageCreated.equals(packageUpdated) && now.isAfter(packageUpdated)
                && now.isBefore(packageUpdated.plusDays(1))) {
            resultActions.andExpect(status().isUnauthorized());
        } else {
            resultActions.andExpect(status().isOk());
        }
    }

    private ResultHandler assignUserPackage(ResultActions resultActions) throws Exception {
        String content = resultActions.andReturn().getResponse().getContentAsString();

        UserPackageControllerTests.userPackageEntity = this.objectMapper.readValue(new JSONObject(content).get("data").toString(), UserPackageEntity.class);
        return (result) -> {
        };
    }
}
