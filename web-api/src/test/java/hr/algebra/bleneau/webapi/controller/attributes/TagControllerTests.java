package hr.algebra.bleneau.webapi.controller.attributes;

import hr.algebra.bleneau.webapi.utils.EmbeddedLoginIT;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class TagControllerTests extends EmbeddedLoginIT {

    @BeforeEach()
    public void setup() throws Exception {
        this.login();
    }

    @Test
    public void testGetAll() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/tag", 1)
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
