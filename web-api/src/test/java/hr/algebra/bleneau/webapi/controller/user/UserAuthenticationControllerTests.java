package hr.algebra.bleneau.webapi.controller.user;

import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.utils.EmbeddedLoginIT;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserAuthenticationControllerTests extends EmbeddedLoginIT {

    public static UserEntity userEntity;

    @Test
    @Order(1)
    public void testPostRegisterSuccessful() throws Exception {
        Random random = new Random();
        Map<String, String> userMap = new HashMap<>();
        String generatedString = random.ints(97, 122 + 1)
                .limit(5).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

        userMap.put("name", "Name-" + generatedString);
        userMap.put("lastname", "Lastname-" + generatedString);
        userMap.put("username", "Username-" + generatedString);
        userMap.put("email", "email-username-" + generatedString + "@unit-test.com");
        userMap.put("password", "PasswordTest|@43210" + generatedString);

        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(userMap)));

        resultActions.andExpect(status().isCreated()).andDo(this.assignUser(resultActions));
    }

    @Test
    @Order(2)
    public void testPostRegisterAlreadyExist() throws Exception {
        Map<String, String> userMap = new HashMap<>();
        userMap.put("name", "Admin");
        userMap.put("lastname", "Admin");
        userMap.put("username", "Admin");
        userMap.put("email", "admin-official@aadbdt.com");
        userMap.put("password", "PasswordTest|@43210");

        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userMap)))
                .andExpect(status().isConflict());
    }

    @Test
    @Order(3)
    public void testPostRegisterBadRequest() throws Exception {
        Map<String, String> userMap = new HashMap<>();
        userMap.put("name", "Test");
        userMap.put("lastname", "Test");
        userMap.put("username", "Test");
        userMap.put("email", "test.email@outlook.com");
        userMap.put("password", "");

        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userMap)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Order(4)
    public void testLoginSuccessful() throws Exception {
        Map<String, String> userMap = new HashMap<>();

        userMap.put("username", "Admin");
        userMap.put("password", "Qwerty|43210");

        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userMap)))
                .andExpect(status().isOk());
    }

    @Test
    @Order(5)
    public void testLoginNotFound() throws Exception {
        Map<String, String> userMap = new HashMap<>();

        userMap.put("username", "Test");
        userMap.put("password", "Qwerty|43210");

        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userMap)))
                .andExpect(status().isNotFound());
    }

    @Test
    @Order(6)
    public void testLogoutSuccessful() throws Exception {
        this.login();

        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/user/logout")
                .cookie(this.getSessionCookie())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(7)
    public void testDeleteRegisteredUser() throws Exception {
        this.login();

        this.mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/v1/user/" + UserAuthenticationControllerTests.userEntity.getId())
                .cookie(this.getSessionCookie())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private ResultHandler assignUser(ResultActions resultActions) throws Exception {
        String content = resultActions.andReturn().getResponse().getContentAsString();

        UserAuthenticationControllerTests.userEntity = this.objectMapper.readValue(new JSONObject(content).get("data").toString(), UserEntity.class);
        return (result) -> {
        };
    }
}
