package hr.algebra.bleneau.webapi.controller.user;

import hr.algebra.bleneau.webapi.entity.user.UserEntity;
import hr.algebra.bleneau.webapi.utils.EmbeddedLoginIT;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserInformationControllerTests extends EmbeddedLoginIT {

    public static UserEntity userEntity;

    @BeforeEach()
    public void setup() throws Exception {
        this.login();
    }

    @Test
    @Order(1)
    public void testGetAll() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/user", 1)
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(2)
    public void testGetUserMe() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/user/me", 1)
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(3)
    public void testGetUserById() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/user/1", 1)
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(4)
    public void testDeleteUserById() throws Exception {
        this.createUser();

        String generatedString = new Random().ints(97, 122 + 1)
                .limit(5).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

        UserInformationControllerTests.userEntity.setLastname("Lastname-" + generatedString);
        UserInformationControllerTests.userEntity.setEmail("email-username-" + generatedString + "@unit-test.com");

        this.mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/v1/user/" + UserInformationControllerTests.userEntity.getId())
                .cookie(this.getSessionCookie())
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(UserInformationControllerTests.userEntity)));
    }

    private void createUser() throws Exception {
        Map<String, String> userMap = new HashMap<>();
        String generatedString = new Random().ints(97, 122 + 1)
                .limit(5).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

        userMap.put("name", "Name-" + generatedString);
        userMap.put("lastname", "Lastname-" + generatedString);
        userMap.put("username", "Username-" + generatedString);
        userMap.put("email", "email-username-" + generatedString + "@unit-test.com");
        userMap.put("password", "PasswordTest|@43210" + generatedString);

        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(userMap)));

        resultActions.andExpect(status().isCreated()).andDo(this.assignUser(resultActions));
    }

    private ResultHandler assignUser(ResultActions resultActions) throws Exception {
        String content = resultActions.andReturn().getResponse().getContentAsString();

        UserInformationControllerTests.userEntity = this.objectMapper.readValue(new JSONObject(content).get("data").toString(), UserEntity.class);
        System.out.println(UserInformationControllerTests.userEntity);
        return (result) -> {
        };
    }
}
