package hr.algebra.bleneau.webapi.controller.user;

import hr.algebra.bleneau.webapi.entity.attributes.image.ImageEntity;
import hr.algebra.bleneau.webapi.utils.EmbeddedLoginIT;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserImageControllerTests extends EmbeddedLoginIT {

    public static ImageEntity imageEntity;

    @BeforeEach()
    public void setup() throws Exception {
        this.login();
    }

    @Test
    @Order(1)
    public void testGetAll() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/image")
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(2)
    public void testGetImageUserById() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/user/1/image")
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(3)
    public void testGetImageUserMe() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/user/me/image")
                .cookie(this.getSessionCookie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(4)
    public void testPostImageUserMe() throws Exception {
        Map<String, Object> imageMap = new HashMap<>();

        imageMap.put("title", "Test Image");
        imageMap.put("description", "Test Image");
        imageMap.put("width", 1920);
        imageMap.put("height", 1080);
        imageMap.put("format", "image/png");
        imageMap.put("image", "data:image/png;base64,iVBORw0KGgoAAA");

        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/user/me/image")
                .cookie(this.getSessionCookie())
                .content(this.objectMapper.writeValueAsString(imageMap))
                .contentType(MediaType.APPLICATION_JSON));

        resultActions
                .andExpect(status().isOk())
                .andDo(this.assignImage(resultActions));
    }

    @Test
    @Order(5)
    public void testPutImageUserMe() throws Exception {
        UserImageControllerTests.imageEntity.setTitle("New Test title");
        UserImageControllerTests.imageEntity.setDescription("New Test Description");

        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders
                .put("/api/v1/user/me/image")
                .cookie(this.getSessionCookie())
                .content(this.objectMapper.writeValueAsString(UserImageControllerTests.imageEntity))
                .contentType(MediaType.APPLICATION_JSON));

        resultActions
                .andExpect(status().isOk())
                .andDo(this.assignImage(resultActions));
    }

    @Test
    @Order(6)
    public void testDeleteImageUserMe() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/v1/user/me/image/" + UserImageControllerTests.imageEntity.getId())
                .cookie(this.getSessionCookie())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private ResultHandler assignImage(ResultActions resultActions) throws Exception {
        String content = resultActions.andReturn().getResponse().getContentAsString();

        UserImageControllerTests.imageEntity = this.objectMapper.readValue(new JSONObject(content).get("data").toString(), ImageEntity.class);
        return (result) -> {
        };
    }
}
