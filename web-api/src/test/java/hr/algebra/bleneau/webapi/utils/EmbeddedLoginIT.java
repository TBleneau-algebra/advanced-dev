package hr.algebra.bleneau.webapi.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.servlet.http.Cookie;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public abstract class EmbeddedLoginIT {

    private Cookie sessionCookie;

    @Autowired
    public MockMvc mockMvc;

    @Autowired
    public ObjectMapper objectMapper;

    public void login() throws Exception {
        Map<String, String> userMap = new HashMap<>();

        userMap.put("username", "Admin");
        userMap.put("password", "Qwerty|43210");

        ResultActions resultActions = this.mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/api/v1/user/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userMap)));

        resultActions.andExpect(status().isOk()).andDo(this.assignCookie(resultActions));
    }

    private ResultHandler assignCookie(ResultActions resultActions) {
        this.sessionCookie = resultActions.andReturn().getResponse().getCookie("JWToken");

        return (result) -> {
        };
    }

    public Cookie getSessionCookie() {
        return sessionCookie;
    }
}
