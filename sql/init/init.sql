CREATE DATABASE IF NOT EXISTS `aadbdt_db_dev`;
CREATE DATABASE IF NOT EXISTS `aadbdt_db_prod`;

CREATE USER IF NOT EXISTS 'aadbdt_user_dev'@'%' IDENTIFIED BY '!hxA?%fNWC_MP2Yv';
CREATE USER IF NOT EXISTS 'aadbdt_user_prod'@'%' IDENTIFIED BY '!hxA?%fNWC_MP2Yv';

GRANT ALL PRIVILEGES ON aadbdt_db_dev.* TO 'aadbdt_user_dev'@'%';

FLUSH PRIVILEGES;

GRANT ALL PRIVILEGES ON aadbdt_db_prod.* TO 'aadbdt_user_prod'@'%';

FLUSH PRIVILEGES;
