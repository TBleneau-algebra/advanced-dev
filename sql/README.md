# SQL Database

### Initialize the database in your local machine

On your local machine, you must first connect to the mysql daemon

```sh
      mysql -u root -p
```

You need to know the password of your root user. Enter the password of your root user and you are finally connected as a super user to MySQL
Now, you just have to create a user and your database for the connection.

```sh 
      CREATE DATABASE IF NOT EXISTS aadbdt_db_dev;
      CREATE DATABASE IF NOT EXISTS aadbdt_db_prod;

      CREATE USER IF NOT EXISTS 'aadbdt_dev_user'@'localhost' IDENTIFIED BY 'xxxxxxxxxxxxxxxxxxx';
      CREATE USER IF NOT EXISTS 'aadbdt_prod_user'@'localhost' IDENTIFIED BY 'xxxxxxxxxxxxxxxxxxx';

      GRANT ALL PRIVILEGES ON aadbdt_database.* TO 'aadbdt_dev_user'@'localhost';

      FLUSH PRIVILEGES;

      GRANT ALL PRIVILEGES ON aadbdt_database.* TO 'aadbdt_prod_user'@'localhost';

      FLUSH PRIVILEGES;

      QUIT
```

### Initialize the database in your docker container

You have the choice between two methods to create the database and the user who will have the rights to modify this database.

To do this, you must define in the [config/environment](./config/environment) folder, a file [sql-db.env](./config/environment/sql-db.env) which will be loaded at the start of the containerization of the MySQL image.
Then simply set the following environment variables:

```sh
      MYSQL_ROOT_PASSWORD=xxxxxxxxxxxxx   # Corresponds to the MySQL root user password
```

All you have to do is place an initialization script that will be executed when the MySQL daemon is launched in your docker container. 
This file must be placed in the [init](./init) folder.  
The script must contain the following MySQL commands :  

```sh 
      CREATE DATABASE IF NOT EXISTS aadbdt_db_dev;
      CREATE DATABASE IF NOT EXISTS aadbdt_db_prod;

      CREATE USER IF NOT EXISTS 'aadbdt_user_dev'@'localhost' IDENTIFIED BY 'xxxxxxxxxxxxxxxxxxx';
      CREATE USER IF NOT EXISTS 'aadbdt_user_prod'@'localhost' IDENTIFIED BY 'xxxxxxxxxxxxxxxxxxx';

      GRANT ALL PRIVILEGES ON aadbdt_database.* TO 'aadbdt_user_dev'@'localhost';

      FLUSH PRIVILEGES;

      GRANT ALL PRIVILEGES ON aadbdt_database.* TO 'aadbdt_user_prod'@'localhost';

      FLUSH PRIVILEGES;

      QUIT
``` 

The second option is to do this manually.
With docker, your must first access to your docker container. In this project, the container name is 'aadbdt-database'.

```sh
      docker exec -it aadbdt-database /bin/bash
```

Once access to the docker container has been established, you have to connect to the mysql daemon :

```sh
      mysql -u root -p
```

You need to know the password of your root user. Enter the password of your root user and you are finally connected as a super user to MySQL
Now, you just have to create a user and your database for the connection.    

### Define firewall rules for your docker containers  

If you use a MySQL docker container on your local machine, you should create a sample iptables rule to open linux iptables firewall.

```sh
      iptables -A INPUT -i eth0 -p tcp --destination-port 3306 -j ACCEPT
```

In the Server, we only want to allow remote connection from the web API. 
For that, you should create a sample iptables rule which only allows access to the MySQL docker container to an ip address (that of the web API)

```sh
      iptables -A INPUT -i eth0 -s <ip_address> -p tcp --destination-port 3306 -j ACCEPT
```

For security reasons, Docker configures the iptables rules to prevent containers from forwarding traffic from outside the host machine, on Linux hosts. 
Docker sets the default policy of the FORWARD chain to DROP.

If the container running on host1 needs the ability to communicate directly with a container on host2, you need a route from host1 to host2.
Setting the policy to ACCEPT accomplishes this.

```sh
      sudo iptables -P FORWARD ACCEPT
```
